#include "lis3mdl.h"

sLis3mdl lis3mdl;

void Lis3mdl_Wakeup();

unsigned char Lis3mdl_Read(  unsigned char reg );
void          Lis3mdl_Write( unsigned char reg, unsigned char data);

uint8_t txMagn[2];
uint8_t rxMagn[2];


bool Lis3mdl_Init()
{
   Lis3mdl_Write( CTRL_REG3, MD0 ); // Set 
   lis3mdl.who_am_i = Lis3mdl_Read(WHO_AM_I );
   
   if( LIS3MDL_OK )
   {   
      Lis3mdl_ReadStatusReg();
      
      Lis3mdl_Write( CTRL_REG1, (DO1 | DO0 | OM1 | OM0) );
      //Lis3mdl_Write( CTRL_REG2, (FS1 | FS0) ); 
      Lis3mdl_Write( CTRL_REG2, 0 ); 
      Lis3mdl_Write( CTRL_REG3, MD0 ); // Set 
      Lis3mdl_Write( CTRL_REG4, (OMZ0 | OMZ1) ); // Set UHP mode per l'asse Z
      Lis3mdl_Write( CTRL_REG5, 0x00 ); // Set

      lis3mdl.reg1 = Lis3mdl_Read( CTRL_REG1 );
      lis3mdl.reg2 = Lis3mdl_Read( CTRL_REG2 );
      lis3mdl.reg3 = Lis3mdl_Read( CTRL_REG3 );
      lis3mdl.reg4 = Lis3mdl_Read( CTRL_REG4 );
      lis3mdl.reg5 = Lis3mdl_Read( CTRL_REG5 );
      
      return true;
   }
   else return false;
}

void Lis3mdl_Idle()
{
   Lis3mdl_Write( CTRL_REG3, ( MD1 | MD0 ) ); // Set Idle mode
   lis3mdl.idle = 1;
}

void Lis3mdl_Wakeup()
{
   Lis3mdl_Write( CTRL_REG2, 0 ); 
   Lis3mdl_Write( CTRL_REG4, (OMZ0 | OMZ1) ); // Set UHP mode per l'asse Z
   
   lis3mdl.idle = 0;
}

void Lis3mdl_StartContinuous()
{
   if( lis3mdl.idle == 1 ) Lis3mdl_Wakeup();  
   Lis3mdl_Write( CTRL_REG3, 0 ); 
}
void Lis3mdl_StartSingle()
{
   if( lis3mdl.idle == 1 ) Lis3mdl_Wakeup();
   Lis3mdl_Write( CTRL_REG3, MD0 ); 
}

void Lis3mdl_SetInt(int16_t soglia)
{
   uint8_t reg;
   reg = Lis3mdl_Read(  INT_SRC );
   
   Lis3mdl_Write( INT_THS_L, (soglia & 0xff) ); 
   Lis3mdl_Write( INT_THS_H, (soglia >> 8) ); 
   Lis3mdl_Write( INT_CFG, (XIEN | IEN | IEA | LIR ) );
}

void Lis3mdl_ReadStatusReg()
{
   lis3mdl.status_reg = Lis3mdl_Read( STATUS_REG );
}

bool Lis3mdl_ReadValues()
{
   uint8_t datol;
   uint8_t datoh;
   int16_t dato;
   
   datol = Lis3mdl_Read( OUT_X_L );
   datoh = Lis3mdl_Read( OUT_X_H );
   dato  = (int16_t)datoh << 8;
   dato |= (int16_t)datol;
   lis3mdl.value_x = dato >> 3;
   
   datol = Lis3mdl_Read( OUT_Y_L );
   datoh = Lis3mdl_Read( OUT_Y_H );
   dato  = (int16_t)datoh << 8;
   dato |= (int16_t)datol;
   lis3mdl.value_y = dato >> 3;

   datol = Lis3mdl_Read( OUT_Z_L );
   datoh = Lis3mdl_Read( OUT_Z_H );
   dato  = (int16_t)datoh << 8;
   dato |= (int16_t)datol;
   lis3mdl.value_z = dato >> 3;
   
   datol = Lis3mdl_Read( TEMP_OUT_L);
   datoh = Lis3mdl_Read( TEMP_OUT_H);
   dato  = (int16_t)datoh << 8;
   dato |= (int16_t)datol;
   lis3mdl.temperatura = dato;

   Lis3mdl_Write( CTRL_REG3, MD0 ); 
   return true;
}

bool Lis3mdl_ReadOffset()
{
   unsigned char datol;
   unsigned char datoh;
   
   datol = Lis3mdl_Read( OFFSET_X_REG_L_M );
   datoh = Lis3mdl_Read( OFFSET_X_REG_H_M );
   lis3mdl.offset_x  = (int16_t)datoh << 8;
   lis3mdl.offset_x |= (int16_t)datol;

   datol = Lis3mdl_Read( OFFSET_Y_REG_L_M );
   datoh = Lis3mdl_Read( OFFSET_Y_REG_H_M );
   lis3mdl.offset_y  = (int16_t)datoh << 8;
   lis3mdl.offset_y |= (int16_t)datol;

   datol = Lis3mdl_Read( OFFSET_Z_REG_L_M );
   datoh = Lis3mdl_Read( OFFSET_Z_REG_H_M );
   lis3mdl.offset_z  = (int16_t)datoh << 8;
   lis3mdl.offset_z |= (int16_t)datol;

   return true;
   
}

bool Lis3mdl_ReadTemperature()
{
   unsigned char datol;
   unsigned char datoh;
   datol = Lis3mdl_Read( TEMP_OUT_L);
   datoh = Lis3mdl_Read( TEMP_OUT_H);
   lis3mdl.temperatura  = (int16_t)datoh << 8;
   lis3mdl.temperatura |= (int16_t)datol;
   return true;
}
void Lis3mdl_Write( unsigned char reg, unsigned char data)
{
   int timeout = 500;
   
   cy_en_scb_spi_status_t spi_status;
   uint32_t masterStatus;        

   csmagn_on();
   CyDelay(1);
   txMagn[0] = reg;
   txMagn[1] = data;

   spi_status = Cy_SCB_SPI_Transfer( spifr_HW, txMagn , 0 , 2 , &spifr_context);
   
   /* If no error wait till master sends data in Tx FIFO */
    if(spi_status == CY_SCB_SPI_SUCCESS)
    {
        /* Timeout 1 sec */
        /* Wait until master complete read transfer or time out has occured */
        do
        {
            masterStatus  = Cy_SCB_SPI_GetTransferStatus(spifr_HW, &spifr_context);
            if( (masterStatus & CY_SCB_SPI_TRANSFER_ACTIVE) == 0 ) timeout = 0;
            else
            {
               CyDelay(1);
               timeout--;            
            }
        } while(timeout);        
   }
   
   csmagn_off();
}

unsigned char Lis3mdl_Read( unsigned char reg )
{
   unsigned char temp;
   int timeout = 500;
   
   cy_en_scb_spi_status_t spi_status;
   uint32_t masterStatus;        

   reg |= 0b10000000;

   csmagn_on();
   CyDelay(1);
   
   txMagn[0] = reg;
   txMagn[1] = 0;
   rxMagn[0] = 0;
   rxMagn[1] = 0;

   spi_status = Cy_SCB_SPI_Transfer( spifr_HW, txMagn , rxMagn , 2 , &spifr_context);
   
   /* If no error wait till master sends data in Tx FIFO */
    if(spi_status == CY_SCB_SPI_SUCCESS)
    {
        /* Timeout 1 sec */
        /* Wait until master complete read transfer or time out has occured */
        do
        {
            masterStatus  = Cy_SCB_SPI_GetTransferStatus(spifr_HW, &spifr_context);
            if( (masterStatus & CY_SCB_SPI_TRANSFER_ACTIVE) == 0 ) timeout = 0;
            else
            {
               CyDelay(1);
               timeout--;            
            }
        } while(timeout);        
   }
   
   csmagn_off();
   
   return rxMagn[1];
   
}
