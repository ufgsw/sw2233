/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
//#include "bootloader/bootloader_cy.h"
#include "project.h"
#include "dfu/cy_dfu.h"

// CY_SET_REG32(CYREG_IPC_INTR_STRUCT0_INTR_MASK, 0x00070000u);

unsigned long ad;

cy_en_clkbak_in_sources_t a;
cy_en_syspm_sc_charge_key_t key;



int main(void)
{
   cy_en_ble_api_result_t apiResult;
   
   uint32_t reset = Cy_SysLib_GetResetReason();
   Cy_SysLib_ClearResetReason();
   

//  Cy_SysLib_ResetBackupDomain();
//   
//   //key = CY_SYSPM_SC_CHARGE_ENABLE;  
//   key = CY_SYSPM_SC_CHARGE_DISABLE;
//   Cy_SysPm_BackupSuperCapCharge(key);
//
//   Cy_SysClk_WcoBypass(0);
//   
//  
   
//   else 
//      error.bit.wco = 1;

   
   //Cy_GPIO_Write( on_off_PORT, on_off_NUM, 1 );
   
//   Cy_LVD_Disable();
//   Cy_LVD_SetThreshold( CY_LVD_THRESHOLD_3_1_V );  
//   Cy_LVD_Enable();
//   
   
   __enable_irq(); /* Enable global interrupts. */
    /* Enable CM4.  CY_CORTEX_M4_APPL_ADDR must be updated if CM4 memory layout is changed. */

      
   /* Start the Controller portion of BLE. Host runs on the CM4 */
   apiResult = Cy_BLE_Start(NULL);
   if(apiResult == CY_BLE_SUCCESS)
   {
      /* Enable CM4 only if BLE Controller started successfully.
      * CY_CORTEX_M4_APPL_ADDR must be updated if CM4 memory layout
      * is changed. */
      
#ifdef NDEBUG      
      // Per bootloader
      Cy_SysEnableCM4( (uint32_t) (&__cy_app_core1_start_addr) );
#else
      // Per debug
      Cy_SysEnableCM4( CY_CORTEX_M4_APPL_ADDR );
#endif

   }
   else
   {
      /* Halt CPU */
      //CY_ASSERT(0u != 0u);
      while(1)
      {
         Cy_GPIO_Inv( led_rosso_PORT, led_rosso_NUM );
         CyDelay( 150 );
      }      
   }

   
   for(;;)
   {
      /* Process BLE events continuously for controller in dual core mode */
      Cy_BLE_ProcessEvents();

      /* To achieve low power in the device */
      Cy_SysPm_DeepSleep(CY_SYSPM_WAIT_FOR_INTERRUPT); 
   }
}

/* [] END OF FILE */
