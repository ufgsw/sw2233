## DSMART 2.0   Securemme

Serratura con controllo accessi 
Possibilità di apertura in diversi modi ( sfruttando il BLE )

* *Con uno smartphone*
* *Con una tastiera numerica* BLE
* Con una tastiera/tag esterna via cavo
* *Con un lettore di Tag mifire Classic*
* *Con un lettore di impronte*

---



## Changelog



##### ***Release 2.7.7***

+ dopo una chiusura se arriva un apertura da ble entro 5 secondi non veniva considerata, ora ok

##### *Release 2.7.6*

* Release ***speciale*** per prove con corrente sul motore piu' alte ( la limitazione si regola da 1.6 volt a 3.2 volt )

##### *Release 2.7.5*

* Miglioramenti sulla comunicazione col bridge

***Release 2.7.4***

* Risolto un conflitto sulla creazione dell'advertising

##### *Release 2.7.3*

* Release ***speciale*** dove la serratura continua ad aprire e chiudere in continuazione ( utile per testare la durata del motore )

##### *Release 2.7.2*

* Release ***speciale*** dove il tasto funziona solo per l'apertura impostando come tipo di chiusura la modalità chiusura manuale
* Il led di segnalazione è sempre spento 
* Per l'accoppiamento il tasto va tenuto premuto 15 secondi

**Release 2.7.1**

* Migliorata la calibrazione del tempo di chiusura e apertura

##### Release 2.7.0

* Sistemata la chiusura semiautomatica con sensore porta chiusa

##### Release 2.6.8/9

* Migliorato il supporto per il sensore porta chiusa BLE 

##### Release 2.6.7

* Salvato lo stato porta ad ogni cambiamento per trovarsi pronto alla riaccensione

##### Release 2.6.5

* Big fix sul codice apertura da cantiere  ( ora si disabilita appena si crea un utente )

##### Release 2.6.4

* Modificato il tiraggio dello scrocco dopo la seconda apertura consecutiva ( con scrocco disabilitato )

##### Release 2.6.3

* Implementato il sensore porta chiusa BLE

##### Release 2.6.1

* Corretto il bug che mostrava 100 nei minuti delle chiusure programmate

##### Release 2.6.0

* Corretto un Bug sul comando 0x43
* Aggiunto il parametro ***velocita lenta in chiusura***

##### Release 2.5.9

* Aggiunto il comando **0x43** per l'apertura da smartphone con codice tastierino ( solo per affittuari )

##### Release 2.5.8

* Rallentamento in apertura durante la corsa dello scrocco
* Rallentamento in chiusura con e senza scrocco
* Inviati al telefono i parametri aggiornati dopo aver eseguito una calibrazione

##### Release 2.5.7

* Migliorata la gestione led/buzzer sulla tastierina rotonda
* Aggiunto un comando per l'identificazione della scheda da app

##### Release 2.5.6

* L’uscita dalla modalità cantiere ora avviene con il cambio del master code e non con la creazione del primo utente

##### Release 2.5.5

* Segnalata la corretta o l'errata programmazione dell'entrata affittuaria tramite codice web ( 19 cifre ) sul tastierino

##### Release 2.5.4

* Migliorata la corsa dell'apertura in serratura senza scrocco

##### Release 2.5.3

* Migliorata la comunicazione con il Bridge

##### Release 2.5.1

* Tolto un  bug sulla scadenza dell'advertising

##### Release 2.5.0

* Aggiunta la modalità cantiere ( codice 123 e qualsiasi tag ) anche per la versione CON APP, uscirà dalla modalità cantiere dopo aver aggiunto un utente

##### Release 2.4.9

* Nella versione a batteria tengo sveglia la tastierina rotonda alla richiesta di memorizzazione di un tag

##### Release 2.4.8

* Aggiunta l'id di risposta a un messaggio ( byte 3 della parte in chiaro )

##### Release 2.4.4

* Aggiunta la funzionalità senza App ( con tastierino rotondo )

##### Release 2.4.0

* Aggiunta la compatibilità con la tastierina esterna in release 1.2.0 ( con la possibilità di disabilitare il buzzer )

##### Release 2.3.9

* Eliminata la chiusura automatica programmata giornalmente
* Aggiunta la chiusura automatica programmata giornalmente
* Sistemato un bug nel sincronismo stato porta con il cloud

##### Release 2.3.8

* Implementata la scadenza di un messaggio da consegnare al bridge

##### Release 2.3.7

* Modificato il feedback con l'errato inserimento dei codici da tastiera dopo 9 tentativi ( si disattiva per 5 minuti e il led lampeggia rosso )

##### Release 2.3.6

* Sistemato un bug sull'eliminazione automatica degli affittuari

##### Release 2.3.5

* Implementato il log UFG

##### Release 2.3.2

* Modificato il comando 0x1B ( Aggiunta la possibilità di accoppiare un
  ripetitore )
* Modificato il comando 0x11 ( aggiunta l’apertura da ripetitore )

##### Release 2.2.8

* Modificato il comando 0x60 ( aggiunta l’opzione B0 = 3 )

##### Release 2.2.7

* Aggiunto il comando 0x09 per l’eliminazione di un utente affittuario

##### Release 2.2.5

* Aggiunto il comando 0x61 per il settaggio della wireless sul bridge

##### Release 2.2.4

* Aggiunto nella trasmissione utenti il numero utente e il numero utenti registrati

* Aggiunto il backup utente sul cloud quando modificato o creato

##### Release 2.2.1

* Aggiunto il permesso di aprire e chiudere da cloud

##### Release 2.2.0

* implementato il funzionamento del Bridge

##### Release 2.1.4

* Migliorata la memorizzazione del fingerprint
##### Release 2.1.3
* Aggiunto l'accoppiamento con il Brigde
* Modificato l'oscillatore ( utilizzo quello per il BLE *AltHF ECO*" )
##### Release 2.1.2
* Aggiunto come Tipo di utente l'utente Aziendale
##### Release 2.1.1
* Aggiunto il comando 0x37 per sapere il BDAddress e il Localname
* Modificato il comando 0x14 ( possibilità di differenziare le richieste di dati )
* Modificato il comando 0x1B ( aggiunto l'accoppiamento col Bridge )
* Aggiunto il comando 0x38 ( solo ricezione )
* Aggiunto il comando 0x37 per sapere il BDAddress e il nome
##### Release 2.0.6
* Modificato il comando 0x1f ( se un codice apertura è gia esistente restituisce l'errore su 0xf0 )
* Dopo 10 errori consecutivi di codice tastiera la serratura si disabilita per 5 minuti
##### Release 2.0.5
* Modificato il bootloader ( nelle impostazioni di sistema impostata la VBackup su external source )
##### Release 2.0.4 
* Corretto un bug ad utente disabilitato ( ora restituisce il codice di errore giusto )
##### Release 2.0.0
* Implementata la Connessione con più dispositivi
* il messaggio di errore 0x0f contiene nuovi codici
* Mantenendo la connessione attiva il telefono riesce a ricevere se un associazione o un accoppiamento sono andati o meno a buon fine