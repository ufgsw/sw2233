/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "app.h"
#include "nfc.h"

eFaseNfc faseNfc = NFC_INIT;

phStatus_t Ex4_NfcRdLibInit(void);


/* Crypto Headers */
#ifdef NXPBUILD__PH_CRYPTOSYM_SW
#include <phCryptoSym.h>
#endif /* NXPBUILD__PH_CRYPTOSYM_SW */

#ifdef NXPBUILD__PH_CRYPTORNG_SW
#include <phCryptoRng.h>
#endif /* NXPBUILD__PH_CRYPTORNG_SW */

/*******************************************************************************
**   Global Variable Declaration
*******************************************************************************/
#ifdef NXPBUILD__PHPAL_I14443P4MC_SW
extern void pWtoxCallBck(uint8_t bTimerId);
#endif /* NXPBUILD__PHPAL_I14443P4MC_SW */

#ifdef NXPBUILD__PHPAL_I18092MT_SW
extern void pRtoxCallBck(uint8_t bTimerId);
#endif /* NXPBUILD__PHPAL_I18092MT_SW */

#ifdef NXPBUILD__PHCE_T4T_SW
/**
 * Application buffer. Used in phceT4T_Init. Its needed for data exchange
 * between application thread and reader library thread. Refer phceT4T_Init in
 * phceT4T.h for more info.
 * */
extern uint8_t aAppHCEBuf[];
extern uint16_t wAppHCEBuffSize;
#endif /* NXPBUILD__PHCE_T4T_SW */

phPlatform_DataParams_t            sPlatform;          /**< Platform component holder */

/* PAL declarations */
#ifdef NXPBUILD__PHPAL_I14443P3A_SW
phpalI14443p3a_Sw_DataParams_t     spalI14443p3a;      /* PAL ISO I14443-A component */
#endif /* NXPBUILD__PHPAL_I14443P3A_SW */

#ifdef NXPBUILD__PHPAL_I14443P3B_SW
phpalI14443p3b_Sw_DataParams_t     spalI14443p3b;      /* PAL ISO I14443-B component */
#endif /* NXPBUILD__PHPAL_I14443P3B_SW */

#ifdef NXPBUILD__PHPAL_I14443P4A_SW
phpalI14443p4a_Sw_DataParams_t     spalI14443p4a;      /* PAL ISO I14443-4A component */
#endif /* NXPBUILD__PHPAL_I14443P4A_SW */

#ifdef NXPBUILD__PHPAL_I14443P4_SW
phpalI14443p4_Sw_DataParams_t      spalI14443p4;       /* PAL ISO I14443-4 component */
#endif /* NXPBUILD__PHPAL_I14443P4_SW */

#ifdef NXPBUILD__PHPAL_MIFARE_SW
phpalMifare_Sw_DataParams_t        spalMifare;         /* PAL Mifare component */
#endif /* NXPBUILD__PHPAL_MIFARE_SW */

#ifdef NXPBUILD__PHPAL_FELICA_SW
phpalFelica_Sw_DataParams_t        spalFelica;         /* PAL Felica component */
#endif /* NXPBUILD__PHPAL_FELICA_SW */

/* Discovery Loop variable */
#ifdef NXPBUILD__PHAC_DISCLOOP_SW
phacDiscLoop_Sw_DataParams_t       sDiscLoop;          /* Discovery loop component */
#endif /* NXPBUILD__PHAC_DISCLOOP_SW */

/* Application Layer declaration */
#ifdef NXPBUILD__PHAL_FELICA_SW
phalFelica_Sw_DataParams_t         salFelica;          /* AL Felica component */
#endif /* NXPBUILD__PHAL_FELICA_SW */

#ifdef NXPBUILD__PHAL_MFC_SW
phalMfc_Sw_DataParams_t            salMFC;             /* AL Mifare Classic component */
#endif /* NXPBUILD__PHAL_MFC_SW */

#ifdef NXPBUILD__PHAL_MFUL_SW
phalMful_Sw_DataParams_t           salMFUL;            /* AL Mifare Ultralight component */
#endif /* NXPBUILD__PHAL_MFUL_SW */

#ifdef NXPBUILD__PHAL_MFDF_SW
phalMfdf_Sw_DataParams_t           salMFDF;            /* AL Mifare DESFire component */
#endif /* NXPBUILD__PHAL_MFDF_SW */

#ifdef NXPBUILD__PHAL_T1T_SW
phalT1T_Sw_DataParams_t            salT1T;             /* AL T1T component */
#endif /* NXPBUILD__PHAL_T1T_SW */

#ifdef NXPBUILD__PHAL_TOP_SW
phalTop_Sw_DataParams_t            salTop;             /* AL Tag Operations component */
#endif /* NXPBUILD__PHAL_TOP_SW */

#ifdef NXPBUILD__PHCE_T4T_SW
phceT4T_Sw_DataParams_t            sceT4T;             /* HCE component */
#endif /* NXPBUILD__PHCE_T4T_SW */

#ifdef NXPBUILD__PHLN_LLCP_SW
phlnLlcp_Sw_DataParams_t           slnLlcp;            /* LLCP component */
#endif /* NXPBUILD__PHLN_LLCP_SW */

#ifdef NXPBUILD__PH_CRYPTOSYM_SW
phCryptoSym_Sw_DataParams_t        sCryptoSym;         /* Crypto Sym component */
#endif /* NXPBUILD__PH_CRYPTOSYM_SW */

#ifdef NXPBUILD__PH_CRYPTORNG_SW
phCryptoRng_Sw_DataParams_t        sCryptoRng;         /* Crypto Rng component */
#endif /* NXPBUILD__PH_CRYPTORNG_SW */

/* ATR Response or ATS Response holder */
#if defined(NXPBUILD__PHPAL_I14443P4A_SW) || defined(NXPBUILD__PHPAL_I18092MPI_SW)
uint8_t    aResponseHolder[64];
#endif

#ifdef NXPBUILD__PHHAL_HW_TARGET
/* Parameters for L3 activation during Autocoll */
extern uint8_t  sens_res[2]    ;
extern uint8_t  nfc_id1[3]     ;
extern uint8_t  sel_res        ;
extern uint8_t  nfc_id3        ;
extern uint8_t  poll_res[18]   ;
#endif /* NXPBUILD__PHHAL_HW_TARGET */

/*HAL variables*/
uint8_t                            bHalBufferTx[128];          /* HAL  TX buffer */
uint8_t                            bHalBufferRx[128];          /* HAL  RX buffer */

/*PAL variables*/
phKeyStore_Sw_KeyEntry_t           sKeyEntries[NUMBER_OF_KEYENTRIES];                                  /* Sw KeyEntry structure */
phKeyStore_Sw_KUCEntry_t           sKUCEntries[NUMBER_OF_KUCENTRIES];                                  /* Sw Key usage counter structure */
phKeyStore_Sw_KeyVersionPair_t     sKeyVersionPairs[NUMBER_OF_KEYVERSIONPAIRS * NUMBER_OF_KEYENTRIES]; /* Sw KeyVersionPair structure */

uint8_t                            bDataBuffer[DATA_BUFFER_LEN];  /* universal data buffer */

uint8_t                            bSak;                      /* SAK  card type information */
uint16_t                           wAtqa;                     /* ATQA card type information */
uint8_t                            releasePN512 = 0;

/* Set the key for the MIFARE (R) Classic cards. */
uint8_t Key[6] = {0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU};

/* Don't change the following line */
uint8_t Original_Key[6] = {0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU};

#define aMfClscTaskBuffer       NULL

phStatus_t initNfc()
{
    /* Perform Platform Init */
    phPlatform_Init(&sPlatform, bHalBufferTx, sizeof(bHalBufferTx), bHalBufferRx, sizeof(bHalBufferRx));

    /* Initialize the I14443-A PAL layer */
    #ifdef NXPBUILD__PHPAL_I14443P3A_SW
    phpalI14443p3a_Sw_Init( &spalI14443p3a, sizeof(phpalI14443p3a_Sw_DataParams_t), &sPlatform.sHal);
    #endif /* NXPBUILD__PHPAL_I14443P3A_SW */
    
    /* Initialize the I14443-A PAL component */
    #ifdef NXPBUILD__PHPAL_I14443P4A_SW
    phpalI14443p4a_Sw_Init( &spalI14443p4a, sizeof(phpalI14443p4a_Sw_DataParams_t), &sPlatform.sHal);
    #endif /* NXPBUILD__PHPAL_I14443P4A_SW */

    /* Initialize the I14443-4 PAL component */
    #ifdef NXPBUILD__PHPAL_I14443P4_SW
    phpalI14443p4_Sw_Init( &spalI14443p4, sizeof(phpalI14443p4_Sw_DataParams_t), &sPlatform.sHal);
    #endif /* NXPBUILD__PHPAL_I14443P4_SW */
    
    /* Initialize the Mifare PAL component */
    #ifdef NXPBUILD__PHPAL_MIFARE_SW
    phpalMifare_Sw_Init( &spalMifare, sizeof(phpalMifare_Sw_DataParams_t), &sPlatform.sHal, &spalI14443p4  );
    #endif /* NXPBUILD__PHPAL_MIFARE_SW */
    
    /* Initialize AL Mifare Classic component */
    #ifdef NXPBUILD__PHAL_MFC_SW
    phalMfc_Sw_Init( &salMFC, sizeof(phalMfc_Sw_DataParams_t), &spalMifare, &sPlatform.sKeyStore );
    #endif /* NXPBUILD__PHAL_MFC_SW */

    /* Initialize AL Mifare Ultralight component */
    #ifdef NXPBUILD__PHAL_MFUL_SW
      phalMful_Sw_Init( &salMFUL, sizeof(phalMful_Sw_DataParams_t),  &spalMifare,
    #ifdef NXPBUILD__PH_KEYSTORE_SW
            &sPlatform.sKeyStore,
    #else
            NULL,
    #endif
    #ifdef NXPBUILD__PH_CRYPTOSYM_SW
            &sCryptoSym,
    #else
            NULL,
    #endif
    #ifdef NXPBUILD__PH_CRYPTORNG_SW
            &sCryptoRng
    #else
            NULL
    #endif
            );
    #endif /* NXPBUILD__PHAL_MFUL_SW */

    /* Initialize the discover component */
    #ifdef NXPBUILD__PHAC_DISCLOOP_SW
    phacDiscLoop_Sw_Init( &sDiscLoop, sizeof(phacDiscLoop_Sw_DataParams_t), &sPlatform.sHal);
    /* Assign other layer parameters in discovery loop */
    sDiscLoop.pHalDataParams          = &sPlatform.sHal;
    #ifdef NXPBUILD__PHPAL_I14443P3A_SW
        sDiscLoop.pPal1443p3aDataParams   = &spalI14443p3a;
    #endif /* NXPBUILD__PHPAL_I14443P3A_SW */

    #ifdef NXPBUILD__PHPAL_I14443P3B_SW
        sDiscLoop.pPal1443p3bDataParams   = &spalI14443p3b;
    #endif /* NXPBUILD__PHPAL_I14443P3B_SW */

    #ifdef NXPBUILD__PHPAL_I14443P4A_SW
        sDiscLoop.pPal1443p4aDataParams   = &spalI14443p4a;
    #endif /* NXPBUILD__PHPAL_I14443P4A_SW */

    #ifdef NXPBUILD__PHPAL_I14443P4_SW
        sDiscLoop.pPal14443p4DataParams   = &spalI14443p4;
    #endif /* NXPBUILD__PHPAL_I14443P4_SW */

    #ifdef NXPBUILD__PHPAL_FELICA_SW
        sDiscLoop.pPalFelicaDataParams    = &spalFelica;
    #endif /* NXPBUILD__PHPAL_FELICA_SW */

    #ifdef NXPBUILD__PHPAL_SLI15693_SW
        sDiscLoop.pPalSli15693DataParams  = &spalSli15693;
    #endif /* NXPBUILD__PHPAL_SLI15693_SW */

    #ifdef NXPBUILD__PHPAL_I18092MPI_SW
        sDiscLoop.pPal18092mPIDataParams  = &spalI18092mPI;
    #endif /* NXPBUILD__PHPAL_I18092MPI_SW */

    #ifdef NXPBUILD__PHPAL_I18000P3M3_SW
        sDiscLoop.pPal18000p3m3DataParams = &spalI18000p3m3;
    #endif /* NXPBUILD__PHPAL_I18000P3M3_SW */

    #ifdef NXPBUILD__PHAL_I18000P3M3_SW
        sDiscLoop.pAl18000p3m3DataParams  = &salI18000p3m3;
    #endif /* NXPBUILD__PHAL_I18000P3M3_SW */

    #ifdef NXPBUILD__PHAL_T1T_SW
        sDiscLoop.pAlT1TDataParams        = &salT1T;
    #endif /* NXPBUILD__PHAL_T1T_SW */

    #if defined(NXPBUILD__PHPAL_I18092MPI_SW) || defined(NXPBUILD__PHPAL_I18092MT_SW)
        /* Assign the GI for Type A */
        sDiscLoop.sTypeATargetInfo.sTypeA_P2P.pGi       = (uint8_t *)aLLCPGeneralBytes;
        sDiscLoop.sTypeATargetInfo.sTypeA_P2P.bGiLength = bLLCPGBLength;
        /* Assign the GI for Type F */
        sDiscLoop.sTypeFTargetInfo.sTypeF_P2P.pGi       = (uint8_t *)aLLCPGeneralBytes;
        sDiscLoop.sTypeFTargetInfo.sTypeF_P2P.bGiLength = bLLCPGBLength;
    #endif

    #if defined(NXPBUILD__PHAC_DISCLOOP_TYPEA_P2P_TAGS) || defined(NXPBUILD__PHAC_DISCLOOP_TYPEA_P2P_ACTIVE)
        /* Assign ATR response for Type A */
        sDiscLoop.sTypeATargetInfo.sTypeA_P2P.pAtrRes   = aResponseHolder;
    #endif
    #if defined(NXPBUILD__PHAC_DISCLOOP_TYPEF_P2P_TAGS) ||  defined(NXPBUILD__PHAC_DISCLOOP_TYPEF212_P2P_ACTIVE) || defined(NXPBUILD__PHAC_DISCLOOP_TYPEF424_P2P_ACTIVE)
        /* Assign ATR response for Type F */
        sDiscLoop.sTypeFTargetInfo.sTypeF_P2P.pAtrRes   = aResponseHolder;
    #endif
    #ifdef NXPBUILD__PHAC_DISCLOOP_TYPEA_I3P4_TAGS
        /* Assign ATS buffer for Type A */
        sDiscLoop.sTypeATargetInfo.sTypeA_I3P4.pAts     = aResponseHolder;
    #endif /* NXPBUILD__PHAC_DISCLOOP_TYPEA_I3P4_TAGS */
    #endif /* NXPBUILD__PHAC_DISCLOOP_SW */

    Ex4_NfcRdLibInit();
    
    /* Set the generic pointer */
    pHal = &sPlatform.sHal;
return PH_ERR_SUCCESS;
}

int cartina_trovata = 0;

void taskNFC()
{
    //uint8_t     aSak;
    uint8_t     aUid[10];
    uint8_t     lenuid;    
    uint16_t    wTagsDetected = 0;
    phStatus_t  status;
    
    switch( faseNfc )
    {
        case NFC_INIT:
            phOsal_Init();
            if( initNfc() == PH_ERR_SUCCESS ) 
            {
                faseNfc = NFC_READ;
            }
        break;
        
        case NFC_READ:
        
            /* Field OFF */
            status = phhalHw_FieldOff(pHal);

            //CyDelayUs(5100);
            status = phhalHw_Wait(sDiscLoop.pHalDataParams,PHHAL_HW_TIME_MICROSECONDS, 5100);

            /* Configure Discovery loop for Poll Mode */
            status = phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_NEXT_POLL_STATE, PHAC_DISCLOOP_POLL_STATE_DETECTION);

            /* Run Discovery loop */
            status = phacDiscLoop_Run(&sDiscLoop, PHAC_DISCLOOP_ENTRY_POINT_POLL);
        
            if( (status & PH_ERR_MASK) == PHAC_DISCLOOP_DEVICE_ACTIVATED )
            {
                
                status = phacDiscLoop_GetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_TECH_DETECTED, &wTagsDetected);

                if ( wTagsDetected == PHAC_DISCLOOP_POS_BIT_MASK_A)
                {                   
                    //aSak    = sDiscLoop.sTypeATargetInfo.aTypeA_I3P3[0].aSak;                    
                    
                    flag.codice_rfid = 1;
                    // aSak 0x08 = Mifare 1K
                    // aSak 0x18 = Mifare 4K
                    // aSak 0x20 = telefono ?
                    // sSak 0x28 = mio bancomat
                    
                    lenuid  = sDiscLoop.sTypeATargetInfo.aTypeA_I3P3[0].bUidSize;
                    aUid[0] = sDiscLoop.sTypeATargetInfo.aTypeA_I3P3[0].aUid[0]; 
                    aUid[1] = sDiscLoop.sTypeATargetInfo.aTypeA_I3P3[0].aUid[1]; 
                    aUid[2] = sDiscLoop.sTypeATargetInfo.aTypeA_I3P3[0].aUid[2]; 
                    aUid[3] = sDiscLoop.sTypeATargetInfo.aTypeA_I3P3[0].aUid[3]; 
   
                    memcpy(codice_rfid, sDiscLoop.sTypeATargetInfo.aTypeA_I3P3[0].aUid, lenuid);
                    
                    status = phalMfc_Authenticate(&salMFC, 6 , PHHAL_HW_MFC_KEYA, 1, 0, aUid, lenuid);
                    
                    if ((status & PH_ERR_MASK) != PH_ERR_SUCCESS)
                    {
                        faseNfc = WHAIT_WUP;
                        break;
                    }

//                    /* Empty the bDataBuffer */
//                    memset(bDataBuffer, 0xaa , DATA_BUFFER_LEN);
//                    /* Write data to block 4 */
//                    status = phalMfc_Write(&salMFC, 4, bDataBuffer);
//                    /* Check for Status */
//                    if (status != PH_ERR_SUCCESS)
//                    {
//                        goto whait_remove_nfc;
//                    }
//                    /* Empty the bDataBuffer */
//                    memset(bDataBuffer, '\0', DATA_BUFFER_LEN);
//                    /* Read data from block 4 */
//                    status = phalMfc_Read(&salMFC, 4, bDataBuffer);
//                    if (status != PH_ERR_SUCCESS)
//                    {
//                        goto whait_remove_nfc;
//                    }          
//                    /* Read data from block 4 */
//                    status = phalMfc_Read(&salMFC, 5, bDataBuffer);
//                    if (status != PH_ERR_SUCCESS)
//                    {
//                        goto whait_remove_nfc;
//                    }     
                  
                    /* Field RESET */
                    status = phhalHw_FieldReset(pHal);

//whait_remove_nfc:                    
//                    /* Make sure that example application is not detecting the same card continuously */
//                    do
//                    {
//                        /* Send WakeUpA */
//                        status = phpalI14443p3a_WakeUpA( sDiscLoop.pPal1443p3aDataParams, sDiscLoop.sTypeATargetInfo.aTypeA_I3P3[0].aAtqa );
//                        /* Check for Status */
//                        if (status != PH_ERR_SUCCESS)
//                        {
//                            break; /* Card Removed, break from the loop */
//                        }
//                        /* Send HaltA */
//                        status = phpalI14443p3a_HaltA(sDiscLoop.pPal1443p3aDataParams);
//                        /* Delay - 5 milli seconds*/
//                        //status = phhalHw_Wait(sDiscLoop.pHalDataParams, PHHAL_HW_TIME_MILLISECONDS, 5);
//                        CyDelay(5);
//
//                    }while(1);

                }
            }            
        break;   
            
        case WHAIT_WUP:
            
            /* Send WakeUpA */
            status = phpalI14443p3a_WakeUpA( sDiscLoop.pPal1443p3aDataParams, sDiscLoop.sTypeATargetInfo.aTypeA_I3P3[0].aAtqa );
            /* Check for Status */
            if (status != PH_ERR_SUCCESS)
            {
                faseNfc = NFC_READ;
                break; /* Card Removed, break from the loop */
            }
            /* Send HaltA */
            status = phpalI14443p3a_HaltA(sDiscLoop.pPal1443p3aDataParams);
            /* Delay - 5 milli seconds*/
            //status = phhalHw_Wait(sDiscLoop.pHalDataParams, PHHAL_HW_TIME_MILLISECONDS, 5);
        
        break;
    }
}


phStatus_t Ex4_NfcRdLibInit(void)
{
    /* Device limit for Type A */
   phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_TYPEA_DEVICE_LIMIT, 1);

    /* Bailout on Type A detect */
   phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_BAIL_OUT, PHAC_DISCLOOP_POS_BIT_MASK_A );

    /* Initialize the keystore component */
   phKeyStore_Sw_Init(
       &sPlatform.sKeyStore,
        sizeof(phKeyStore_Sw_DataParams_t),
        &sKeyEntries[0],
        NUMBER_OF_KEYENTRIES,
        &sKeyVersionPairs[0],
        NUMBER_OF_KEYVERSIONPAIRS,
        &sKUCEntries[0],
        NUMBER_OF_KUCENTRIES
        );

    /* load a Key to the Store */
    /* Note: If You use Key number 0x00, be aware that in SAM this Key is the 'Host authentication key' !!! */
   phKeyStore_FormatKeyEntry( &sPlatform.sKeyStore, 1, PH_KEYSTORE_KEY_TYPE_MIFARE );

    /* Set Key Store */
   phKeyStore_SetKey( &sPlatform.sKeyStore, 1, 0, PH_KEYSTORE_KEY_TYPE_MIFARE, &Key[0], 0);

    /* Read the version of the reader IC */
   phhalHw_Rc523_ReadRegister(&sPlatform.sHal, PHHAL_HW_RC523_REG_VERSION, &bDataBuffer[0]);
    
   releasePN512 = bDataBuffer[0];  // 0x80 PN512 release 1
                                    // 0x82 PN512 release 2
   
    if( releasePN512 == 0x80 || releasePN512 == 0x82 )
    {
        phhalHw_Rc523_WriteRegister(&sPlatform.sHal, 0x02, 0x80 );
        phhalHw_Rc523_WriteRegister(&sPlatform.sHal, 0x03, 0x80 );
        phhalHw_Rc523_WriteRegister(&sPlatform.sHal, 0x04, 0x14 );
        phhalHw_Rc523_WriteRegister(&sPlatform.sHal, 0x05, 0x00 );
        
        return PH_ERR_SUCCESS;
    }
    else
    {
        return PH_ERR_READ_WRITE_ERROR;
    }
}

void nfcPowerDown()
{
    phhalHw_Rc523_WriteRegister(&sPlatform.sHal, 0x01, 0x30 );    
}

void nfcPowerUp()
{
    phhalHw_Rc523_WriteRegister(&sPlatform.sHal, 0x01, 0x00 );    
}



