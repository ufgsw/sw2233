/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdio.h>
#include <stdlib.h>

#include <complex.h>
#include "app.h"
#include "fram.h"
#include "dstring.h"
#include "lis3mdl.h"

#include "crypto.h"

//#define IN_DEBUG
#define LOG_UFG     0
#define MAX_PACKET 10
#define MAX_BUF    (MAX_PACKET * 16)

//#define SOLO_APERTURA ( (RELEASE_FW == 272) && (par.tipo_chiusura_set == chiusura_manuale) )

void aperturaChiusura();
void ledTastierino(eLed state, uint16_t timeout);
void verificaStatoPorta();
void notificationTask(int device);
void setMagnetometroPortaChiusa();
void setMagnetometroPortaAperta();
bool leggo_magnetometro();
void setDefault(int8_t evento);
void salvaLogPorta(uint8_t evento, uint8_t esecuzione);
void impostaRisveglioTraUnMinuto();
void impostoPrimoRisveglioPerChiusura();
void impostoProssimoRisveglioPerChiusura();
void outProgrammabile(eOutQuandoAttivo quando);
void verifica_indirizzamento_fr();
void verifica_chiaveCrittografica();
void verifica_parametri();
void lettura_batteria();
void uart_read();
void uart_procrx();

void uart_procrx_codice3();
void uart_procrx_codice4(int len);
void uart_procrx_codice7();
void uart_procrx_codice8();
void uart_procrx_tag();

//void invia_a_tastiera_esterna( int b1, int b2, int b3 );
void set_tastiera_esterna( int b1, int b2, int b3 , int b4, int b5);

bool salvaParametri();
bool salvaCards();
bool salvaCrypto();

//cy_stc_ble_gatt_handle_value_pair_t notificationHandle;
//cy_stc_ble_conn_handle_t appConnHandle[MAX_CONN_HANDLE];
eGapDevice gapDevice[CY_BLE_MAX_CONNECTION_INSTANCES];

uint16_t timerOff = 0;
uint16_t timerOn = 10000;
uint16_t timerNotification = 0;
uint32_t timeout_mot = 0;
uint32_t timeout_associazione = 0;
uint32_t timeout_ble = 0;
uint32_t timeout_mastercode = 0;
uint32_t timeout_disable = 0;
eMotore  faseMotore = mot_init;
uint32_t limitazioni_corrente = 0;
uint8_t  tasto_ex = 0;
uint8_t  tasto_ex_old = 0;
uint16_t timerMaster = 0;
uint16_t timerCicala = 0;
uint16_t timerNfc = 0;
uint16_t timerLed = 0;
uint16_t timerMinuti = 0;
uint16_t timerTask = 0;
uint16_t timerRitardoPortaChiusa = 0;
uint16_t timerTastoPremuto = 0;
uint16_t timer_refresh_led = 0;
uint32_t timerPortaAperta = 0;
uint16_t timerProssimaApertura = 0;
uint16_t timerLampeggio = 0;
uint16_t timerOutProgrammabile = 0;
uint16_t timer_send_event = 0;

uint8_t  pTastiera = 0;
int16_t  batteria_raw = 0;
uint8_t  batteria = 0;
uint8_t  batteria_old = 0;
uint16_t tempo_chiusura = 0;
uint8_t  orologio_ok = 0;
uint8_t  revision = 0;
uint8_t  nuovo_evento = 0;
uint8_t  bdAddress[6];

sParametri par;

sFlag flag;
uErr error;
eExe appState = exe_init;
uStatoPorta statoPorta;
uStatoPorta statoPortaOld;
uStatoPorta statoPortaBle;

sCryptoKey      crypto;
sLogSerratura   logSerratura;
sOrarioChiusura chiusureProgrammate[7];
eTipoChiusura   tipo_chiusura;

uAdv adv;
sLogUfg  logUfg;
bridge_s bridge;
wifi_s   wifi;

eLed stato_led = leds_off;
int lampeggio;
int crono = 0;

int8_t  codiceTastierino[31];
uint8   led_regs[3];
uint8_t masterCode[20];
uint8_t masterCodeRx[20];
uint8_t tastieraCode[30];
uint8_t codice_rfid[10];

uint8_t buf[MAX_BUF];
uint8_t bufrx[MAX_BUF];
uint8_t buftx[MAX_BUF];
uint8_t chiper[MAX_BUF];

sUtente utente_tmp;
sUtente utente_eliminato;

cy_en_ble_api_result_t api_result;
cy_stc_ble_gatt_handle_value_pair_t *handleValuePair;

cy_stc_rtc_config_t orologio;
cy_stc_rtc_config_t orario_apertura;

int r;
int utente_rfid;
int utente_fingerprint;
int utente_cloud;
int utente_cloud_tipo;  // 0 = modificato, 1 = nuovo, 2 = eliminato
int in_citofono = 0;
uint8_t id_risposta_cloud;
uint8_t errcode_risposta_cloud;

uint8_t codice_impronta;

uint64_t codice_web;
uint8_t cripto[16];
uint8_t mess[16];

uint8_t  tentativi_falliti = 0;
uint16_t timer_reset_tentativi = 0;
uint32_t timer_tastierino_disabilitato = 0;

uint32_t oggichiuso = 0;

cy_stc_scb_uart_context_t uart_context;
uint8_t uart_bufrx[50];
uint8_t uart_buftx[50];
int     uart_lenrx = 0;

int device_a_cui_rispondere;
int byte_liberi;

uint8_t fl_invia_tastiera_esterna = 0;
uint8_t richiesta_in_sospeso = 0;

int len_par;
int len_user;
int len_logs;
int len_orari_chiusura;
int len_crypto;
int len_logufg;
int len_bridgeid;
int len_cards;
int error_adress_fr = 0;
void verifica_indirizzamento_fr()
{
    //return;
    //   int len_tdes = sizeof(sCryptoKey);
    len_par = sizeof(sParametri);
    len_user = sizeof(utenti);
    len_logs = sizeof(sLogSerratura);
    len_orari_chiusura = sizeof(chiusureProgrammate);
    len_crypto = sizeof(sCryptoKey);
    len_logufg = sizeof(sLogUfg);
    len_bridgeid = sizeof(bridge_s);
    len_cards = sizeof( sCards );

    if (len_par >= ADDRESS_LOG_SER)
    {
        error_adress_fr = 1;
    }
    if (len_logs >= (ADDRESS_CLOSER - ADDRESS_LOG_SER))
    {
        error_adress_fr = 2;
    }
    if (len_orari_chiusura >= (ADDRESS_KEY - ADDRESS_CLOSER))
    {
        error_adress_fr = 3;
    }
    if (len_crypto >= (ADDRESS_KEY_COPY - ADDRESS_KEY))
    {
        error_adress_fr = 4;
    }
    if (len_logufg >= (ADDRESS_NEWFW - ADDRESS_LOGUFG))
    {
        error_adress_fr = 5;
    }
    if (len_user >= (ADDRESS_BRIDGEID - ADDRESS_UTENTI))
    {
        error_adress_fr = 6;
    }
    if (len_bridgeid >= (ADDRESS_CARDS - ADDRESS_BRIDGEID))
    {
        error_adress_fr = 7;
    }
    if (len_cards >= (ADDRESS_STATUS - ADDRESS_CARDS))
    {
        error_adress_fr = 8;
    }
    
    byte_liberi = (0xffff - ADDRESS_STATUS) - len_cards;
}

void verifica_chiaveCrittografica()
{
    uint8_t crc;

    crc = CRC8((unsigned char *)&crypto, sizeof(sCryptoKey) - 1);

    if (crc != crypto.crc)
    {
        // CAZZI AMARI !

        // Provo a leggere la seconda chiave crittografica dalla fr
        fram_readArray(ADDRESS_KEY_COPY, (uint8_t *)&crypto, sizeof(sCryptoKey));

        // Ricontrollo
        crc = CRC8((unsigned char *)&crypto, sizeof(sCryptoKey) - 1);

        if (crc != crypto.crc)
        {
            //  Dati corrotti !!
            error.bit.fram = 1;
        }
    }
}

void verifica_parametri()
{
    int t;
    uint8_t crc;
    uint8_t err;
    if( par.release != RELEASE_PAR && par.release > 100 )
    {
        // Devo fare qualche aggiornamento ?    
        if( par.release < 121 )
        {
            // Aggiornamento fw
            par.en_serratura  = 1;
            par.en_ble        = 1;
            par.en_buzzer_ext = 1;
            // azzero il codice padronale
            par.padronale[0] = 0;
            par.padronale[1] = 0;
            par.padronale[2] = 0;
            par.padronale[3] = 0;
            par.padronale[4] = 0;
            par.padronale[5] = 0;
            par.padronale[6] = 0;
            // inizializzo i dati delle tessere slave        
            memset( &cards, 0, sizeof( sCards )) ;
            fram_writeArray(ADDRESS_CARDS, (uint8_t *)&cards, sizeof(sCards) );
            // Azzero i log serratura
            memset(&logSerratura, 0, sizeof(sLogSerratura));
            fram_writeArray(ADDRESS_LOG_SER, (uint8_t *)&logSerratura, sizeof(sLogSerratura));
            // nuovo parametro per il rallentamento chiusura
            par.velocita_rallentamento_in_chiusura = 30;
            // nuovi parmetri per gli affittuari
            par.orario_ingresso_affittuari = 12;
            par.orario_uscita_affittuari   = 10;
        }
        else if( par.release < 122 ) // cambiamento dalla 121 alla 122
        {
            // Azzero i log serratura
            memset(&logSerratura, 0, sizeof(sLogSerratura));
            fram_writeArray(ADDRESS_LOG_SER, (uint8_t *)&logSerratura, sizeof(sLogSerratura));
            // nuovo parametro per il rallentamento chiusura
            par.release = RELEASE_PAR;
            par.velocita_rallentamento_in_chiusura = 30;
            // nuovi parmetri per gli affittuari
            par.orario_ingresso_affittuari = 12;
            par.orario_uscita_affittuari   = 10;
        }        
        else if( par.release < 123 )    // cambiamento da 122 a 123
        {
            // Azzero i log serratura
            memset(&logSerratura, 0, sizeof(sLogSerratura));
            fram_writeArray(ADDRESS_LOG_SER, (uint8_t *)&logSerratura, sizeof(sLogSerratura));
            // nuovi parmetri per gli affittuari
            par.orario_ingresso_affittuari = 12;
            par.orario_uscita_affittuari   = 10;
        }
        else // cambiamento dalla 123 alla 124
        {
            // nuovi parmetri per gli affittuari
            par.orario_ingresso_affittuari = 12;
            par.orario_uscita_affittuari   = 10;
            // nuova chiave crittografica
            crypto.secu[0] = 1;
            crypto.secu[1] = 2;
            crypto.secu[2] = 4;
            crypto.secu[3] = 7;
            crypto.secu[4] = 1;
            crypto.secu[5] = 9;
            crypto.secu[6] = 7;
            crypto.secu[7] = 2;
            
            salvaCrypto();            
        }
        par.release       = RELEASE_PAR;
        salvaParametri();
    }        

    // Le versioni vecchie non avevano il CRC
    if (par.release < 118)
        return;

    // verifico solo quelle dalla release parametri >= 118
    crc = CRC8((unsigned char *)&par, sizeof(sParametri) - 1);

    if (crc != par.crc)
    {
        // CAZZI AMARI, provo a rileggere
        fram_readArray(ADDRESS_PAR, (uint8_t *)&par, sizeof(sParametri));

        // Ricontrollo
        crc = CRC8((unsigned char *)&par, sizeof(sParametri) - 1);
        if (crc != par.crc)
        {
            // Parametri corrotti !!
            error.bit.fram = 1;
        }
    }
    
    // Verifico le chiusure programmate
    for( t = 0; t < 7; t++)
    {
        err = 0;
        if( chiusureProgrammate[t].minuti      > 59 ) ++err;
        if( chiusureProgrammate[t].minuti_stop > 59 ) ++err;
        if( chiusureProgrammate[t].ore         > 23 ) ++err;
        if( chiusureProgrammate[t].ore_stop    > 23 ) ++err;
        
        if( err != 0 )
        {
            chiusureProgrammate[t].enable = 0;
            chiusureProgrammate[t].minuti = 0;    
            chiusureProgrammate[t].minuti_stop = 0;    
            chiusureProgrammate[t].ore = 0;    
            chiusureProgrammate[t].ore_stop = 0;    
            flag.save_orari_chiusura = 1;
        }
    }        
}
extern uint8_t Seriale_initVar;
int ok = 1;
void applicationTask()
{
    
//    if( par.tipo_alimentazione == alimentazione_rete )
//    {            
//
//        if( statoPortaBle.bit.porta_chiusa )
//        {
//            setLed( led_rosso_on );    
//        }
//        else
//        {
//            setLed( led_verde_on );    
//        }
//    }
    
    switch (appState)
    {
    case exe_null:

        //         fram_wakeup();
        //
        //         fram_readArray( ADDRESS_KEY ,    (uint8_t*)&crypto,                 sizeof( sCryptoKey)   );
        //         if( crypto.release_key != RELEASE_CRYPTO )
        //         {
        //            ok = 0;
        //             setLed( led_rosso_toggle );
        //            break;
        //         }
        //         if( ok == 1 ) appState = exe_init;

        break;

    case exe_init:

        setLed(led_rosso_on);
        CyDelay(1000);
        error.val = 0;

        crypto_init();
        flag.crypto_en = 1;

        if (Lis3mdl_Init() == false)
            error.bit.magnetometro = 1;

        // verifico se ho debordato gli indirizzamenti in fram
        verifica_indirizzamento_fr();

        if (fram_init() == false)
            error.bit.fram = 1;
        else
        {
            CyDelay(50);
            fram_readArray(ADDRESS_PAR, (uint8_t *)&par, sizeof(sParametri));
            CyDelay(50);
            fram_readArray(ADDRESS_LOG_SER, (uint8_t *)&logSerratura, sizeof(logSerratura));
            CyDelay(50);
            fram_readArray(ADDRESS_CLOSER, (uint8_t *)&chiusureProgrammate, sizeof(chiusureProgrammate));
            CyDelay(50);
            fram_readArray(ADDRESS_KEY, (uint8_t *)&crypto, sizeof(sCryptoKey));
            CyDelay(50);
            fram_readArray(ADDRESS_LOGUFG, (uint8_t *)&logUfg, sizeof(sLogUfg));
            CyDelay(50);
            fram_readArray(ADDRESS_BRIDGEID, (uint8_t *)&bridge, sizeof(bridge_s) );
            CyDelay(50);
            fram_readArray(ADDRESS_CARDS, (uint8_t *)&cards, sizeof(sCards) );
            CyDelay(50);
            fram_readByte( ADDRESS_STATUS, &richiesta_in_sospeso );
            CyDelay(50);
            fram_readByte( ADDRESS_STATUS_DOOR, &statoPorta.val );
            
            statoPortaBle.val = statoPorta.val;
            
            memset(&utenti, 0, sizeof(utenti));
            readUtenti();

            // reset come nuova ?
            if (Cy_GPIO_Read(reset_fw_PORT, reset_fw_NUM) == 1)
            {
                if (Cy_GPIO_Read(reset_fw_PORT, reset_fw_NUM) == 1)
                {
                    setDefault(RESET_DA_JUMPER);
                    Cy_BLE_SetLocalName((const char *)par.localname);
                }
            }

            // ----------------------------------------------------------------------------
            verifica_parametri();
            // ----------------------------------------------------------------------------
            verifica_chiaveCrittografica();
            // ----------------------------------------------------------------------------

            if (logUfg.release != 100)
            {
                memset((void *)&logUfg, 0, sizeof(sLogUfg));
                logUfg.release = 100;

                fram_wakeup();
                fram_writeArray(ADDRESS_LOGUFG, (uint8_t *)&logUfg, sizeof(sLogUfg));
                fram_sleep();
            }
        }
        fram_sleep();

        comparatore_Stop();
        Cy_CTDAC_SetValueBuffered(vdac_CTDAC_HW, 0);

        pwm_motore_SetCompare0(950);
        //ANA_StartConvert();

        flag.tasto_premuto = 0;
        flag.apri_porta    = 0;
        flag.chiudi_porta  = 0;
        flag.ho_aperto     = 0;
        flag.ho_chiuso     = 0;

//        statoPorta.val = 0;
//        statoPorta.bit.serratura_chiusa = 1;
//        statoPorta.bit.porta_chiusa = 1;
//        statoPortaOld.val = 0;

        //CY_BLE_DIS_SERIAL_NUMBER = 2
        //api_result = Cy_BLE_DISS_SetCharacteristicValue(2, 6, par.serial_number);
        setLed(leds_off);

        // TEST AFFITTUARIO
        
//    dal 11-09-2021 ore 10
//    al 12-09-2021  ore 10
//    pin 8020
//    codice attivazione 8698-2977-7867-8413-870

//                 crypto.des[0] = 0xcb; //9A 95 80 B2 4A 9D 9A CB
//                 crypto.des[1] = 0x9a;
//                 crypto.des[2] = 0x9d;
//                 crypto.des[3] = 0x4a;
//                 crypto.des[4] = 0xb2;
//                 crypto.des[5] = 0x80;
//                 crypto.des[6] = 0x95;
//                 crypto.des[7] = 0x9a;

//                 crypto.des[0] = 252; //9A 95 80 B2 4A 9D 9A CB
//                 crypto.des[1] = 203;
//                 crypto.des[2] = 215;
//                 crypto.des[3] = 105;
//                 crypto.des[4] = 21;
//                 crypto.des[5] = 39;
//                 crypto.des[6] = 36;
//                 crypto.des[7] = 113;
//
//                 utente_tmp.affitto.anno   = 19;    // 2021, 9, 11, 10
//                 utente_tmp.affitto.mese   =  6;
//                 utente_tmp.affitto.giorno = 2;
//                 utente_tmp.affitto.ore    = 12;
//                 utente_tmp.affitto.orePermanenza = 24;
//                 sprintf( utente_tmp.tastierino, "%d", 8020 );
//        
//                 mess[0] = utente_tmp.affitto.anno;
//                 mess[1] = utente_tmp.affitto.mese;
//                 mess[2] = utente_tmp.affitto.giorno;
//                 mess[3] = utente_tmp.affitto.ore;
//                 mess[4] = utente_tmp.affitto.orePermanenza >> 8;
//                 mess[5] = utente_tmp.affitto.orePermanenza & 0xff;
//                 mess[7] = 12;
//                 mess[6] = 47;
//        
//                 crypto_des_encript( cripto, mess , 8 );
//        
//                 codice_web  = (uint64_t)cripto[0] << 56;
//                 codice_web |= (uint64_t)cripto[1] << 48;
//                 codice_web |= (uint64_t)cripto[2] << 40;
//                 codice_web |= (uint64_t)cripto[3] << 32;
//                 codice_web |= (uint64_t)cripto[4] << 24;
//                 codice_web |= (uint64_t)cripto[5] << 16;
//                 codice_web |= (uint64_t)cripto[6] << 8;
//                 codice_web |= (uint64_t)cripto[7];
//        
//        
//                // codice_web = 42996361580589903;
//        
//                 cripto[0] = (codice_web >> 56) & 0xff;
//                 cripto[1] = (codice_web >> 48) & 0xff;
//                 cripto[2] = (codice_web >> 40) & 0xff;
//                 cripto[3] = (codice_web >> 32) & 0xff;
//                 cripto[4] = (codice_web >> 24) & 0xff;
//                 cripto[5] = (codice_web >> 16) & 0xff;
//                 cripto[6] = (codice_web >>  8) & 0xff;
//                 cripto[7] = (codice_web      ) & 0xff;
//        
//                 mess[0] = 0;
//                 mess[1] = 0;
//                 mess[2] = 0;
//                 mess[3] = 0;
//                 mess[4] = 0;
//                 mess[5] = 0;
//                 mess[6] = 0;
//                 mess[7] = 0;
//        
//                 crypto_des_decript( cripto, mess , 8 );

        // Test chiusure programmate
//                chiusureProgrammate[0].enable = 1;
//                chiusureProgrammate[0].ore    = 8;
//                chiusureProgrammate[0].minuti = 0;
//                chiusureProgrammate[0].ore_stop    = 9;
//                chiusureProgrammate[0].minuti_stop = 0;
//                 
//                chiusureProgrammate[1].enable = 1;
//                chiusureProgrammate[1].ore    = 8;
//                chiusureProgrammate[1].minuti = 0;
//                chiusureProgrammate[1].ore_stop    = 9;
//                chiusureProgrammate[1].minuti_stop = 0;
//
//                chiusureProgrammate[2].enable = 1;
//                chiusureProgrammate[2].ore    = 8;
//                chiusureProgrammate[2].minuti = 0;
//                chiusureProgrammate[2].ore_stop    = 9;
//                chiusureProgrammate[2].minuti_stop = 0;
//
//                chiusureProgrammate[3].enable = 1;
//                chiusureProgrammate[3].ore    = 8;
//                chiusureProgrammate[3].minuti = 0;
//                chiusureProgrammate[3].ore_stop    = 9;
//                chiusureProgrammate[3].minuti_stop = 0;
//
//                chiusureProgrammate[4].enable = 1;
//                chiusureProgrammate[4].ore    = 8;
//                chiusureProgrammate[4].minuti = 0;
//                chiusureProgrammate[4].ore_stop    = 9;
//                chiusureProgrammate[4].minuti_stop = 0;
//
//                chiusureProgrammate[5].enable = 1;
//                chiusureProgrammate[5].ore    = 8;
//                chiusureProgrammate[5].minuti = 0;
//                chiusureProgrammate[5].ore_stop    = 9;
//                chiusureProgrammate[5].minuti_stop = 0;
//
//                chiusureProgrammate[6].enable = 1;
//                chiusureProgrammate[6].ore    = 8;
//                chiusureProgrammate[6].minuti = 0;
//                chiusureProgrammate[6].ore_stop    = 9;
//                chiusureProgrammate[6].minuti_stop = 0;

                //
        //         orologio.date      = 1;
        //         orologio.month     = 1;
        //         orologio.year      = 19;
        //         orologio.hour      = 19;
        //         orologio.min       = 58;
        //         orologio.sec       = 0;
        //         orologio.dayOfWeek = 1;
        //         Cy_RTC_SetDateAndTime( &orologio );

        // Test uscita programmabile
        //         par.output.anable        = 1;
        //         par.output.stato_attivo  = out_attivo_alto;
        //         par.output.quando_attivo = out_prima_di_aprire;
        //         par.output.ms_attivo     = 250;
        //         par.en_motore            = 0;
        //
        //         par.limitazioni_corrente = 1000;

        #ifdef TEST_APERTURE
        impostaRisveglioTraUnMinuto();
        #else
        impostoPrimoRisveglioPerChiusura();
        #endif

        #ifdef TEST_APERTURE_ALIMENTATA
        par.tipo_alimentazione = alimentazione_rete;     
        par.tipo_chiusura_set  = chiusura_manuale;          
        #endif
        
        uart_context.rxBuf = uart_bufrx;
        uart_context.txBuf = uart_buftx;

#ifdef IN_DEBUG        
        par.tipo_alimentazione = alimentazione_rete;
        par.en_buzzer_ext = 1;
        par.nuova = 1;
        cards.mastercard1[0] = 0;
        cards.mastercard1[1] = 0;
#endif        

        Seriale_Start();
        set_tastiera_esterna( 0, par.tipo_alimentazione, par.en_buzzer_ext ,0,0);

        // se è attivo il sensore di porta chiusa procedo alla memorizzazzione delle tessere master
//        if( Cy_GPIO_Read( sensore_porta_chiusa_PORT, sensore_porta_chiusa_NUM ) == 0 && Cy_GPIO_Read( citofono_PORT, citofono_NUM) == 0 )
//        {
//            CyDelay(100);
//            if( Cy_GPIO_Read( sensore_porta_chiusa_PORT, sensore_porta_chiusa_NUM ) == 0 && Cy_GPIO_Read( citofono_PORT, citofono_NUM) == 0 )
//            {
//                flag.associa_mastercard = 1;
//                timeout_associazione    = 60000;
//                appState = exe_mastercard;
//                set_tastiera_esterna( 0 , 1 , 1 ,0,0 );
//            }
//        }
//        else
//        {
//            
//        }
        
//        set_tastiera_esterna( 0, par.tipo_alimentazione, par.en_buzzer_ext ,0,0);
        
        // Abilito l'interrupt per il sensore apertura esterno
        Cy_SysInt_Init(&SysInt_port10_cfg, isrCitofono);
        NVIC_ClearPendingIRQ(SysInt_port10_cfg.intrSrc);
        NVIC_EnableIRQ(SysInt_port10_cfg.intrSrc);
        
        // TEST PER IL SETTAGGIO DEL WIFI BRIDGE
//        sprintf( (char*)wifi.ssid , "UFG Dipendenti" );
//        sprintf( (char*)wifi.pass , "wpa0916ufg" );
//        wifi.channel  = 6;
//        wifi.len_ssid = 14;
//        wifi.len_pass = 13;
//        flag.send_wifi_tobridge = 1;
        
        // flag.ble_accoppia_bridge = 1;
        // flag.send_user_tocloud = 1;
        // flag.send_par_tocloud = 1;        
        // flag.ble_accoppia_ripetitore = 1;
        // flag.ble_update_adv = 1;
        // flag.crypto_en               = 0;
        
//        flag.crypto_en             = 0;
//        flag.ble_accoppia_tastiera = 1;
//        flag.ble_update_adv        = 1;
//        par.tipo_sensore_chiusura  = uso_sensore_ble;
        
        
        // par.en_motore = 0;
        // par.output.stato_attivo = out_attivo_basso;
        // Cy_GPIO_Write(out_programmabile_PORT, out_programmabile_NUM, !par.output.stato_attivo);
        
        
        uart_buftx[0] = 0;
        uart_buftx[1] = 2;  // giorno
        uart_buftx[2] = 5;  // giorno
        uart_buftx[3] = 0;  // mese
        uart_buftx[4] = 2;  // mese
        uart_buftx[5] = 0;  // giorni permanenza
        uart_buftx[6] = 7;  // giorni permanenza
        uart_buftx[7] = 0;
        
        rollup( uart_buftx , crypto.secu, 8 , 8);
        
        rolldw( uart_buftx , crypto.secu, 8 , 8);
        
        
        lettura_batteria();        

        if (error.val == 0)
            appState = exe_run;
        else
            appState = exe_err;        
        
        break;

    case exe_run:
        
        if( fl_invia_tastiera_esterna )
        {
            set_tastiera_esterna( 0, par.tipo_alimentazione, par.en_buzzer_ext ,0 ,0 );
            fl_invia_tastiera_esterna = 0;    
        }

        if (flag.reset_scheda)
        {
            fram_wakeup();
            setDefault(RESET_DA_BLE);            
            fram_sleep();
            
            Cy_BLE_SetLocalName((const char *)par.localname);
            flag.ble_update_adv = 1;
            flag.reset_scheda = 0;
        }
        if (flag.crypto_en == 0)
        {
            timeout_associazione = 30000;
            appState = exe_disable_crypto;
            break;
        }
        if (flag.recovery == 1)
        {
            timeout_associazione = 30000;
            setLed( led_rosso_toggle );
            while( Cy_GPIO_Read( tasto_PORT, tasto_NUM ) == 0 );
            appState = exe_recovery;
            break;
        }
        // Se sbaglio il codice da tastiera per piu' di 9 volte disabilito tutto per 5 minuti ( Serve ? )
        if( tentativi_falliti > 9 )
        {
            timeout_disable = 60000 * 5; // mi fermo 5 minuti
            appState = exe_disable;
            break;
        }        
        // Leggo magnetometro
        if (flag.calibra_porta_chiusa)
            setMagnetometroPortaChiusa();
            
        // Arrivato qualche dato dalla tastiera esterna ?
        uart_read();
        
        //
        if( flag.mastercard )
        {
            timeout_associazione = 30000;
            flag.associa_rfid_slave = 1;
            flag.mastercard = 0;
            set_tastiera_esterna( 'V' , 1, par.en_buzzer_ext, 3, 3 ); 
            appState = exe_slavecard;
            break;
        }
        //    
        aperturaChiusura();

        // aggiorno lo stato dei led 
        if( par.tipo_alimentazione == alimentazione_rete )
        {                     
            if( flag.apri_porta ) 
            {
                setLed(led_verde_on);
            }
            else if( flag.chiudi_porta )
            {
                setLed(led_rosso_on);
            }
            else if( statoPorta.bit.serratura_chiusa )  
            {
                setLed(led_rosso_on);
            }
            else 
            {
                setLed(led_verde_on);
            }
        }
        // Se la serratura si sta muovendo non proseguo
        if (flag.apri_porta)
            break;
        if (flag.chiudi_porta)
            break;
        if (flag.calibra_corsa)
            break;
        
        lettura_batteria();

        if (flag.upgrade_fw)
        {
            // ADDRESS_NEWFW
            fram_wakeup();
            fram_writeByte(ADDRESS_NEWFW, 0xaa);
            fram_sleep();
            flag.upgrade_fw = 0;
            flag.start_boot = 1;
        }

        if( flag.ble_accoppia_bridge )
        {
            // Lampeggia il led verde per massimo 120 secondi in attesa del Bridge
            timeout_associazione = 120000;
            appState = exe_add_bridge;
            break;
        }
        
        if (flag.associa_rfid  )
        {
            // Lampeggia il led verde per massimo 120 secondi in attesa del codice rfid 
            timeout_associazione = 120000;
            appState             = exe_add_rfid;
            flag.ble_update_adv  = 1;
            break;
        }
        if ( flag.associa_fingerprint  )
        {
            // Lampeggia il led verde per massimo 120 secondi in attesa del fingerprint
            timeout_associazione = 120000;
            appState             = exe_add_fingerprint;
            flag.ble_update_adv  = 1;
            break;
        }

        if (flag.save_par)
        {
            flag.save_par = 0;
            salvaParametri();
        }
        if (flag.save_utenti)
        {
            flag.save_utenti = 0;
            fram_wakeup();
            writeUtenti();
            fram_sleep();
        }
        if (flag.save_log)
        {
            flag.save_log = 0;
            fram_wakeup();
            fram_writeArray(ADDRESS_LOG_SER, (uint8_t *)&logSerratura, sizeof(sLogSerratura));
            fram_sleep();
        }
        if (flag.save_orari_chiusura)
        {
            flag.save_orari_chiusura = 0;
            fram_wakeup();
            fram_writeArray(ADDRESS_CLOSER, (uint8_t *)&chiusureProgrammate, sizeof(chiusureProgrammate));
            fram_sleep();
        }
        if (flag.save_logufg)
        {
            flag.save_logufg = 0;
            fram_wakeup();
            fram_writeArray(ADDRESS_LOGUFG, (uint8_t *)&logUfg, sizeof(sLogUfg));
            fram_sleep();
        }

        // Se sono connesso col BLE non proseguo        
        if (flag.ble_connect)
        {
            break;
        }
        if( par.tipo_alimentazione == alimentazione_rete )
        {
            break;    
        }        
        
        // In attesa di addormentarmi
        if ( timer_send_event == 0 && timerOn == 0 )
        {
            if( eliminaAffittiScaduti() == true )
            {
                fram_wakeup();
                writeUtenti();
                fram_sleep();
            }
            appState = exe_enter_sleep;
        }

        break;

    case exe_add_rfid:
        // Attendo che la tastiera mi invii il codice RFID
        uart_read();
        setLed( led_verde_toggle );
        
        if( Cy_GPIO_Read( tasto_PORT, tasto_NUM ) == 0 ) timeout_associazione = 0;
        
        if( timeout_associazione == 0 ) 
        {
            // Rispondo al telefono che è NON andato a buon fine
            gapDevice[device_a_cui_rispondere].faseSend   = send_error_code;
            gapDevice[device_a_cui_rispondere].errCommand = cmd_link_rfid_user;
            gapDevice[device_a_cui_rispondere].errCode    = err_link_rfid;

            flag.associa_rfid        = 0;
            flag.associa_fingerprint = 0;
            flag.ble_update_adv      = 1;
               
            setLed( leds_off );
            appState = exe_run;
            break;
        }
        
        if( flag.associa_rfid == 0  )
        {
            setLed( leds_off );
            appState = exe_run;
            // Rispondo al telefono che è andato a buon fine
            gapDevice[device_a_cui_rispondere].faseSend   = send_error_code;
            gapDevice[device_a_cui_rispondere].errCommand = cmd_link_rfid_user;
            gapDevice[device_a_cui_rispondere].errCode    = err_ok;
            
            flag.ble_update_adv      = 1;
        }
        
        break;

    case exe_add_fingerprint:
        // Attendo che la tastiera mi invii il codice RFID
        setLed( led_verde_toggle );
        
        if( Cy_GPIO_Read( tasto_PORT, tasto_NUM ) == 0 ) timeout_associazione = 0;
        
        if( timeout_associazione == 0 ) 
        {
            // Rispondo al telefono che è NON andato a buon fine
            gapDevice[device_a_cui_rispondere].faseSend   = send_error_code;
            gapDevice[device_a_cui_rispondere].errCommand = cmd_link_finger_user;
            gapDevice[device_a_cui_rispondere].errCode    = err_link_finger;

            flag.associa_rfid        = 0;
            flag.associa_fingerprint = 0;
            flag.ble_update_adv      = 1;
            setLed( leds_off );
            appState = exe_run;
            
            break;
        }
        
        if( flag.associa_fingerprint == 0 )
        {
            setLed( leds_off );
            appState = exe_run;
            // Rispondo al telefono che è andato a buon fine
            gapDevice[device_a_cui_rispondere].faseSend   = send_error_code;
            gapDevice[device_a_cui_rispondere].errCommand = cmd_link_finger_user;
            gapDevice[device_a_cui_rispondere].errCode    = err_ok;
            
            flag.ble_update_adv      = 1;
        }
        
        break;
        
    case exe_add_bridge:
        
        setLed( led_verde_toggle );
        if( Cy_GPIO_Read( tasto_PORT, tasto_NUM ) == 0 ) timeout_associazione = 0;
        
        if( timeout_associazione == 0 ) 
        {
            // Tempo scaduto    
            // Rispondo al telefono che è NON andato a buon fine
            gapDevice[device_a_cui_rispondere].faseSend   = send_error_code;
            gapDevice[device_a_cui_rispondere].errCommand = cmd_link_finger_user;
            gapDevice[device_a_cui_rispondere].errCode    = err_link_finger;

            flag.ble_accoppia_bridge = 0;
            flag.ble_update_adv      = 1;
            setLed( leds_off );
            appState = exe_run;
        }
        if( flag.ble_accoppia_bridge == 0)
        {
            // Tot apost !
            // Invio il CID al telefono
            gapDevice[device_a_cui_rispondere].faseSend   = send_bridgeid;
            gapDevice[device_a_cui_rispondere].errCommand = 0x39;
            gapDevice[device_a_cui_rispondere].errCode    = err_ok;

            flag.ble_accoppia_bridge = 0;
            flag.ble_update_adv      = 1;
            setLed( leds_off );
            appState = exe_run;
        }
           
        break;

    case exe_disable_crypto:

        if (timerTask) break;
        
        timerTask = 50;
        
        if( timeout_associazione == 0 )
        {
            // Tempo scaduto            
            flag.crypto_en = 1;    
            
            setLed(leds_off);

            flag.apri_porta = 0;
            flag.chiudi_porta = 0;
            flag.tasto_aprichiudi = 0;
            flag.ble_accoppia_tastiera = 0;
            flag.ble_accoppia_telefono = 0;
            flag.ble_update_adv = 1;

            faseMotore = mot_attesa;
            appState   = exe_run;

            gapDevice[device_a_cui_rispondere].faseSend   = send_error_code;
            gapDevice[device_a_cui_rispondere].errCommand = cmd_exe_pair_device;
            gapDevice[device_a_cui_rispondere].errCode    = err_pair_device;
            break;
        }

        setLed(led_verde_toggle);

        if ( flag.crypto_en == 1 )
        {
            setLed(leds_off);

            flag.apri_porta = 0;
            flag.chiudi_porta = 0;
            flag.tasto_aprichiudi = 0;
            flag.ble_accoppia_tastiera = 0;
            flag.ble_accoppia_telefono = 0;
            flag.ble_update_adv = 1;

            faseMotore = mot_attesa;
            appState = exe_run;
            
            gapDevice[device_a_cui_rispondere].faseSend   = send_error_code;
            gapDevice[device_a_cui_rispondere].errCommand = cmd_exe_pair_device;
            gapDevice[device_a_cui_rispondere].errCode    = err_ok;

        }

        break;

    case exe_recovery:
        
        if (timerTask) break;        
        timerTask = 50;
        
        setLed(led_rosso_toggle);        

        if( Cy_GPIO_Read( tasto_PORT, tasto_NUM ) == 0 )
        {
            CyDelay( 100 );    
            if( Cy_GPIO_Read( tasto_PORT, tasto_NUM ) == 0 ) timeout_associazione = 0;
        }

        if( timeout_associazione == 0 )
        {
            // Tempo scaduto
            setLed(leds_off);
            CyDelay( 100 );    
            
            flag.apri_porta       = 0;
            flag.chiudi_porta     = 0;
            flag.tasto_aprichiudi = 0;
            flag.recovery         = 0;   
            
            timerOn    = 1000;
            faseMotore = mot_attesa;
            appState   = exe_run;
            break;
        }
        
        if( flag.recovery == 0 )
        {
            gapDevice[device_a_cui_rispondere].faseSend   = send_error_code;
            gapDevice[device_a_cui_rispondere].errCommand = cmd_exe_pair_device;
            gapDevice[device_a_cui_rispondere].errCode    = err_ok;
            setLed(leds_off);
            CyDelay( 100 );    
            
            flag.apri_porta       = 0;
            flag.chiudi_porta     = 0;
            flag.tasto_aprichiudi = 0;
            flag.recovery         = 0;   
            
            faseMotore = mot_attesa;
            appState   = exe_run;
            break;
        }

        if (flag.crypto_en == 0)
        {
            flag.recovery = 0;
            setLed(leds_off);
            timeout_associazione = 20000;
            appState = exe_disable_crypto;
            break;
        }
        
        break;
        
    case exe_disable:
        
        flag.serratura_disabilitata = 1;
        timerOn = 5000;
        
        setLed( led_rosso_toggle5 );
                
        if( flag.tasto_aprichiudi ) timeout_disable = 0;
        if( timeout_disable ) break;
        
        tentativi_falliti     = 0;
        flag.tasto_aprichiudi = 0;
        
        setLed( leds_off );
        
        flag.serratura_disabilitata = 0;
        appState = exe_run;
        
        break;

    case exe_enter_sleep:

        if( par.en_ble == 0 ) wdog_Stop();
        
        /* Check regulator current mode */
        if (!Cy_SysPm_SystemIsMinRegulatorCurrentSet())
        {
            /* Set regulator minimum current mode */
            Cy_SysPm_SystemSetMinRegulatorCurrent();
        }

        // Enter sleep mode
        Lis3mdl_Idle();

        pwm_motore_Disable();

        ANA_Sleep();

        controlReg_Sleep();

        Cy_GPIO_Write(lettura_corrente_PORT, lettura_corrente_NUM, 0);

        Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 0);
        Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 0);
        Cy_GPIO_Write(en_motore_avanti_PORT,   en_motore_avanti_NUM, 1);
        Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 1);

        Cy_GPIO_SetDrivemode(cs_fram_PORT, cs_fram_NUM, CY_GPIO_DM_HIGHZ);
        Cy_GPIO_SetDrivemode(cs_magnetometro_PORT, cs_magnetometro_NUM, CY_GPIO_DM_HIGHZ);
        Cy_GPIO_SetDrivemode(cs_eeprom_PORT, cs_eeprom_NUM, CY_GPIO_DM_HIGHZ);

        Cy_GPIO_SetDrivemode(spifr_miso_m_0_PORT, spifr_miso_m_0_NUM, CY_GPIO_DM_PULLDOWN );

#if LOG_UFG == 0
        Cy_GPIO_SetDrivemode(Seriale_tx_PORT, Seriale_tx_NUM, CY_GPIO_DM_PULLDOWN );
        Cy_GPIO_SetDrivemode(Seriale_rx_PORT, Seriale_rx_NUM, CY_GPIO_DM_PULLDOWN );
        Seriale_DeInit();
#else
    Seriale_PutString( "SLEEP\r\n");
#endif
    
        Cy_SysTick_Disable();
        
        flag.ble_update_adv = 1;

        appState = exe_deepsleep;
        break;

    case exe_deepsleep:
        {
            //int t1;
            //int t2;
            
            Cy_SysPm_CpuEnterDeepSleep(CY_SYSPM_WAIT_FOR_INTERRUPT);

            if (flag.apri_da_citofono)
            {
                // Rileggo l'ingresso per evitare falsi contatti o disturbi
                CyDelay(25);
                if (Cy_GPIO_Read(citofono_PORT, citofono_NUM) == 0)
                {
                    CyDelay(25);
                    if (Cy_GPIO_Read(citofono_PORT, citofono_NUM) == 0)
                    {
                        flag.apri_porta = 1;
                        salvaLogPorta(apertura_da_citofono, evento_eseguito);
                    }
                    else
                        flag.apri_da_citofono = 0;
                }
                else
                    flag.apri_da_citofono = 0;
            }
            flag.apri_da_citofono = 0;


            Cy_GPIO_SetDrivemode(cs_magnetometro_PORT, cs_magnetometro_NUM, CY_GPIO_DM_STRONG);
            verificaStatoPorta();
            Cy_GPIO_SetDrivemode(cs_magnetometro_PORT, cs_magnetometro_NUM, CY_GPIO_DM_HIGHZ);

            if (flag.tasto_aprichiudi == 1 || flag.apri_porta == 1 || flag.chiudi_porta == 1)
            {
                appState = exe_wakeup;
            }
            
            if( flag.wakeup || flag.ble_connect )
            {
                flag.wakeup = 0;
                appState = exe_wakeup;    
            }
        }
        break;

    case exe_wakeup:

        if (Cy_SysPm_SystemIsMinRegulatorCurrentSet())
        {
            /* Set regulator to normal current mode */
            Cy_SysPm_SystemSetNormalRegulatorCurrent();
        }
              
#if LOG_UFG == 0
        //Cy_GPIO_SetDrivemode(Seriale_tx_PORT, Seriale_tx_NUM, CY_GPIO_DM_PULLUP );
        //Cy_GPIO_Write(Seriale_tx_PORT, Seriale_tx_NUM, 1);
        
        Cy_GPIO_SetDrivemode(Seriale_tx_PORT, Seriale_tx_NUM, CY_GPIO_DM_STRONG );
        Cy_GPIO_SetDrivemode(Seriale_rx_PORT, Seriale_rx_NUM, CY_GPIO_DM_HIGHZ );
        
        Seriale_initVar = 0;
        Seriale_Start();        
#else
    Seriale_PutString( "WAKEUP\r\n");
#endif

        Cy_SysTick_Enable();
        
        Cy_GPIO_Write(en_motore_avanti_PORT, en_motore_avanti_NUM, 1);
        Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 1);

        Cy_GPIO_SetDrivemode(cs_fram_PORT, cs_fram_NUM, CY_GPIO_DM_STRONG);
        Cy_GPIO_SetDrivemode(cs_magnetometro_PORT, cs_magnetometro_NUM, CY_GPIO_DM_STRONG);
        Cy_GPIO_SetDrivemode(cs_eeprom_PORT, cs_eeprom_NUM, CY_GPIO_DM_STRONG);

        Cy_GPIO_Write(cs_fram_PORT, cs_fram_NUM, 1);
        Cy_GPIO_Write(cs_eeprom_PORT, cs_eeprom_NUM, 0); // non montata
        Cy_GPIO_Write(cs_magnetometro_PORT, cs_magnetometro_NUM, 1);

        Cy_GPIO_SetDrivemode(led_verde_PORT, led_verde_NUM, CY_GPIO_DM_STRONG);
        Cy_GPIO_SetDrivemode(led_rosso_PORT, led_rosso_NUM, CY_GPIO_DM_STRONG);
        // Cy_GPIO_SetDrivemode( out_programmable_PORT, out_programmable_NUM, CY_GPIO_DM_STRONG );

        Cy_GPIO_SetDrivemode( spifr_miso_m_0_PORT, spifr_miso_m_0_NUM, CY_GPIO_DM_HIGHZ );
        
        controlReg_Wakeup();

        ANA_Wakeup();
        //ANA_StartConvert();

        pTastiera = 0;
        memset(tastieraCode, 0, 30);
        
        set_tastiera_esterna(0,par.tipo_alimentazione,par.en_buzzer_ext ,0 ,0);
        CyDelay(50);
        set_tastiera_esterna(0,par.tipo_alimentazione,par.en_buzzer_ext ,0 ,0);

        if( par.en_ble == 0 ) wdog_Start();
        
        
        timerOn  = 5000;
        appState = exe_run;
        break;
    
    case exe_mastercard:
        
        uart_read();
        
        if( timeout_associazione == 0 ) 
        {
            // Scaduto il tempo
            appState = exe_run;
            set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 3 );
        }
        // attendo che venga digitato il codice 123E o 124E sul tastierino
        if( flag.associa_mastercard ) 
        {
            setLed( led_verde_rosso_toggle );
            break;
        }
        else
        {
            setLed( led_verde_toggle );
        }
        
        if( flag.associa_rfid_master1 == 0 && flag.associa_rfid_master2 == 0 )
        {
            // codice memorizzato   
            appState = exe_run;
            set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 3 ); 
            salvaCards();
            setLed( leds_off );
        }
        
        break;
    
    case exe_slavecard:
    
        uart_read();
        
        setLed( led_verde_toggle );
        
        aperturaChiusura();
        
        if( timeout_associazione == 0 ) 
        {
            // Scaduto il tempo, salvo e esco
            set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 4 );
            salvaParametri();
            salvaCards();            
            setLed( leds_off );
            appState = exe_run;    
        }
        if( flag.associa_rfid_slave == 0 )
        {
            set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 4 ); 
            salvaParametri();
            salvaCards();            
            setLed( leds_off );
            appState = exe_run;    
        }
        
        break;
        
        
    case exe_err:

        if (error.bit.magnetometro)
        {
            // led acceso una volta al secondo
            if (timerTask < 100)
                setLed(led_rosso_on);
            else
                setLed(leds_off);
        }
        else if (error.bit.fram)
        {
            // led acceso due volte al secondo
            if (timerTask < 100)
                setLed(led_rosso_on);
            else if (timerTask < 200)
                setLed(leds_off);
            else if (timerTask < 300)
                setLed(led_rosso_on);
            else
                setLed(leds_off);
        }
        else if (error.bit.wco)
        {
            // led acceso tre volte al secondo
            if (timerTask < 100)
                setLed(led_rosso_on);
            else if (timerTask < 200)
                setLed(leds_off);
            else if (timerTask < 300)
                setLed(led_rosso_on);
            else if (timerTask < 400)
                setLed(leds_off);
            else if (timerTask < 500)
                setLed(led_rosso_on);
            else
                setLed(leds_off);
        }

        if (timerTask)
            break;
        timerTask = 1000;
        break;
    }
}

void uart_read()
{
    uint8_t ch;
    while( Cy_SCB_UART_GetNumInRxFifo(Seriale_HW) )
    {
        ch = Cy_SCB_UART_Get(Seriale_HW);
        if( ch == ':' )
        {
            uart_lenrx = 0;
        }
        else if( ch == 0x0d )
        {
            uart_procrx();
            memset( uart_bufrx, 0, uart_lenrx);
        }
        else if( uart_lenrx < 50 )
        {
            uart_bufrx[ uart_lenrx++ ] = ch;    
        }
    }            
}

void uart_procrx()
{
    //int ut  = 0;
    int ln  = 0;
    //int pos = 0;
    eErrorCode err_code = err_ok;
    uint64_t u64;
    uint16_t ui16;
    uint8_t  tmp[20];
    
    if( parseString( (char*)uart_bufrx ) == err_ok )
    {
        if( uart_bufrx[0] == '1' )
        {
            // Codice di tastierino    
            memset(tastieraCode,   0,  30);
            ln = atoi(campo(1));
            memcpy( tastieraCode, campo(2), ln );
            
            if( ln == 1 && tastieraCode[0] == 'E' )
            {
                flag.chiudi_porta = 1;    
                //salvoLogUtente(ut, chiusura_da_tastierino, evento_eseguito);
                salvaLogPorta(chiusura_da_tastierino, evento_eseguito);
            }  
            else if( ln == 3 )
            {
                timeout_associazione = 60000;
                uart_procrx_codice3();
                return;
            }
            else if( ln > 3 && ln < 7 )
            {
                uart_procrx_codice4(ln);
                return;
            }
            else if( ln == 7 )
            {
                uart_procrx_codice7();
                return;
            }
            else if( ln == 8 )
            {
                uart_procrx_codice8();
                return;
            }
            else if(ln > 8 && ln < 30)
            {
                // Aggiungo un affittuario creato sulla piattaforma WEB
                u64 = strtoull( (const char*)tastieraCode,0,10);
                
                tmp[0] = (u64 >> 56) & 0xff;
                tmp[1] = (u64 >> 48) & 0xff;
                tmp[2] = (u64 >> 40) & 0xff;
                tmp[3] = (u64 >> 32) & 0xff;
                tmp[4] = (u64 >> 24) & 0xff;
                tmp[5] = (u64 >> 16) & 0xff;
                tmp[6] = (u64 >>  8) & 0xff;
                tmp[7] = (u64      ) & 0xff;
                
                crypto_des_decript(tmp, mess, 8);

                memset(&utente_tmp, 0, sizeof(sUtente));

                utente_tmp.tipo     = utente_affittuario;
                utente_tmp.permessi = permesso_tastierino;

                utente_tmp.affitto.anno   = mess[0];
                utente_tmp.affitto.mese   = mess[1];
                utente_tmp.affitto.giorno = mess[2];
                utente_tmp.affitto.ore    = mess[3];
                utente_tmp.affitto.orePermanenza  = (uint16_t)mess[4] << 8;
                utente_tmp.affitto.orePermanenza |= (uint16_t)mess[5];

                ui16  = (uint16_t)mess[6] << 8;
                ui16 += (uint16_t)mess[7];

                // faccio un controllo
                if (mess[0] < 19 || mess[0] > 99)
                    err_code = err_nodata;
                if (mess[1] < 1 || mess[1] > 12)
                    err_code = err_nodata;
                if (mess[2] < 1 || mess[2] > 31)
                    err_code = err_nodata;
                if (mess[3] < 1 || mess[3] > 24)
                    err_code = err_nodata;
                if (utente_tmp.affitto.orePermanenza == 0)
                    err_code = err_nodata;
                if (ui16 < 1000)
                    err_code = err_nodata;
                if (ui16 > 9999)
                    err_code = err_nodata;

                if (err_code == err_ok) 
                {
                    sprintf(utente_tmp.tastierino, "%u", ui16);
                    sprintf(utente_tmp.nome, "Affitto");

                    if (addUtente(utente_tmp) != err_ok)
                        err_code = err_operazione_non_permessa;
                    else
                    {
                        flag.save_utenti = 1;
                    }
                }
            }    
            else
            {
                //set_tastiera_esterna( 'R' , par.tipo_alimentazione, 1, 4, 1 );        
                err_code = err_nodata;
            }
        }
        else if( uart_bufrx[0] == '2' )
        {
            // Codice di TAG    
            uart_procrx_tag();
            return;
        }
    }
    else
        err_code = err_nodata;
    
    // Accendo il led sulla tastiera esterna
    if( err_code == err_ok )
    {
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext ,5 ,1 );
    }
    else
    {
        set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext ,5 ,1);
    }
    
}

void uart_procrx_codice3()
{
    //int al = par.tipo_alimentazione;
    uint8_t masterver[10];
    
    masterver[0] = 'm';
    masterver[1] = 'a';
    masterver[2] = 's';
    masterver[3] = 't';
    masterver[4] = 'e';
    masterver[5] = 'r';
    masterver[6] = 0;
    masterver[7] = 0;
    masterver[8] = 0;
    masterver[9] = 0;    
            
    // codice di tre cifre utilizzato nell'applicazione senza app
    
    // L'apertura da cantiere deve funzionare finchè non è stato creato un utente
    if( memcmp( tastieraCode, "123", 3 ) == 0 ) 
    {
        // apertura da cantiere senza app 
        if( par.en_ble == 0 ) 
        {
            if( par.nuova )
            {
                flag.apri_porta = 1;
                set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
            }
            else
                set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 );    
        }
        // apertura da cantiere con app 
        else  
        {
            if( utenti_registrati == 0 )
            {
                flag.apri_porta = 1;
                set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
            }
            else
                set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 );    
        }
        return;
    }
    
    if( par.en_ble )
    {
        // Versione con APP   
        // I settaggi devono funzionare finchè non è stata cambiata la parola master
        if( memcmp( crypto.mastercode, masterver, 10 ) != 0 )
        {
            set_tastiera_esterna( 'R' , par.tipo_alimentazione,  par.en_buzzer_ext , 4, 1 );    
            return;
        }
    }
    else
    {
        // Versione senza APP
        if( par.nuova == 0 && flag.associa_rfid_slave == 0 )
        {
            set_tastiera_esterna( 'R' , par.tipo_alimentazione,  par.en_buzzer_ext , 4, 1 );    
            return;
        }
    }
    
    if( memcmp( tastieraCode, "011", 3 ) == 0 ) 
    {
        // Seleziona alimentazione a batteria
        par.tipo_alimentazione = alimentazione_batteria;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
        salvaParametri();
    }
    else if( memcmp( tastieraCode, "021", 3 ) == 0 ) 
    {
        // Seleziona alimentazione di rete
        par.tipo_alimentazione = alimentazione_rete;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
        salvaParametri();
    }
    else if( memcmp( tastieraCode, "031", 3 ) == 0 ) 
    {
        // Seleziona porta con apertura a destra
        par.direzione_apertura = apertura_destra;
        flag.calibra_corsa     = 1;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
        salvaParametri();
    }
    else if( memcmp( tastieraCode, "041", 3 ) == 0 ) 
    {
        // Seleziona porta con apertura a sinistra
        par.direzione_apertura = apertura_sinistra;
        flag.calibra_corsa     = 1;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
        salvaParametri();
    }
    else if( memcmp( tastieraCode, "051", 3 ) == 0 ) 
    {
        // Seleziona apertura con scrocchio
        par.tipo_apertura = apertura_con_scrocchio;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
        salvaParametri();
    }
    else if( memcmp( tastieraCode, "061", 3 ) == 0 ) 
    {
        // Seleziona apertura senza scrocchio
        par.tipo_apertura = apertura_senza_scrocchio;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
        salvaParametri();
    }
    else if( memcmp( tastieraCode, "071", 3 ) == 0 ) 
    {
        // Seleziona la chiusura manuale
        par.tipo_chiusura_set     = chiusura_manuale;
        par.tipo_sensore_chiusura = uso_niente;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
        salvaParametri();
    }
    else if( memcmp( tastieraCode, "081", 3 ) == 0 ) 
    {
        // Seleziona la chiusura automatica
        par.tipo_chiusura_set     = chiusura_automatica;
        par.tipo_sensore_chiusura = uso_sensore_esterno;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
        salvaParametri();
    }
    else if( memcmp( tastieraCode, "091", 3 ) == 0 ) 
    {
        // Seleziona la chiusura  semi automatica
        par.tipo_chiusura_set     = chiusura_semiatomatica;
        par.tipo_sensore_chiusura = uso_sensore_esterno;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
        salvaParametri();
    }
    else if( memcmp( tastieraCode, "101", 3 ) == 0 ) 
    {
        // 
        par.en_serratura = 1;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
        salvaParametri();
    }
    else if( memcmp( tastieraCode, "130", 3 ) == 0 ) 
    {
        par.en_citofono = 0;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
        salvaParametri();
    }
    else if( memcmp( tastieraCode, "131", 3 ) == 0 ) 
    {
        par.en_citofono = 1;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
        salvaParametri();
    }
    else if( memcmp( tastieraCode, "280", 3 ) == 0 ) 
    {
        // Disabilito la tessera master 1
        cards.en_mastercard1 = 0;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
    }
    else if( memcmp( tastieraCode, "281", 3 ) == 0 ) 
    {
        // Abilito la tessera master 1
        cards.en_mastercard1 = 1;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
    }
    else if( memcmp( tastieraCode, "290", 3 ) == 0 ) 
    {
        // Disabilito la tessera master 2
        cards.en_mastercard2 = 0;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
    }
    else if( memcmp( tastieraCode, "291", 3 ) == 0 ) 
    {
        // Aabilito la tessera master 2
        cards.en_mastercard2 = 1;
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
    }
    else if( memcmp( tastieraCode, "456", 3 ) == 0 ) 
    {
        // Rimetto come nuova e disabilito il bluetooth
        set_tastiera_esterna( 'V' , 1 , par.en_buzzer_ext , 4, 1 ); 
        setDefault(RESET_DA_CODICE);
        par.en_ble = 0;
        salvaParametri();
        flag.associa_rfid_slave = 0;
        set_tastiera_esterna( '0' , par.tipo_alimentazione , par.en_buzzer_ext, 0, 0 );
    }
    else if( memcmp( tastieraCode, "789", 3 ) == 0 ) 
    {
        // Rimetto come nuova e abilito il bluetooth
        set_tastiera_esterna( 'V' , 1 , par.en_buzzer_ext, 4, 1 ); 
        setDefault(RESET_DA_CODICE);
        flag.associa_rfid_slave = 0;        
        set_tastiera_esterna( '0' , par.tipo_alimentazione , par.en_buzzer_ext, 0, 0 );
    }
    else if( memcmp( tastieraCode, "321", 3 ) == 0 ) 
    {
        // se è nuova e non ho mai memorizzato nessuna maser ne accoppio una
        if( par.nuova && cards.mastercard1[0] == 0 && cards.mastercard1[1] == 0 )
        {
            flag.associa_rfid_master1 = 1;
            timeout_associazione      = 60000;
            appState                  = exe_mastercard;
            set_tastiera_esterna( 'V' , 1, par.en_buzzer_ext, 4, 1 ); 
        }
        else
            set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 4, 1 ); 
    }
    else if( memcmp( tastieraCode, "421", 3 ) == 0 ) 
    {
        // Inizio accoppiamento mastercard 
        if( flag.associa_mastercard )
        {
            flag.associa_rfid_master2 = 1;
            flag.associa_mastercard   = 0;
            set_tastiera_esterna( 'A' , 1, par.en_buzzer_ext , 0, 0 ); 
        }
    }    
    else
    {
        set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext , 4, 1 );    
    }
}
void uart_procrx_codice4(int len)
{
    int ut  = 0;
    int ln  = len;
    int pos = 0;
    // -------------------------------
    // -- APP SOLO CON CARD
    // -------------------------------
    if( par.en_ble == 0 )   
    {
        // devo memorizzare un codice nelle slavecard ?
        if( appState == exe_slavecard )
        {
            // il codice è gia esistente ?
            if( findCode( (char *)tastieraCode, &ut ) == true )
            {
                set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
                return;    
            }
            // lo memorizzo
            pos = 0;
            while( pos < MAX_CARDS )
            {
                if( cards.slave[pos].en == 0 )
                {
                    memset( cards.slave[pos].code, 0 , 10 );
                    memcpy( cards.slave[pos].code, tastieraCode, ln );        
                    cards.slave[pos].en   = 1;
                    cards.slave[pos].tipo = card_tastierino;
                    pos = MAX_CARDS;
                    set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
                    par.nuova = 0;  // Esco dalla modalita cantiere !
                }
                ++pos;
            }
        }
        else 
        {
            if( par.en_serratura == 0 ) 
            {
                set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext , 5 , 2 ); 
                return;
            }            
            // cerco i codici nelle cards
            if( findCard( (char*)tastieraCode, &ut) == true )
            {
                flag.apri_porta = 1;    
                set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext , 5 , 1 ); 
                CyDelay(500);
            }
            else
            {
                set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext , 5 , 1 ); 
            }
        }
        return;
    }
    // -------------------------------
    // -- APP CON UTENTI E BLE ATTIVO             
    // -------------------------------
    else
    {
        // Codice apertura porta
        // di chi è il codice ?
        if (verificaCodiceApertura((char *)tastieraCode, &ut) == true)
        {
            // può aprire ?
            if (verificaPermessoApertura(ut, permesso_tastierino) == err_ok)
            {
                tentativi_falliti = 0;
                ++utenti[ut].aperture;
                salvoLogUtente(ut, apertura_da_tastierino, evento_eseguito);

                ++logUfg.totale_aperture;
                flag.send_event_tocloud = 1;
                flag.save_logufg = 1;
                flag.apri_porta = 1;
                set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext , 5 , 1 ); 
            }
            else
            {
                salvoLogUtente(ut, apertura_da_tastierino, evento_non_eseguito);
                flag.send_event_tocloud = 1;
                ++tentativi_falliti;
                set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext , 5 , 1 ); 
            }
            fram_wakeup();
            writeUtente(ut, &utenti[ut]);
            fram_sleep();
        }
        else
        {
            ++tentativi_falliti;
            set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext , 5 , 1 ); 
        }
    }
}
void uart_procrx_codice7()
{
    int pos;

    if( appState == exe_slavecard )
    {
        if( memcmp( tastieraCode, "1357911", 7 ) == 0 )
        {
            // Eliminazione di tutte le slave card    
            for( pos = 0 ; pos < MAX_CARDS; pos++)
            {
                if( cards.slave[pos].tipo == card_rfid )
                {
                    cards.slave[pos].code[0] = 0;    
                    cards.slave[pos].code[1] = 0;    
                    cards.slave[pos].code[2] = 0;    
                    cards.slave[pos].code[3] = 0;    
                    cards.slave[pos].code[4] = 0;    
                    cards.slave[pos].code[5] = 0;    
                    cards.slave[pos].code[6] = 0;    
                    cards.slave[pos].code[7] = 0;    
                    cards.slave[pos].code[8] = 0;    
                    cards.slave[pos].code[9] = 0;    
                    cards.slave[pos].en      = 0;
                    cards.slave[pos].tipo    = 0;
                    cards.memorizzati        = cards.memorizzati - 1;
                }
            }
            set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 );
            return;
        }
        if( memcmp( tastieraCode, "2468022", 7 ) == 0 )
        {
            // Eliminazione di tutti i codici tastierino
            for( pos = 0 ; pos < MAX_CARDS; pos++)
            {
                if( cards.slave[pos].tipo == card_tastierino )
                {
                    cards.slave[pos].code[0] = 0;    
                    cards.slave[pos].code[1] = 0;    
                    cards.slave[pos].code[2] = 0;    
                    cards.slave[pos].code[3] = 0;    
                    cards.slave[pos].code[4] = 0;    
                    cards.slave[pos].code[5] = 0;    
                    cards.slave[pos].code[6] = 0;    
                    cards.slave[pos].code[7] = 0;    
                    cards.slave[pos].code[8] = 0;    
                    cards.slave[pos].code[9] = 0;    
                    cards.slave[pos].en      = 0;
                    cards.slave[pos].tipo    = 0;
                    cards.memorizzati        = cards.memorizzati - 1;
                }
            }
            set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
            return;
        }
        set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
        return;
    }    
    
    // verifico se è un codice di sistema
    if( memcmp( tastieraCode, "1234567", 7 ) == 0 ) 
    {
        par.en_buzzer_ext = 0;
        salvaParametri();
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
    }
    else if( memcmp( tastieraCode, "7654321", 7 ) == 0 ) 
    {
        par.en_buzzer_ext = 1;
        salvaParametri();
        set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
    }
    else if( memcmp( tastieraCode, "1234321", 7 ) == 0 )
    {
        flag.recovery = 1;    
    }
    else
    {
        set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
    }

}
void uart_procrx_codice8()
{
    cy_stc_rtc_config_t orologio;
    Cy_RTC_GetDateAndTime(&orologio);
    
    uint8_t cod[8];
    eErrorCode err_code = err_ok;
    uint16_t p;
    // -------------------------------
    // -- APP SOLO CON CARD
    // -------------------------------
    if( par.en_ble == 0 )   
    {
        if( appState == exe_slavecard )
        {
            // Memorizzo il padronale 
            memcpy( par.padronale, tastieraCode, 8 );
            set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
        }
        else
        {
            if( memcmp( tastieraCode, par.padronale, 8 ) == 0 )
            {
                set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
                par.en_serratura ^= 1;
                salvaParametri();
            }
            else
                set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
        }
    }
    else
    {
        // Possibile codice affittuario    
        cod[0] = tastieraCode[0] - 0x30;
        cod[1] = tastieraCode[1] - 0x30;
        cod[2] = tastieraCode[2] - 0x30;
        cod[3] = tastieraCode[3] - 0x30;
        cod[4] = tastieraCode[4] - 0x30;
        cod[5] = tastieraCode[5] - 0x30;
        cod[6] = tastieraCode[6] - 0x30;
        cod[7] = tastieraCode[7] - 0x30;
        
        rolldw( cod, crypto.secu,8,8);
        
        if( cod[0] == 0 && cod[7] == 0 )
        {
            // Potrebbe essere valido !   
            
            utente_tmp.tipo     = utente_affittuario;
            utente_tmp.permessi = permesso_tastierino;

            // faccio un controllo
           
            if( cod[1] > 3 ) err_code = err_nodata;
            if( cod[3] > 1 ) err_code = err_nodata;

            if (err_code == err_ok) 
            {
                p = (cod[5] * 10 ) + cod[6];    // giorni peranenza
                
                utente_tmp.affitto.giorno = (cod[1] * 10 ) + cod[2];
                utente_tmp.affitto.mese   = (cod[3] * 10 ) + cod[4];                
                utente_tmp.affitto.ore    = par.orario_ingresso_affittuari;
                utente_tmp.affitto.orePermanenza = p * 24;
                
                if( orologio.month < utente_tmp.affitto.mese ) utente_tmp.affitto.anno = orologio.year;
                else if( orologio.month == utente_tmp.affitto.mese )
                {
                    if( orologio.date <= utente_tmp.affitto.giorno ) utente_tmp.affitto.anno = orologio.year;
                    else utente_tmp.affitto.anno = orologio.year +1;
                }
                else
                {
                    utente_tmp.affitto.anno = orologio.year +1;   
                }
                if( par.orario_ingresso_affittuari > par.orario_uscita_affittuari ) 
                {
                    utente_tmp.affitto.orePermanenza += par.orario_ingresso_affittuari - par.orario_uscita_affittuari;
                }
                utente_tmp.tastierino[0] = tastieraCode[1];
                utente_tmp.tastierino[1] = tastieraCode[2];
                utente_tmp.tastierino[2] = tastieraCode[5];
                utente_tmp.tastierino[3] = tastieraCode[6];
                utente_tmp.tastierino[4] = 0;
                utente_tmp.tastierino[5] = 0;
                
                sprintf(utente_tmp.nome, "Affitto");

                if (addUtente(utente_tmp) != err_ok)
                {
                    err_code = err_operazione_non_permessa;
                    set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
                }
                else
                {
                    flag.save_utenti = 1;
                    set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
                }
            }
            else
            {
                set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
            }
        }        
    }    
}

void uart_procrx_tag()
{
    int ln;
    int ut;
    int pos;
    
    ln = atoi(campo(1));
    ut = 0;
    memset(codice_rfid,   0,  10);
    while(ln)
    {
        codice_rfid[ut] = atoi( campo(ut+2));                        
        ++ut;
        --ln;
    }
    // Sto' cercando di memorizzare la tessera MASTER ?
    if( appState == exe_mastercard )
    {
        if( flag.associa_rfid_master1 )  // mi assicuro che sia stato premuto 321E sulla tastiera
        {
            ln = atoi(campo(1));
            ut = 0;
            while( ut < ln )
            {
                cards.mastercard1[ut] = codice_rfid[ut];    
                cards.en_mastercard1  = 1;
                ++ut;
            }
            flag.associa_rfid_master1 = 0;
        }
        else if( flag.associa_rfid_master2 )  // mi assicuro che sia stato premuto 421E sulla tastiera
        {
            ln = atoi(campo(1));
            ut = 0;
            while( ut < ln )
            {
                cards.mastercard2[ut] = codice_rfid[ut];    
                cards.en_mastercard2  = 1;
                ++ut;
            }
            flag.associa_rfid_master2 = 0;
        }
        return;
    }
    // Stò cercando di memorizzare una tessera Slave ?
    else if( appState == exe_slavecard )
    {
        ln = atoi(campo(1));
        // se è la mastercard devo terminar le associazioni slave
        if( strlen( (const char*)cards.mastercard1 ) == (unsigned int)ln )
        {
            if( memcmp( codice_rfid, cards.mastercard1, ln ) == 0 )
            {
                flag.associa_rfid_slave = 0;
                return;
            }
        }
        // se è la mastercard devo terminar le associazioni slave
        if( strlen( (const char*)cards.mastercard2 ) == (unsigned int)ln )
        {
            if( memcmp( codice_rfid, cards.mastercard2, ln ) == 0 )
            {
                flag.associa_rfid_slave = 0;
                return;
            }
        }
        // altrmimenti memorizzo come slave
        pos = 0;
        // posso registrarne ancora ?
        if( cards.memorizzati == MAX_CARDS )
        {
            set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 4 ); 
            return;
        }
        // la card è gia stata registrata ?
        if( findCard( (char*)codice_rfid, &ut ) == true )
        {
            set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
            timeout_associazione = 60000;
            return;
        }
        // altrmienti la registro
        while( pos < MAX_CARDS )
        {
            if( cards.slave[pos].en == 0 )
            {
                // trovata una posizione per memorizzare la card
                memset( cards.slave[pos].code, 0 , 10 );
                ln = atoi(campo(1));
                ut = 0;
                while( ut < ln )
                {
                    cards.slave[pos].code[ut] = atoi( campo(ut+2));    
                    cards.slave[pos].en   = 1;
                    cards.slave[pos].tipo = card_rfid;
                    ++ut;
                }                        
                ++cards.memorizzati;
                pos = MAX_CARDS;                        
                par.nuova = 0;  // Esco dalla modalita cantiere !
                set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 2 ); 
                timeout_associazione = 60000;
            }
            ++pos;
        }
    return;
    }
    // Devo associarla ad un utente ?
    else if( flag.associa_rfid )
    {       
        if( campi.len > 5 && campi.len < 10 )
        {
            ln = atoi(campo(1));
            ut = 0;
            while( ut < ln )
            {
                utenti[utente_rfid].rfid[ut] = atoi( campo(ut+2));    
                ++ut;
            }
            
            utenti[utente_rfid].permessi |= permesso_rfid;

            fram_wakeup();
            if (writeUtente(utente_rfid, &utenti[utente_rfid] ) == false) set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 1 ); 
            else                                                          set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext , 5, 1 );
            fram_sleep();            
        }
        flag.associa_rfid = 0;
    }        
    else
    {
        ut = 0;
        ln = atoi(campo(1));
        // è la mastercard 1 ?
        if( strlen( (const char*)cards.mastercard1 ) == (unsigned int)ln )
        {
            if( memcmp( codice_rfid, cards.mastercard1, ln ) == 0 )
            {
                //
                if( cards.en_mastercard1 == 1 ) flag.mastercard = 1;
                else
                    set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 1 ); 
                return;
            }
        }                
        // è la mastercard 2 ?
        if( strlen( (const char*)cards.mastercard2 ) == (unsigned int)ln )
        {
            if( memcmp( codice_rfid, cards.mastercard2, ln ) == 0 )
            {
                //
                if( cards.en_mastercard2 == 1 ) flag.mastercard = 1;
                else
                    set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 1 ); 
                return;
            }
        }                
        if( par.en_ble == 0 )   // applicazione solo con card ?
        {
            if( par.en_serratura == 0 ) 
            {
                set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext , 5 , 2 ); 
                return;
            }

            if( par.nuova == 1 )
            {
                // Se sono in modalita cantiere tutti i trasponder Aprono
                flag.apri_porta = 1;    
                set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext , 5, 1 );
            }
            else
            {
                // è una slavecard ?
                if( findCard( (char*)codice_rfid, &ut) == true )
                {
                    flag.apri_porta = 1;
                    set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext , 5, 1 );
                }
                else
                {
                    set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext , 5, 1 );
                }
            }
            return;
        }
        else
        {
            if( utenti_registrati == 0 )
            {
                // Se sono in modalita cantiere tutti i trasponder Aprono
                flag.apri_porta = 1;    
                set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext , 5, 1 );
                return;
            }
            // di chi è il codice ?
            if (verificaCodiceRfidApertura((char *)codice_rfid, &ut) == true)
            {
                // può aprire ?
                if (verificaPermessoApertura(ut, permesso_tastierino) == err_ok)
                {
                    ++utenti[ut].aperture;
                    salvoLogUtente(ut, apertura_da_nfc, evento_eseguito);

                    ++logUfg.totale_aperture;
                    flag.send_event_tocloud = 1;
                    flag.save_logufg = 1;
                    flag.apri_porta = 1;
                    set_tastiera_esterna( 'V' , par.tipo_alimentazione, par.en_buzzer_ext , 5, 1 );
                }
                else
                {
                    salvoLogUtente(ut, apertura_da_nfc, evento_non_eseguito);
                    set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 1 ); 
                    flag.send_event_tocloud = 1;
                    ++tentativi_falliti;
                }
                fram_wakeup();
                if (writeUtente(ut, &utenti[ut]) == false)
                    set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 1 ); 
                fram_sleep();
            }
            else
            {
                set_tastiera_esterna( 'R' , par.tipo_alimentazione, par.en_buzzer_ext, 3, 1 ); 
                ++tentativi_falliti;
            }
        }
    }
}


void lettura_batteria()
{
    ANA_StartConvert();
    ANA_IsEndConversion(CY_SAR_WAIT_FOR_RESULT);
    ANA_StopConvert();        
    {
        batteria_raw = ANA_GetResult16(0) + 100;

        batteria_raw = batteria_raw - livello_batteria_3_7;

        if (batteria_raw < 0)
            batteria = 0;
        else if (batteria_raw > 100)
            batteria = 100;
        else
            batteria = (uint8_t)batteria_raw;

        if (batteria != batteria_old)
            flag.ble_update_adv = 1;
        batteria_old = batteria;
    }
    if (batteria < 40)
    {
        timerOff = 20000;
    }
}

void setDefault(int8_t evento)
{
    cy_stc_rtc_config_t orologio;
    Cy_RTC_GetDateAndTime(&orologio);

    setLed(led_verde_toggle);
    CyDelay(1000);

    memset((void *)&par, 0, sizeof(sParametri));

    par.release = RELEASE_PAR;
    par.nuova   = 1;

    par.localname[0] = 'd';
    par.localname[1] = 's';
    par.localname[2] = 'm';
    par.localname[3] = 'a';
    par.localname[4] = 'r';
    par.localname[5] = 't';
    par.localname[6] = ' ';
    par.localname[7] = '2';
    par.localname[8] = ' ';
    par.localname[9] = 0;

    par.orario_ingresso_affittuari = 12;
    par.orario_uscita_affittuari   = 10;
    
//    par.serial_number[0] = '0';
//    par.serial_number[1] = '0';
//    par.serial_number[2] = '0';
//    par.serial_number[3] = '0';
//    par.serial_number[4] = '0';
//    par.serial_number[5] = '1';

    par.timezone = 100;
    
    par.en_tastiera_con_filo = 0;
    par.en_ble               = 1;
    par.en_motore            = 1;
    par.en_buzzer_ext        = 1;
    par.en_citofono          = 1;
    par.en_serratura         = 1;    
    
    par.forza_motore                 = 70; // %
    par.direzione_apertura           = apertura_sinistra;
    par.tipo_alimentazione           = alimentazione_batteria;
    par.tipo_chiusura_set            = chiusura_manuale;
    par.tipo_apertura                = apertura_con_scrocchio;
    par.tipo_sensore_chiusura        = uso_niente;
    par.tipo_movimentazione_chiusura = chiusura_con_limitazione;
    par.tempo_massimo_porta_aperta   = 10;
    par.tempo_chiusura               = 0;
    par.tempo_apertura               = 0;
    par.tempo_scrocchio              = 500;
    par.limitazioni_corrente         = 1000;
            
    par.velocita_apertura                  = 50;
    par.velocita_rallentamento_in_chiusura = 40;

    par.output.anable        = 0;
    par.output.ms_attivo     = 500;
    par.output.quando_attivo = out_prima_di_aprire;
    par.output.stato_attivo  = out_attivo_alto;

    par.crc = CRC8((unsigned char *)&par, sizeof(sParametri) - 1);
    fram_writeArray(ADDRESS_PAR, (uint8_t *)&par, sizeof(sParametri));

    memset(&logSerratura, 0, sizeof(sLogSerratura));
    fram_writeArray(ADDRESS_LOG_SER, (uint8_t *)&logSerratura, sizeof(sLogSerratura));

    memset(&chiusureProgrammate[0], 0, sizeof(chiusureProgrammate));
    fram_writeArray(ADDRESS_CLOSER, (uint8_t *)&chiusureProgrammate, sizeof(chiusureProgrammate));

    clearUtenti();

    crypto.mastercode[0] = 'm';
    crypto.mastercode[1] = 'a';
    crypto.mastercode[2] = 's';
    crypto.mastercode[3] = 't';
    crypto.mastercode[4] = 'e';
    crypto.mastercode[5] = 'r';
    crypto.mastercode[6] = 0;
    crypto.mastercode[7] = 0;
    crypto.mastercode[8] = 0;
    crypto.mastercode[9] = 0;    
    
    generate_new_key(&crypto);
    crypto.opt = 0xaa;
    crypto.crc = CRC8( (unsigned char*)&crypto, sizeof(sCryptoKey) -1 );
    fram_writeArray(ADDRESS_KEY, (uint8_t *)&crypto, sizeof(sCryptoKey));
    fram_writeArray(ADDRESS_KEY_COPY, (uint8_t *)&crypto, sizeof(sCryptoKey));

    logUfg.giorno_eas   = orologio.date;
    logUfg.mese_eas     = orologio.month;
    logUfg.anno_eas     = orologio.year;
    logUfg.evento_reset = evento;

    memset( (void*)&bridge, 0, sizeof(bridge) );
    fram_writeArray(ADDRESS_BRIDGEID, (uint8_t *)&bridge, sizeof(bridge_s) );
    
    memset( &cards, 0, sizeof( sCards )) ;
    fram_writeArray(ADDRESS_CARDS, (uint8_t *)&cards, sizeof(sCards) );
    
    flag.save_logufg = 1;

    CyDelay(1000);
    setLed(leds_off);
}

//#define LIMITAZIONI_CORRENTE par.limitazioni_corrente
#define LIMITAZIONI_CORRENTE 1000

void aperturaChiusura()
{
    int32_t power;

    if (!par.en_motore)
    {
        if (flag.apri_da_citofono)
        {
            CyDelay(25);
            if (Cy_GPIO_Read(citofono_PORT, citofono_NUM) == 0)
            {
                salvaLogPorta(apertura_da_citofono, evento_eseguito);
                flag.apri_porta = 1;
            }
            flag.apri_da_citofono = 0;
        }

        if (flag.apri_porta || flag.tasto_aprichiudi)
        {
            tentativi_falliti = 0;
            flag.apri_porta = 0;
            if (flag.tasto_aprichiudi)
            {
                flag.tasto_aprichiudi = 0;
                // Attesa rilascio
                while (Cy_GPIO_Read(tasto_PORT, tasto_NUM) == 0)
                {
                    if (flag.recovery)
                        return;
                }
                salvaLogPorta(apertura_da_serratura, evento_eseguito);
            }
            par.output.anable = 1;
            par.output.quando_attivo = out_prima_di_aprire;

            setLed(led_verde_on);
            outProgrammabile(out_prima_di_aprire);
            setLed(leds_off);

            timerOn = 1000;

            if( gapDevice[0].master_ok == 0 &&
                gapDevice[1].master_ok == 0 &&
                gapDevice[2].master_ok == 0 &&
                gapDevice[3].master_ok == 0 )
            {            
                timeout_ble = 500;
            }
            
            GenerateRandomKey(2, random_code);
        }
        return;
    }

    switch (faseMotore)
    {
    case mot_init:
        pwm_motore_Disable();
        faseMotore = mot_attesa;
        flag.apri_porta = 0;
        flag.chiudi_porta = 0;
        flag.tasto_aprichiudi = 0;
        
#if RELEASE_FW == 272
        //par.tipo_chiusura_set = chiusura_disabilitata;
#endif    
        if( richiesta_in_sospeso == 1 ) flag.apri_porta   = 1;
        if( richiesta_in_sospeso == 2 ) flag.chiudi_porta = 1;

        break;
    case mot_attesa:

        if (timeout_mot)
            break;
        timeout_mot = 100;

        verificaStatoPorta();
        
#if RELEASE_FW == 273
        if( flag.ho_aperto ) flag.chiudi_porta = 1;
        if( flag.ho_chiuso ) flag.apri_porta = 1;
        flag.ho_aperto = 0;
        flag.ho_chiuso = 0;
#endif            
        
        if( par.en_serratura == 0 ) 
        {
            flag.apri_da_citofono = 0;
            flag.apri_porta       = 0;
            flag.chiudi_porta     = 0;
            flag.tasto_aprichiudi = 0;
            break;
        }

        if (flag.tasto_aprichiudi)
        {           
            statoPortaBle.bit.nuovo_dato = 0;
            faseMotore = mot_attesa_rilascio_tasto;
        }

        if (flag.apri_da_citofono)
        {
            CyDelay(25);
            if (Cy_GPIO_Read(citofono_PORT, citofono_NUM) == 0)
            {
                salvaLogPorta(apertura_da_citofono, evento_eseguito);
                flag.apri_porta = 1;
            }
            flag.apri_da_citofono = 0;
        }

        if (flag.chiudi_porta)
        {
            tentativi_falliti = 0;
            flag.tasto_aprichiudi = 0;
            timeout_mot = 15000;
            faseMotore = mot_attesa_porta_chiusa;
        }
        else if (flag.apri_porta)
        {
            tentativi_falliti = 0;
            //
            outProgrammabile(out_prima_di_aprire);
            //
            flag.tasto_aprichiudi = 0;
            faseMotore = mot_apri;
        }

        if (flag.calibra_corsa)
        {
            // La memorizzazzione del tempo impiegato viene fatto in chiusura
            // Quindi mi devo assicurare che la serratura sia completamente aperta
            faseMotore = mot_calibra_apri;
        }

        break;

    case mot_attesa_rilascio_tasto:

        if (flag.crypto_en == 0 || flag.recovery == 1)
        {
            flag.chiudi_porta     = 0;
            flag.apri_porta       = 0;
            flag.tasto_aprichiudi = 0;
            faseMotore = mot_attesa;
            break;
        }

        if (statoPorta.bit.serratura_chiusa) setLed(led_verde_on);
        else                                 setLed(led_rosso_on);
        
        if (Cy_GPIO_Read(tasto_PORT, tasto_NUM) == 0)
            break;

        if ( RELEASE_FW == 272 && par.tipo_chiusura_set == chiusura_manuale )
        {
            flag.apri_porta   = 1;
            flag.chiudi_porta = 0; 
        }
        else    
        {
            if (statoPorta.bit.serratura_chiusa)
                flag.apri_porta = 1;
            else
                flag.chiudi_porta = 1;
        }
        
        if (flag.chiudi_porta)
        {
            outProgrammabile(out_prima_di_chiudere);
            salvaLogPorta(chiusura_da_serratura, evento_eseguito);
            flag.tasto_aprichiudi = 0;
            timeout_mot = 15000;
            faseMotore = mot_attesa_porta_chiusa;
        }
        else if (flag.apri_porta)
        {
            richiesta_in_sospeso = 2;
            fram_wakeup();
            fram_writeByte( ADDRESS_STATUS, richiesta_in_sospeso );
            fram_sleep();

            outProgrammabile(out_prima_di_aprire);
            salvaLogPorta(apertura_da_serratura, evento_eseguito);
            flag.tasto_aprichiudi = 0;
            faseMotore = mot_apri;
        }
        else 
        {
            flag.tasto_aprichiudi = 0;
            faseMotore = mot_attesa;
        }
        
        set_tastiera_esterna( 0, par.tipo_alimentazione, par.en_buzzer_ext ,0 ,0);
        
        break;

    case mot_apri:

        richiesta_in_sospeso = 1;
        fram_wakeup();
        fram_writeByte( ADDRESS_STATUS, richiesta_in_sospeso );
        fram_sleep();

        if (statoPorta.bit.serratura_chiusa == 1)
        {
            power = soglia_iniziale_dac + ((uint32_t)par.forza_motore * moltiplicatore_dac);
            Cy_CTDAC_SetValueBuffered(vdac_CTDAC_HW, power);
        }
        else
        {
            power = soglia_iniziale_dac + 300;
            Cy_CTDAC_SetValueBuffered(vdac_CTDAC_HW, power);
        }

        if (batteria > 80)
            setLed(led_verde_on);
        else
            setLed(led_verde_toggle);

        Cy_GPIO_Write(lettura_corrente_PORT, lettura_corrente_NUM, 1);

        contatore_SetCounter(0);
        contatore_Start();

        comparatore_Start();
        pwm_motore_Enable();

        pwm_motore_SetPeriod0(1000);   
        pwm_motore_SetCompare0(950);

        if (par.direzione_apertura == apertura_sinistra)
        {
            controlReg_Write(1); // 1 = pwm motore indietro
            CyDelay(10);
            Cy_GPIO_Write(en_motore_avanti_PORT,   en_motore_avanti_NUM,   1);
            Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 0);
            CyDelay(10);
        }
        else
        {
            controlReg_Write(0); // 0 = pwm motore avanti
            CyDelay(10);
            Cy_GPIO_Write(en_motore_avanti_PORT,   en_motore_avanti_NUM,   0); // 0 = enable
            Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 1); // 1 = disable
            CyDelay(10);
        }
        pwm_motore_Start();

        flag.tasto_aprichiudi = 0;
        limitazioni_corrente  = 0;
        timeout_mot = 10000;
        timerOn = 25000;
        if (par.tipo_apertura == apertura_senza_scrocchio && par.tempo_apertura != 0)
        {
            timeout_mot = par.tempo_apertura;
            faseMotore  = mot_in_apertura_a_tempo;
        }
        else
        {
            // faseMotore = mot_in_apertura;
            if( par.tempo_chiusura == 0 || statoPorta.bit.serratura_chiusa == 0 ) 
            {
                faseMotore = mot_in_apertura;
            }
            else
            {
                timeout_mot    = par.tempo_apertura;    
                faseMotore = mot_in_apertura_a_tempo_con_scrocchio;
            }
        }
        break;

    case mot_in_apertura:
        limitazioni_corrente = contatore_GetCounter();
        if (limitazioni_corrente > par.limitazioni_corrente || timeout_mot == 0) // tenere lo scrocchio lungo ( magari programmabile )
        {
            if (par.tipo_apertura == apertura_con_scrocchio)
            {
                outProgrammabile(out_dopo_scrocchio);
                timeout_mot = par.tempo_scrocchio;
                faseMotore  = mot_tenuta_scrocchio;
            }
            else
                faseMotore = mot_fine_movimento;
        }
        break;

    case mot_tenuta_scrocchio:
        if (timeout_mot)
            break;

        faseMotore = mot_uscita_scrocchio;
        break;

    case mot_in_apertura_a_tempo:
        if (timeout_mot == 0)
        {
            faseMotore = mot_fine_movimento;
            break;
        }

        limitazioni_corrente = contatore_GetCounter();
        if (limitazioni_corrente > 300 ) // messo solo 300 ms
        {
            // In apertura a tempo lo scrocco non dovrebbe essere tirato, se per caso leggo limitazioni di corrente significa che la
            // serratura era gia aperta e ormai ha tirato lo scrocco....  lo faccio uscire
            
            // E' la seconda volta che apro di seguito tengo tirato lo scrocco per aprire la porta
            // do per scontato che lo scrocco è disabilitato ma fisiccamente esiste.
            // ma che si fa se lo scrocco non esiste ?????
            
            timeout_mot    = par.tempo_scrocchio;   
            faseMotore = mot_tenuta_scrocchio;
            
            //faseMotore = mot_uscita_scrocchio;
        }
        break;
        
    case mot_in_apertura_a_tempo_con_scrocchio:
        limitazioni_corrente = contatore_GetCounter();
        if (limitazioni_corrente > 1000 ) 
        {
            // serratura era gia aperta e ormai ha tirato lo scrocco....  lo faccio uscire
            faseMotore = mot_uscita_scrocchio;
            break;
        }
        if (timeout_mot == 0)
        {
            // porto la velocita al valore programmato
            power = ((uint32)par.velocita_apertura * 950) / 100;            
            pwm_motore_SetPeriod0(1000);
            pwm_motore_SetCompare0(power);

            // porto la forza motore al massimo
            Cy_CTDAC_SetValueBuffered(vdac_CTDAC_HW, soglia_massima_dac );
            
            timeout_mot = 3000;
            limitazioni_corrente = 0;
            faseMotore = mot_in_apertura;
            break;
        }        
        break;

    case mot_attesa_porta_chiusa:
        // attendo il rilascio del tasto
        if (Cy_GPIO_Read(tasto_PORT, tasto_NUM) == 0)
            break;

        setLed(led_rosso_on);
        
        if (tipo_chiusura == chiusura_manuale || par.tipo_sensore_chiusura == uso_niente)
        {
            // Non aspetto la porta chiusa
            timeout_mot = 0;
            faseMotore = mot_chiudi;
            break;
        }
        else
        {
            verificaStatoPorta();
            if (statoPorta.bit.porta_chiusa == 1)
            {
                // Porta fisicamente chiusa, aspetto mezzo secondo prima di fare partire la serratura
                timeout_mot = 500;
                faseMotore = mot_chiudi;
            }
        }

        if (timeout_mot == 0)
        {
            // Ho aspettato troppo torno in attesa
            setLed(leds_off);
            flag.tasto_aprichiudi = 0;
            flag.apri_porta = 0;
            flag.chiudi_porta = 0;
            faseMotore = mot_attesa;
            break;
        }

        break;

    case mot_chiudi:
        if (timeout_mot)
            break;

//        richiesta_in_sospeso = 2;
//        fram_wakeup();
//        fram_writeByte( ADDRESS_STATUS, richiesta_in_sospeso );
//        fram_sleep();

        power = soglia_iniziale_dac + ((uint32_t)par.forza_motore * moltiplicatore_dac);
        Cy_CTDAC_SetValueBuffered(vdac_CTDAC_HW, power);

        if (batteria > 80)
            setLed(led_rosso_on);
        else
            setLed(led_rosso_toggle);

        Cy_GPIO_Write(lettura_corrente_PORT, lettura_corrente_NUM, 1);

        contatore_SetCounter(0);
        contatore_Start();

        comparatore_Start();
        pwm_motore_Enable();
        pwm_motore_SetCompare0(950);

        if (par.direzione_apertura == apertura_sinistra)
        {
            controlReg_Write(0); // 0 = pwm motore avanti
            CyDelay(10);
            Cy_GPIO_Write(en_motore_avanti_PORT,   en_motore_avanti_NUM,   0); // 0 = enable
            Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 1); // 1 = disable
            CyDelay(10);
        }
        else
        {
            controlReg_Write(1); // 1 = pwm motore indietro
            CyDelay(10);
            Cy_GPIO_Write(en_motore_avanti_PORT,   en_motore_avanti_NUM,   1);
            Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 0);
            CyDelay(10);
        }
        pwm_motore_Start();
        flag.tasto_aprichiudi = 0;
        limitazioni_corrente  = 0;
        timeout_mot           = 10000;

        if (par.tipo_movimentazione_chiusura == chiusura_con_limitazione)
            faseMotore = mot_in_chiusura_limitazione;
        else
        {
            timeout_mot = par.tempo_chiusura;
            if (timeout_mot == 0)
                timeout_mot = 5000;

            faseMotore = mot_in_chiusura_a_tempo;
        }
        break;

    case mot_in_chiusura_a_tempo:
        
        limitazioni_corrente = contatore_GetCounter();
        if (limitazioni_corrente > LIMITAZIONI_CORRENTE)
        {
            faseMotore = mot_fine_movimento;
        }
        if( timeout_mot < 500 )
        {
            // quando manca poco alla fine chiusura 
            // vado a sbattere con poca corrente e a bassa velocità ( Modifica Piero 02/11/2022 ) 
            if( par.velocita_rallentamento_in_chiusura <  30 ) par.velocita_rallentamento_in_chiusura =  30;
            if( par.velocita_rallentamento_in_chiusura > 100 ) par.velocita_rallentamento_in_chiusura = 100;
            
            power = 2500;
            Cy_CTDAC_SetValueBuffered(vdac_CTDAC_HW, power);            

            power = (par.velocita_rallentamento_in_chiusura * 950) / 100;   // porto la velocita al 25%
            pwm_motore_SetPeriod0(1000);
            pwm_motore_SetCompare0(power);
            
            timeout_mot    = 2500;
            faseMotore = mot_in_chiusura_limitazione;
        }
            
//        if (par.tipo_apertura == apertura_senza_scrocchio)
//        {
//            // Senza scrocco vado a sbattere per fare un azzeramento quota
//            if( timeout < 250 )
//            {
//                // quando manca poco alla fine chiusura 
//                // vado a sbattere con poca corrente e a bassa velocità ( Modifica Piero 02/11/2022 ) 
//                power = 2500;
//                Cy_CTDAC_SetValueBuffered(vdac_CTDAC_HW, power);            
//
//                power = (25 * 950) / 100;   // porto la velocita al 50%
//                pwm_motore_SetPeriod0(1000);
//                pwm_motore_SetCompare0(power);
//                
//                timeout    = 2500;
//                faseMotore = mot_in_chiusura_limitazione;
//            }
//        }
//        else
//        {
//            if (timeout == 0)
//            {
//                // Chiuso
//                faseMotore = mot_fine_movimento;
//            }
//        }
        break;

    case mot_in_chiusura_limitazione:
        limitazioni_corrente = contatore_GetCounter();
        if (limitazioni_corrente > LIMITAZIONI_CORRENTE || timeout_mot == 0)
        {
            faseMotore = mot_fine_movimento;
        }
        break;

    case mot_uscita_scrocchio:
        // Dopo la completa apertura giro per 200 ms il motore in chiusura per aiutare lo scrocchio ad uscire
        if (par.direzione_apertura == apertura_sinistra)
        {
            controlReg_Write(0);
            CyDelay(5);
            Cy_GPIO_Write(en_motore_avanti_PORT,   en_motore_avanti_NUM,   0);
            Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 1);
            CyDelay(5);
        }
        else
        {
            controlReg_Write(1); // 1 = pwm motore indietro
            CyDelay(5);
            Cy_GPIO_Write(en_motore_avanti_PORT,   en_motore_avanti_NUM,   1);
            Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 0);
            CyDelay(5);
        }

        pwm_motore_Start();
        limitazioni_corrente = 0;
        timeout_mot = 200;
        faseMotore = mot_attesa_uscita_scrocchio;
        break;

    case mot_attesa_uscita_scrocchio:
        if (timeout_mot)
            break;
        faseMotore = mot_fine_movimento;
        break;

    case mot_calibra_apri:

        // Mi assicuro che la serratura sia completamente aperta
        power = soglia_iniziale_dac + ((uint32_t)par.forza_motore * moltiplicatore_dac);
        Cy_CTDAC_SetValueBuffered(vdac_CTDAC_HW, power);

        Cy_GPIO_Write(lettura_corrente_PORT, lettura_corrente_NUM, 1);

        contatore_SetCounter(0);
        contatore_Start();

        comparatore_Start();
        pwm_motore_Enable();
        pwm_motore_SetCompare0(900);

        if (par.direzione_apertura == apertura_sinistra)
        {
            controlReg_Write(1); // 1 = pwm motore indietro
            CyDelay(5);
            Cy_GPIO_Write(en_motore_avanti_PORT, en_motore_avanti_NUM, 1);
            Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 0);
            CyDelay(5);
        }
        else
        {
            controlReg_Write(0);    // 0 = pwm motore avanti
            CyDelay(5);
            Cy_GPIO_Write(en_motore_avanti_PORT, en_motore_avanti_NUM,     0); // 0 = enable
            Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 1); // 1 = disable
            CyDelay(5);
        }
        pwm_motore_Start();

        flag.tasto_aprichiudi = 0;
        limitazioni_corrente = 0;
        timeout_mot = 10000;

        faseMotore = mot_calibra_apri_attesa;

        break;

    case mot_calibra_apri_attesa:
        // Attendo l'apertura della serratura

        limitazioni_corrente = contatore_GetCounter();
        if (limitazioni_corrente > 1500 || timeout_mot == 0)
        {
            pwm_motore_Disable();
            controlReg_Write(0);
            Cy_GPIO_Write(en_motore_avanti_PORT, en_motore_avanti_NUM, 1);
            Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 1);

            timeout_mot = 2000;
            faseMotore = mot_calibra_chiudi;
        }
        break;

    case mot_calibra_chiudi:
        if (timeout_mot)
            break;
        // Cronometro il tempo in chiusura !
        power = soglia_iniziale_dac + ((uint32_t)par.forza_motore * moltiplicatore_dac);
        Cy_CTDAC_SetValueBuffered(vdac_CTDAC_HW, power);

        contatore_SetCounter(0);
        contatore_Start();

        comparatore_Start();
        pwm_motore_Enable();
        pwm_motore_SetCompare0(950);

        if (par.direzione_apertura == apertura_sinistra)
        {
            controlReg_Write(0); // 0 = pwm motore avanti
            CyDelay(5);
            Cy_GPIO_Write(en_motore_avanti_PORT, en_motore_avanti_NUM, 0);     // 0 = enable
            Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 1); // 1 = disable
            CyDelay(5);
        }
        else
        {
            controlReg_Write(1); // 1 = pwm motore indietro
            CyDelay(5);
            Cy_GPIO_Write(en_motore_avanti_PORT, en_motore_avanti_NUM, 1);
            Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 0);
            CyDelay(5);
        }
        pwm_motore_Start();

        flag.tasto_aprichiudi = 0;
        limitazioni_corrente = 0;
        timeout_mot = 10000;
        tempo_chiusura = 0;

        flag.cronometro = 1;

        faseMotore = mot_calibra_chiudi_attesa;
        break;

    case mot_calibra_chiudi_attesa:

        if (timeout_mot == 0)
            limitazioni_corrente = 5000;
        else
            limitazioni_corrente = contatore_GetCounter();
            
        if (limitazioni_corrente < 500)
            break;
        
        // Chiuso
        flag.cronometro = 0;

        if (tempo_chiusura > 2000)
        {
            par.tipo_movimentazione_chiusura = chiusura_a_tempo;
            
            par.tempo_chiusura = tempo_chiusura - 1000;
            par.tempo_apertura = tempo_chiusura - 800;
            salvaParametri();
        }

        flag.apri_porta = 1;
        // Aggiorno i paramteri al telefono che mi ha detto di fare la calibrazione
        gapDevice[device_a_cui_rispondere].faseSend     = send_par1;
        gapDevice[device_a_cui_rispondere].faseSendNext = send_null;
        
        faseMotore = mot_apri;

        //faseMotore = mot_calibra_riapri;
        break;


    case mot_fine_movimento:

        pwm_motore_Disable();
        controlReg_Write(0);
        CyDelay(10);
        Cy_GPIO_Write(en_motore_avanti_PORT,   en_motore_avanti_NUM, 1);
        Cy_GPIO_Write(en_motore_indietro_PORT, en_motore_indietro_NUM, 1);

        statoPorta.bit.serratura_chiusa = flag.chiudi_porta;

        if (par.tipo_alimentazione == alimentazione_batteria)
            setLed(leds_off);

        // Modifico il random code per l' apertura da smartphone
        GenerateRandomKey(2, random_code);

        flag.ho_aperto = flag.apri_porta;
        flag.ho_chiuso = flag.chiudi_porta;
                
        if (flag.apri_porta)
        {
            outProgrammabile(out_dopo_apertura);
            if (tipo_chiusura == chiusura_automatica )
            {
                flag.tasto_aprichiudi = 0;
                
                if( par.tipo_sensore_chiusura == uso_sensore_ble ) timeout_mot = 35000;
                else timeout_mot = 5000;
                
                faseMotore = mot_fine_movimento_attesa_porta_aperta;
                break;
            }
        }
        if (flag.chiudi_porta)
        {
            outProgrammabile(out_dopo_chiusura);
            if (tipo_chiusura != chiusura_manuale)
            {
                flag.tasto_aprichiudi = 0;
                
                if( par.tipo_sensore_chiusura == uso_sensore_ble ) timeout_mot = 10000;
                else timeout_mot = 5000;

                faseMotore = mot_fine_movimento_attesa_porta_chiusa;
                break;
            }
        }
        
        // proseguo con chiusura manuale

        richiesta_in_sospeso = 0;
        fram_wakeup();
        fram_writeByte( ADDRESS_STATUS, richiesta_in_sospeso );
        fram_writeByte( ADDRESS_STATUS_DOOR, statoPorta.val );
        fram_sleep();
        
        flag.ble_update_adv   = 1;
        flag.tasto_aprichiudi = 0;
        flag.apri_porta       = 0;
        flag.chiudi_porta     = 0;
        flag.calibra_corsa    = 0;
        faseMotore            = mot_attesa;
        
        timerOn = 2000;

        timeout_ble = 500;
        
        if( gapDevice[0].is_bridge ) timeout_ble = 10000;
        if( gapDevice[1].is_bridge ) timeout_ble = 10000;
        if( gapDevice[2].is_bridge ) timeout_ble = 10000;
        if( gapDevice[3].is_bridge ) timeout_ble = 10000;
        
        if( gapDevice[0].master_ok ) timeout_ble = 10000;
        if( gapDevice[1].master_ok ) timeout_ble = 10000;
        if( gapDevice[2].master_ok ) timeout_ble = 10000;
        if( gapDevice[3].master_ok ) timeout_ble = 10000;
            
        break;
    case mot_fine_movimento_attesa_porta_aperta:
        
        verificaStatoPorta();      
        if( par.tipo_sensore_chiusura == uso_sensore_ble )
        {
            if( statoPortaBle.bit.porta_chiusa == 0 )
            {
                timeout_mot = 0;    
            }
        }
        
        if( flag.tasto_aprichiudi )
        {
            timeout_mot = 0;
        }
        if( flag.chiudi_porta == 1 )
        {
            // hanno richiuso la porta da app    
            flag.chiudi_porta = 1;
            flag.apri_porta   = 0;
            faseMotore        = mot_chiudi;
            timeout_mot       = 0;
            break;
        }
        
        if( timeout_mot ) break;
        
        if (statoPorta.bit.porta_chiusa)
        {
            // Ho aperto la serratura ma qualcuno ha richiuso la porta mentre stavo chiudendo la serratura, Riapro !
            flag.chiudi_porta = 1;
            flag.apri_porta   = 0;
            faseMotore        = mot_chiudi;
            break;
        }
        
        richiesta_in_sospeso = 0;
        fram_wakeup();
        fram_writeByte( ADDRESS_STATUS, richiesta_in_sospeso );
        fram_writeByte( ADDRESS_STATUS_DOOR, statoPorta.val );
        fram_sleep();
        
        flag.ble_update_adv   = 1;
        //flag.tasto_aprichiudi = 0;
        flag.apri_porta       = 0;
        flag.chiudi_porta     = 0;
        flag.calibra_corsa    = 0;
        faseMotore            = mot_attesa;
        
        if (tipo_chiusura == chiusura_automatica)
            timerOn = 15000;
        else
            timerOn = 2000;

        timeout_ble = 500;
        
        if( gapDevice[0].is_bridge ) timeout_ble = 10000;
        if( gapDevice[1].is_bridge ) timeout_ble = 10000;
        if( gapDevice[2].is_bridge ) timeout_ble = 10000;
        if( gapDevice[3].is_bridge ) timeout_ble = 10000;
        
        if( gapDevice[0].master_ok ) timeout_ble = 10000;
        if( gapDevice[1].master_ok ) timeout_ble = 10000;
        if( gapDevice[2].master_ok ) timeout_ble = 10000;
        if( gapDevice[3].master_ok ) timeout_ble = 10000;


        break;
    case mot_fine_movimento_attesa_porta_chiusa:
                        
        verificaStatoPorta();
        if( flag.tasto_aprichiudi ) timeout_mot = 0;
        
        if( flag.apri_porta == 1 || flag.apri_da_citofono == 1 )
        {
            // hanno riaperto la porta da app    
            flag.chiudi_porta = 0;
            flag.apri_porta   = 1;
            faseMotore        = mot_apri;
            timeout_mot       = 0;
            break;
        }

        if( par.tipo_sensore_chiusura == uso_sensore_ble )
        {
            if( statoPortaBle.bit.porta_chiusa == 1 )
            {
                timeout_mot = 0;    
            }
        }

        if( timeout_mot ) break;
        
        if (!statoPorta.bit.porta_chiusa)
        {
            // Ho chiuso la serratura ma qualcuno ha aperto la porta mentre stavo chiudendo la serratura, Riapro !
            flag.chiudi_porta = 0;
            flag.apri_porta   = 1;
            faseMotore        = mot_apri;
            break;
        }
        
        richiesta_in_sospeso = 0;
        fram_wakeup();
        fram_writeByte( ADDRESS_STATUS, richiesta_in_sospeso );
        fram_writeByte( ADDRESS_STATUS_DOOR, statoPorta.val );
        fram_sleep();
        
        flag.ble_update_adv   = 1;
        //flag.tasto_aprichiudi = 0;
        flag.apri_porta    = 0;
        flag.chiudi_porta  = 0;
        flag.calibra_corsa = 0;
        faseMotore         = mot_attesa;
        
        if (tipo_chiusura == chiusura_automatica)
            timerOn = 15000;
        else
            timerOn = 2000;

        timeout_ble = 500;
        
        if( gapDevice[0].is_bridge ) timeout_ble = 10000;
        if( gapDevice[1].is_bridge ) timeout_ble = 10000;
        if( gapDevice[2].is_bridge ) timeout_ble = 10000;
        if( gapDevice[3].is_bridge ) timeout_ble = 10000;
        
        if( gapDevice[0].master_ok ) timeout_ble = 10000;
        if( gapDevice[1].master_ok ) timeout_ble = 10000;
        if( gapDevice[2].master_ok ) timeout_ble = 10000;
        if( gapDevice[3].master_ok ) timeout_ble = 10000;
        
        break;
    }
}

#define MLIMIT_O 120
#define MLIMIT_C 240

void verificaStatoPorta()
{
    static uint32_t chiusa = 10;
    static uint8_t fase_automatica = 0;
    static uint8_t old = 0;

    int limit;
    int t1;
    int t2;
    int t3;
    int giorno;
    
    Cy_RTC_GetDateAndTime(&orologio);

    giorno = orologio.dayOfWeek - 1;
    
    if( giorno >= 0 )
    {
        if( chiusureProgrammate[ giorno ].enable )
        {
            t1 = (orologio.hour * 60) + orologio.min;
            t2 = (chiusureProgrammate[ giorno ].ore     * 60 ) + chiusureProgrammate[ giorno ].minuti;
            t3 = (chiusureProgrammate[ giorno ].ore_stop * 60) + chiusureProgrammate[ giorno ].minuti_stop;
            
            if( t1 >= t2 && t1 <= t3 ) tipo_chiusura = par.tipo_chiusura_set;
            else                       tipo_chiusura = chiusura_automatica;        
        }
        else tipo_chiusura = par.tipo_chiusura_set;        
    }
    else 
        tipo_chiusura = par.tipo_chiusura_set;        

    if (statoPorta.bit.serratura_chiusa)
        limit = MLIMIT_C;
    else
        limit = MLIMIT_O;

    if (par.tipo_sensore_chiusura == uso_niente)
        return;

    if (par.tipo_sensore_chiusura == uso_sensore_esterno)
    {
        if (Cy_GPIO_Read(sensore_porta_chiusa_PORT, sensore_porta_chiusa_NUM) == 0)
        {
            if (chiusa)
                --chiusa;
        }
        else
        {
            chiusa = 3;
        }
    }
    else if (par.tipo_sensore_chiusura == uso_magnetometro)
    {
        leggo_magnetometro();

        if (par.magnete_porta_chiusa.x > (lis3mdl.value_x - limit) && par.magnete_porta_chiusa.x < (lis3mdl.value_x + limit))
        {
            if (par.magnete_porta_chiusa.y > (lis3mdl.value_y - limit) && par.magnete_porta_chiusa.y < (lis3mdl.value_y + limit))
            {
                if (par.magnete_porta_chiusa.z > (lis3mdl.value_z - limit) && par.magnete_porta_chiusa.z < (lis3mdl.value_z + limit))
                {
                    if (chiusa)
                        --chiusa;
                }
                else
                    chiusa = 3;
            }
            else
                chiusa = 3;
        }
        else
            chiusa = 3;
    }
    else if( par.tipo_sensore_chiusura == uso_sensore_ble )
    {
        if( statoPortaBle.bit.porta_chiusa )
            chiusa = 0;
        else
            chiusa = 3;
    }

    if (chiusa == 0)
        statoPorta.bit.porta_chiusa = 1;
    else
    {
        statoPorta.bit.porta_chiusa = 0;
    }

    if (statoPortaOld.bit.porta_chiusa == 0 && statoPorta.bit.porta_chiusa == 1)
    {
        // la porta si è chiusa
        outProgrammabile(out_sensore_porta_chiusa);
        statoPortaOld.val = statoPorta.val;
        flag.ble_update_adv = 1;
    }
    if (statoPortaOld.bit.porta_chiusa == 1 && statoPorta.bit.porta_chiusa == 0)
    {
        // la porta si è aperta
        statoPorta.bit.serratura_chiusa = 0;
        outProgrammabile(out_sensore_porta_aperta);
        statoPortaOld.val = statoPorta.val;
        flag.ble_update_adv = 1;
    }

    if( statoPorta.val != old )
    {
        fram_wakeup();
        fram_writeByte( ADDRESS_STATUS_DOOR, statoPorta.val );
        fram_sleep();
        
        old = statoPorta.val;
    }
    
    
    // Vedo se devo chiudere la porta automaticamente
    if (tipo_chiusura == chiusura_automatica)
    {
        switch (fase_automatica)
        {
        case 0:
            if (statoPorta.bit.serratura_chiusa == 0)
            {
                // Attendo che la porta si apra
                timerPortaAperta = (uint32_t)par.tempo_massimo_porta_aperta * 1000;
                fase_automatica = 1;
            }
            break;

        case 1:
            if (statoPorta.bit.serratura_chiusa)
            {
                // La serratura è stata chiusa manualmente
                fase_automatica = 0;
                break;
            }
            if (statoPorta.bit.porta_chiusa == 0)
            {
                // La porta si è fisicamente aperta
                fase_automatica = 2;
                break;
            }
            if (timerPortaAperta == 0 && statoPorta.bit.porta_chiusa)
            {
                // Se dopo 10 secondi la porta non si è fisicamente apperta , chiudo la serratura
                CyDelay(100);
                flag.chiudi_porta = 1;
                fase_automatica = 0;
                salvaLogPorta(chiusura_automatica_da_serratura, evento_eseguito);
            }
            break;

        case 2:
            if (statoPorta.bit.serratura_chiusa)
            {
                // La serratura è stata chiusa manualmente
                fase_automatica = 0;
                break;
            }
            if (statoPorta.bit.porta_chiusa && statoPorta.bit.serratura_chiusa == 0)
            {
                // La porta si è richiusa, chiudo la serratura
                CyDelay(100);
                flag.chiudi_porta = 1;
                fase_automatica = 0;
                salvaLogPorta(chiusura_automatica_da_serratura, evento_eseguito);
            }
            break;
        }
    }
}
//

extern bool main_start;
void tick_1ms()
{
#ifdef TEST_APERTURE_ALIMENTATA
    static int timer_apri_chiudi = 10000;    
    if( timer_apri_chiudi ) --timer_apri_chiudi;
    else
    {
        if( flag.ble_connect == 0 )
            flag.tasto_aprichiudi = 1;
        
        timer_apri_chiudi = 15000;
    }
#endif
    
    if(timer_refresh_led) --timer_refresh_led;
    if (timerOn) --timerOn;
    if (timerOff) --timerOff;
    if (timeout_mot) --timeout_mot;
    if (timerLed) --timerLed;
    if (timerTask) --timerTask;
    if (timeout_associazione) --timeout_associazione;
    if( timeout_disable ) --timeout_disable;
    if (timeout_ble) --timeout_ble;
    if (timeout_mastercode ) --timeout_mastercode;
    if (timerPortaAperta) --timerPortaAperta;
    if (timerProssimaApertura) --timerProssimaApertura;
    if (timerNotification) --timerNotification;
    if( timer_send_event ) --timer_send_event;
    
    if (flag.ble_connect)
        ++crono;
    else
    {
        if (crono)
        {
            crono = crono / 1000;
            logUfg.secondi_ble += crono;
            flag.save_logufg = 1;
            crono = 0;
        }
    }

    if (flag.cronometro) ++tempo_chiusura;
    
    //
    if (flag.recovery == 0)
    {
        if (Cy_GPIO_Read(tasto_PORT, tasto_NUM) == 0)
        {
            if (++timerTastoPremuto == MS_PER_ACCOPPIARE )
            {
                flag.recovery = 1;
            }            
        }
        else
        {
            timerTastoPremuto = 0;
        }
    }
    else
        timerTastoPremuto = 0;

    if (timerLampeggio)
    {
        if (timerLampeggio < 100)
            setLed(leds_off);
        else
        {
            setLed(led_verde_on);
        }
        --timerLampeggio;
    }

    if (timerOutProgrammabile)
    {
        if (timerOutProgrammabile < 100)
            Cy_GPIO_Write(out_programmabile_PORT, out_programmabile_NUM, 0);
        else
        {
            Cy_GPIO_Write(out_programmabile_PORT, out_programmabile_NUM, 1);
        }
        --timerOutProgrammabile;
    }

    if (lampeggio)
        --lampeggio;

    switch (stato_led)
    {
    case leds_off:
        Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 0);
        Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 0);
        break;
    case led_verde_on:
        if (RELEASE_FW != 272 || par.tipo_chiusura_set != chiusura_manuale )
        {
            Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 1);
            Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 0);
        }
        else
        {
            stato_led = leds_off;
        }
        break;
    case led_verde_toggle:
        if (lampeggio > 200)
        {
            Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 1);
            Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 0);
            break;
        }
        Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 0);
        Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 0);
        if (lampeggio == 0)
            lampeggio = 400;
        break;
    case led_rosso_on:
        if (RELEASE_FW != 272 || par.tipo_chiusura_set != chiusura_manuale )
        {
            Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 0);
            Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 1);
        }
        else
        {
            stato_led = leds_off;
        }
        break;
    case led_rosso_toggle:
        if (lampeggio > 200)
        {
            Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 0);
            Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 1);
            break;
        }
        Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 0);
        Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 0);
        if (lampeggio == 0)
            lampeggio = 400;
        break;
    case led_rosso_toggle5:
        if (lampeggio > 1900)
        {
            Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 0);
            Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 1);
            break;
        }
        if (lampeggio > 1700 && lampeggio < 1800 )
        {
            Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 0);
            Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 1);
            break;
        }
        if (lampeggio > 1500 && lampeggio < 1600 )
        {
            Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 0);
            Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 1);
            break;
        }
        if (lampeggio > 1300 && lampeggio < 1400 )
        {
            Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 0);
            Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 1);
            break;
        }
        if (lampeggio > 1100 && lampeggio < 1200 )
        {
            Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 0);
            Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 1);
            break;
        }
        Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 0);
        Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 0);
        if (lampeggio == 0)
            lampeggio = 2000;
        break;
        
    case led_verde_rosso_toggle:
        if (lampeggio > 200)
        {
            Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 1);
            Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 0);
            break;
        }
        Cy_GPIO_Write(led_verde_PORT, led_verde_NUM, 0);
        Cy_GPIO_Write(led_rosso_PORT, led_rosso_NUM, 1);
        if (lampeggio == 0)
            lampeggio = 400;
        break;
    case led_arancio_on:

        break;
    }
}

void setLed(eLed led)
{
    static int rosso = 0;
    static eLed old;
    
    if( led != old )
    {
        stato_led = led;    
        old = led;
        if( led == led_rosso_on )
        {
            ++rosso;
        }
    }
    
}

void BLE_Data()
{
    timerOn = 5000;
    
    if( gapDevice[0].faseSend != send_null ) notificationTask(0);
    if( gapDevice[1].faseSend != send_null ) notificationTask(1);
    if( gapDevice[2].faseSend != send_null ) notificationTask(2);
    if( gapDevice[3].faseSend != send_null ) notificationTask(3);
}

void notificationTask(int device)
{
    static uint16_t cUtente = 0;
    static int cLog = 0;

    uint8_t id_risposta = gapDevice[device].id_risposta;
    uint8_t command     = 0;
    uint8_t len         = 0;
    uint8_t pacchetto   = 0;
    uint8_t pacchetti   = 0;
    uint8_t crypto_en   = 1;

    if( cy_ble_connState[device] != CY_BLE_CONN_STATE_CONNECTED )
    {
        // Se si è disconnesso non invio nulla        
        gapDevice[device].faseSend = send_null;
        return;
    }
    
    // Se è impegnato riprovo dopo
    if (Cy_BLE_GATT_GetBusyStatus( gapDevice[device].connHandle.attId  ) != CY_BLE_STACK_STATE_FREE)
        return;
    
    memset(buf, 0, 128);
    memset(chiper, 0, 128);

    // I primi 4 byte verranno inviati in chiaro :
    // B0 = comando
    // B1 = pacchetto
    // B2 = Totali pacchetti
    // B3 = Opzionale ( 0 al momento )

    gapDevice[device].notificationHandle.attrHandle = CY_BLE_SERVER_UART_TX_DATA_CHAR_HANDLE;
    //notificationHandle.attrHandle = CY_BLE_SERVER_UART_TX_DATA_CHAR_HANDLE;
   
    switch ( gapDevice[device].faseSend )
    {
    case send_null:
        return;
        break;
    // PARAMETRI
    case send_par1:
        command = 0x01;
        len = 32;

        buf[0] = 0;
        buf[1] = 0;
        buf[2] = par.timezone;
        buf[3] = par.forza_motore;
        buf[4] = par.direzione_apertura;
        buf[5] = par.tipo_alimentazione;
        buf[6] = par.tipo_apertura;
        buf[7] = par.tipo_chiusura_set;
        buf[8] = par.tipo_movimentazione_chiusura;
        buf[9] = par.tipo_sensore_chiusura;
        buf[10] = (uint8_t)utenti_registrati;
        buf[11] = (uint8_t)(par.tempo_chiusura >> 8);
        buf[12] = (uint8_t)(par.tempo_chiusura & 0xff);
        buf[13] = (uint8_t)(par.limitazioni_corrente >> 8);
        buf[14] = (uint8_t)(par.limitazioni_corrente & 0xff);
        buf[15] = revision;

        buf[16] = par.output.anable;
        buf[17] = par.output.stato_attivo;
        buf[18] = par.output.quando_attivo;
        buf[19] = (uint8_t)(par.output.ms_attivo >> 8);
        buf[20] = (uint8_t)(par.output.ms_attivo & 0xff);

        buf[21] = (uint8_t)(par.tempo_scrocchio >> 8);
        buf[22] = (uint8_t)(par.tempo_scrocchio & 0xff);

        buf[23] = par.en_citofono;
        buf[24] = par.velocita_apertura;
        buf[25] = par.en_motore;

        buf[26] = (uint8_t)(par.tempo_apertura >> 8);
        buf[27] = (uint8_t)(par.tempo_apertura & 0xff);
        buf[28] = par.velocita_rallentamento_in_chiusura;
        
        cUtente = 0;
        cLog = 0;

        flag.send_par_tocloud = 0;
        
        gapDevice[device].faseSend = gapDevice[device].faseSendNext;
        break;
        
    case send_chiusure:
        command = 0x36;
        len = 48;

        buf[0]  = chiusureProgrammate[0].enable;
        buf[1]  = chiusureProgrammate[0].ore;
        buf[2]  = chiusureProgrammate[0].minuti;
        buf[3]  = chiusureProgrammate[0].ore_stop;
        buf[4]  = chiusureProgrammate[0].minuti_stop;
        
        buf[5]  = chiusureProgrammate[1].enable;
        buf[6]  = chiusureProgrammate[1].ore;
        buf[7]  = chiusureProgrammate[1].minuti;
        buf[8]  = chiusureProgrammate[1].ore_stop;
        buf[9]  = chiusureProgrammate[1].minuti_stop;
        
        buf[10]  = chiusureProgrammate[2].enable;
        buf[11]  = chiusureProgrammate[2].ore;
        buf[12]  = chiusureProgrammate[2].minuti;
        buf[13]  = chiusureProgrammate[2].ore_stop;
        buf[14]  = chiusureProgrammate[2].minuti_stop;
        
        buf[15] = chiusureProgrammate[3].enable;
        buf[16] = chiusureProgrammate[3].ore;
        buf[17] = chiusureProgrammate[3].minuti;
        buf[18] = chiusureProgrammate[3].ore_stop;
        buf[19] = chiusureProgrammate[3].minuti_stop;
        
        buf[20] = chiusureProgrammate[4].enable;
        buf[21] = chiusureProgrammate[4].ore;
        buf[22] = chiusureProgrammate[4].minuti;
        buf[23] = chiusureProgrammate[4].ore_stop;
        buf[24] = chiusureProgrammate[4].minuti_stop;
        
        buf[25] = chiusureProgrammate[5].enable;
        buf[26] = chiusureProgrammate[5].ore;
        buf[27] = chiusureProgrammate[5].minuti;
        buf[28] = chiusureProgrammate[5].ore_stop;
        buf[29] = chiusureProgrammate[5].minuti_stop;
        
        buf[30] = chiusureProgrammate[6].enable;
        buf[31] = chiusureProgrammate[6].ore;
        buf[32] = chiusureProgrammate[6].minuti;
        buf[33] = chiusureProgrammate[6].ore_stop;
        buf[34] = chiusureProgrammate[6].minuti_stop;

        gapDevice[device].faseSendNext = send_log;
        gapDevice[device].faseSend     = send_users;
        break;

    // UTENTI
    case send_users:
        // Utente : nome
        
        if (cUtente == utenti_registrati)
        {            
            if( flag.send_users_tocloud || flag.send_all_tocloud || flag.send_cloud )
                gapDevice[device].faseSend = send_null;
            else 
                gapDevice[device].faseSend = gapDevice[device].faseSendNext;
                
            flag.send_users_tocloud = 0;            
            flag.send_all_tocloud   = 0;
            flag.send_cloud         = 0;
                
            len     = 0;
            cUtente = 0;
            break;
        }
        command = 0x02;
        len = 80;

        buf[0]  = utenti[cUtente].tipo;

        buf[1]  = utenti[cUtente].nome[0];
        buf[2]  = utenti[cUtente].nome[1];
        buf[3]  = utenti[cUtente].nome[2];
        buf[4]  = utenti[cUtente].nome[3];
        buf[5]  = utenti[cUtente].nome[4];
        buf[6]  = utenti[cUtente].nome[5];
        buf[7]  = utenti[cUtente].nome[6];
        buf[8]  = utenti[cUtente].nome[7];
        buf[9]  = utenti[cUtente].nome[8];
        buf[10] = utenti[cUtente].nome[9];

        buf[11] = utenti[cUtente].pass[0];
        buf[12] = utenti[cUtente].pass[1];
        buf[13] = utenti[cUtente].pass[2];
        buf[14] = utenti[cUtente].pass[3];
        buf[15] = utenti[cUtente].pass[4];
        buf[16] = utenti[cUtente].pass[5];
        buf[17] = utenti[cUtente].pass[6];
        buf[18] = utenti[cUtente].pass[7];

        buf[19] = utenti[cUtente].tastierino[0];
        buf[20] = utenti[cUtente].tastierino[1];
        buf[21] = utenti[cUtente].tastierino[2];
        buf[22] = utenti[cUtente].tastierino[3];
        buf[23] = utenti[cUtente].tastierino[4];
        buf[24] = utenti[cUtente].tastierino[5];

        buf[25] = utenti[cUtente].permessi;

        buf[26] = (uint8_t)((utenti[cUtente].permessi_orari[0] >> 40) & 0xff);
        buf[27] = (uint8_t)((utenti[cUtente].permessi_orari[0] >> 32) & 0xff);
        buf[28] = (uint8_t)((utenti[cUtente].permessi_orari[0] >> 24) & 0xff);
        buf[29] = (uint8_t)((utenti[cUtente].permessi_orari[0] >> 16) & 0xff);
        buf[30] = (uint8_t)((utenti[cUtente].permessi_orari[0] >>  8) & 0xff);
        buf[31] = (uint8_t)((utenti[cUtente].permessi_orari[0]) & 0xff);

        buf[32] = (uint8_t)((utenti[cUtente].permessi_orari[1] >> 40) & 0xff);
        buf[33] = (uint8_t)((utenti[cUtente].permessi_orari[1] >> 32) & 0xff);
        buf[34] = (uint8_t)((utenti[cUtente].permessi_orari[1] >> 24) & 0xff);
        buf[35] = (uint8_t)((utenti[cUtente].permessi_orari[1] >> 16) & 0xff);
        buf[36] = (uint8_t)((utenti[cUtente].permessi_orari[1] >>  8) & 0xff);
        buf[37] = (uint8_t)((utenti[cUtente].permessi_orari[1]) & 0xff);

        buf[38] = (uint8_t)((utenti[cUtente].permessi_orari[2] >> 40) & 0xff);
        buf[39] = (uint8_t)((utenti[cUtente].permessi_orari[2] >> 32) & 0xff);
        buf[40] = (uint8_t)((utenti[cUtente].permessi_orari[2] >> 24) & 0xff);
        buf[41] = (uint8_t)((utenti[cUtente].permessi_orari[2] >> 16) & 0xff);
        buf[42] = (uint8_t)((utenti[cUtente].permessi_orari[2] >>  8) & 0xff);
        buf[43] = (uint8_t)((utenti[cUtente].permessi_orari[2]) & 0xff);

        buf[44] = (uint8_t)((utenti[cUtente].permessi_orari[3] >> 40) & 0xff);
        buf[45] = (uint8_t)((utenti[cUtente].permessi_orari[3] >> 32) & 0xff);
        buf[46] = (uint8_t)((utenti[cUtente].permessi_orari[3] >> 24) & 0xff);
        buf[47] = (uint8_t)((utenti[cUtente].permessi_orari[3] >> 16) & 0xff);
        buf[48] = (uint8_t)((utenti[cUtente].permessi_orari[3] >>  8) & 0xff);
        buf[49] = (uint8_t)((utenti[cUtente].permessi_orari[3]) & 0xff);

        buf[50] = (uint8_t)((utenti[cUtente].permessi_orari[4] >> 40) & 0xff);
        buf[51] = (uint8_t)((utenti[cUtente].permessi_orari[4] >> 32) & 0xff);
        buf[52] = (uint8_t)((utenti[cUtente].permessi_orari[4] >> 24) & 0xff);
        buf[53] = (uint8_t)((utenti[cUtente].permessi_orari[4] >> 16) & 0xff);
        buf[54] = (uint8_t)((utenti[cUtente].permessi_orari[4] >>  8) & 0xff);
        buf[55] = (uint8_t)((utenti[cUtente].permessi_orari[4]) & 0xff);

        buf[56] = (uint8_t)((utenti[cUtente].permessi_orari[5] >> 40) & 0xff);
        buf[57] = (uint8_t)((utenti[cUtente].permessi_orari[5] >> 32) & 0xff);
        buf[58] = (uint8_t)((utenti[cUtente].permessi_orari[5] >> 24) & 0xff);
        buf[59] = (uint8_t)((utenti[cUtente].permessi_orari[5] >> 16) & 0xff);
        buf[60] = (uint8_t)((utenti[cUtente].permessi_orari[5] >>  8) & 0xff);
        buf[61] = (uint8_t)((utenti[cUtente].permessi_orari[5]) & 0xff);

        buf[62] = (uint8_t)((utenti[cUtente].permessi_orari[6] >> 40) & 0xff);
        buf[63] = (uint8_t)((utenti[cUtente].permessi_orari[6] >> 32) & 0xff);
        buf[64] = (uint8_t)((utenti[cUtente].permessi_orari[6] >> 24) & 0xff);
        buf[65] = (uint8_t)((utenti[cUtente].permessi_orari[6] >> 16) & 0xff);
        buf[66] = (uint8_t)((utenti[cUtente].permessi_orari[6] >>  8) & 0xff);
        buf[67] = (uint8_t)((utenti[cUtente].permessi_orari[6]) & 0xff);

        buf[68] = (uint8_t)((utenti[cUtente].aperture >> 8) & 0xff);
        buf[69] = (uint8_t)((utenti[cUtente].aperture & 0xff));

        buf[70] = utenti[cUtente].affitto.giorno;
        buf[71] = utenti[cUtente].affitto.mese;
        buf[72] = utenti[cUtente].affitto.anno;
        buf[73] = utenti[cUtente].affitto.ore;
        buf[74] = utenti[cUtente].affitto.orePermanenza >> 8;
        buf[75] = utenti[cUtente].affitto.orePermanenza & 0xff;
        
        buf[76] = utenti[cUtente].impronta;

        buf[77] = cUtente + 1;
        buf[78] = utenti_registrati;

        if( flag.send_users_tocloud || flag.send_all_tocloud ) CyDelay( 500 );
        
        ++cUtente;
        break;
        
    case send_user_to_cloud:
        
        flag.send_user_tocloud = 0;
        
        command = 0x02;
        len     = 80;
        
        if( utente_cloud_tipo == 2 )
        {
            buf[0] = utente_eliminato.tipo;
            
            buf[1]  = utente_eliminato.nome[0];
            buf[2]  = utente_eliminato.nome[1];
            buf[3]  = utente_eliminato.nome[2];
            buf[4]  = utente_eliminato.nome[3];
            buf[5]  = utente_eliminato.nome[4];
            buf[6]  = utente_eliminato.nome[5];
            buf[7]  = utente_eliminato.nome[6];
            buf[8]  = utente_eliminato.nome[7];
            buf[9]  = utente_eliminato.nome[8];
            buf[10] = utente_eliminato.nome[9];

            buf[19] = utente_eliminato.tastierino[0];
            buf[20] = utente_eliminato.tastierino[1];
            buf[21] = utente_eliminato.tastierino[2];
            buf[22] = utente_eliminato.tastierino[3];
            buf[23] = utente_eliminato.tastierino[4];
            buf[24] = utente_eliminato.tastierino[5];

            buf[70] = utente_eliminato.affitto.giorno;
            buf[71] = utente_eliminato.affitto.mese;
            buf[72] = utente_eliminato.affitto.anno;
            buf[73] = utente_eliminato.affitto.ore;
            buf[74] = utente_eliminato.affitto.orePermanenza >> 8;
            buf[75] = utente_eliminato.affitto.orePermanenza & 0xff;
        
            buf[77] = 1;
            buf[78] = 1;
            buf[79] = utente_cloud_tipo;
            
            gapDevice[device].faseSend = gapDevice[device].faseSendNext;
            
            utente_cloud_tipo = 0;
            break;    
        }
        
        
        buf[0]  = utenti[utente_cloud].tipo;

        buf[1]  = utenti[utente_cloud].nome[0];
        buf[2]  = utenti[utente_cloud].nome[1];
        buf[3]  = utenti[utente_cloud].nome[2];
        buf[4]  = utenti[utente_cloud].nome[3];
        buf[5]  = utenti[utente_cloud].nome[4];
        buf[6]  = utenti[utente_cloud].nome[5];
        buf[7]  = utenti[utente_cloud].nome[6];
        buf[8]  = utenti[utente_cloud].nome[7];
        buf[9]  = utenti[utente_cloud].nome[8];
        buf[10] = utenti[utente_cloud].nome[9];

        buf[11] = utenti[utente_cloud].pass[0];
        buf[12] = utenti[utente_cloud].pass[1];
        buf[13] = utenti[utente_cloud].pass[2];
        buf[14] = utenti[utente_cloud].pass[3];
        buf[15] = utenti[utente_cloud].pass[4];
        buf[16] = utenti[utente_cloud].pass[5];
        buf[17] = utenti[utente_cloud].pass[6];
        buf[18] = utenti[utente_cloud].pass[7];

        buf[19] = utenti[utente_cloud].tastierino[0];
        buf[20] = utenti[utente_cloud].tastierino[1];
        buf[21] = utenti[utente_cloud].tastierino[2];
        buf[22] = utenti[utente_cloud].tastierino[3];
        buf[23] = utenti[utente_cloud].tastierino[4];
        buf[24] = utenti[utente_cloud].tastierino[5];

        buf[25] = utenti[utente_cloud].permessi;

        buf[26] = (uint8_t)((utenti[utente_cloud].permessi_orari[0] >> 40) & 0xff);
        buf[27] = (uint8_t)((utenti[utente_cloud].permessi_orari[0] >> 32) & 0xff);
        buf[28] = (uint8_t)((utenti[utente_cloud].permessi_orari[0] >> 24) & 0xff);
        buf[29] = (uint8_t)((utenti[utente_cloud].permessi_orari[0] >> 16) & 0xff);
        buf[30] = (uint8_t)((utenti[utente_cloud].permessi_orari[0] >> 8) & 0xff);
        buf[31] = (uint8_t)((utenti[utente_cloud].permessi_orari[0]) & 0xff);

        buf[32] = (uint8_t)((utenti[utente_cloud].permessi_orari[1] >> 40) & 0xff);
        buf[33] = (uint8_t)((utenti[utente_cloud].permessi_orari[1] >> 32) & 0xff);
        buf[34] = (uint8_t)((utenti[utente_cloud].permessi_orari[1] >> 24) & 0xff);
        buf[35] = (uint8_t)((utenti[utente_cloud].permessi_orari[1] >> 16) & 0xff);
        buf[36] = (uint8_t)((utenti[utente_cloud].permessi_orari[1] >> 8) & 0xff);
        buf[37] = (uint8_t)((utenti[utente_cloud].permessi_orari[1]) & 0xff);

        buf[38] = (uint8_t)((utenti[utente_cloud].permessi_orari[2] >> 40) & 0xff);
        buf[39] = (uint8_t)((utenti[utente_cloud].permessi_orari[2] >> 32) & 0xff);
        buf[40] = (uint8_t)((utenti[utente_cloud].permessi_orari[2] >> 24) & 0xff);
        buf[41] = (uint8_t)((utenti[utente_cloud].permessi_orari[2] >> 16) & 0xff);
        buf[42] = (uint8_t)((utenti[utente_cloud].permessi_orari[2] >> 8) & 0xff);
        buf[43] = (uint8_t)((utenti[utente_cloud].permessi_orari[2]) & 0xff);

        buf[44] = (uint8_t)((utenti[utente_cloud].permessi_orari[3] >> 40) & 0xff);
        buf[45] = (uint8_t)((utenti[utente_cloud].permessi_orari[3] >> 32) & 0xff);
        buf[46] = (uint8_t)((utenti[utente_cloud].permessi_orari[3] >> 24) & 0xff);
        buf[47] = (uint8_t)((utenti[utente_cloud].permessi_orari[3] >> 16) & 0xff);
        buf[48] = (uint8_t)((utenti[utente_cloud].permessi_orari[3] >> 8) & 0xff);
        buf[49] = (uint8_t)((utenti[utente_cloud].permessi_orari[3]) & 0xff);

        buf[50] = (uint8_t)((utenti[utente_cloud].permessi_orari[4] >> 40) & 0xff);
        buf[51] = (uint8_t)((utenti[utente_cloud].permessi_orari[4] >> 32) & 0xff);
        buf[52] = (uint8_t)((utenti[utente_cloud].permessi_orari[4] >> 24) & 0xff);
        buf[53] = (uint8_t)((utenti[utente_cloud].permessi_orari[4] >> 16) & 0xff);
        buf[54] = (uint8_t)((utenti[utente_cloud].permessi_orari[4] >> 8) & 0xff);
        buf[55] = (uint8_t)((utenti[utente_cloud].permessi_orari[4]) & 0xff);

        buf[56] = (uint8_t)((utenti[utente_cloud].permessi_orari[5] >> 40) & 0xff);
        buf[57] = (uint8_t)((utenti[utente_cloud].permessi_orari[5] >> 32) & 0xff);
        buf[58] = (uint8_t)((utenti[utente_cloud].permessi_orari[5] >> 24) & 0xff);
        buf[59] = (uint8_t)((utenti[utente_cloud].permessi_orari[5] >> 16) & 0xff);
        buf[60] = (uint8_t)((utenti[utente_cloud].permessi_orari[5] >> 8) & 0xff);
        buf[61] = (uint8_t)((utenti[utente_cloud].permessi_orari[5]) & 0xff);

        buf[62] = (uint8_t)((utenti[utente_cloud].permessi_orari[6] >> 40) & 0xff);
        buf[63] = (uint8_t)((utenti[utente_cloud].permessi_orari[6] >> 32) & 0xff);
        buf[64] = (uint8_t)((utenti[utente_cloud].permessi_orari[6] >> 24) & 0xff);
        buf[65] = (uint8_t)((utenti[utente_cloud].permessi_orari[6] >> 16) & 0xff);
        buf[66] = (uint8_t)((utenti[utente_cloud].permessi_orari[6] >> 8) & 0xff);
        buf[67] = (uint8_t)((utenti[utente_cloud].permessi_orari[6]) & 0xff);

        buf[68] = (uint8_t)((utenti[utente_cloud].aperture >> 8) & 0xff);
        buf[69] = (uint8_t)((utenti[utente_cloud].aperture & 0xff));

        buf[70] = utenti[utente_cloud].affitto.giorno;
        buf[71] = utenti[utente_cloud].affitto.mese;
        buf[72] = utenti[utente_cloud].affitto.anno;
        buf[73] = utenti[utente_cloud].affitto.ore;
        buf[74] = utenti[utente_cloud].affitto.orePermanenza >> 8;
        buf[75] = utenti[utente_cloud].affitto.orePermanenza & 0xff;
        
        buf[76] = utenti[utente_cloud].impronta;

        buf[77] = 1;
        buf[78] = 1;
        buf[79] = utente_cloud_tipo;
        
        gapDevice[device].faseSend = gapDevice[device].faseSendNext;
        
        break;

    case send_log:
        // Log
        if (cUtente == utenti_registrati)
        {
            len = 0;
            cUtente = 0;
            gapDevice[device].faseSend = send_log_serratura;
            break;
        }
        if (cLog == utenti[cUtente].tlog)
        {
            ++cUtente;
            cLog = 0;
            len = 0;
            break;
        }

        command = 0x05;
        len = 32;

        buf[0] = utenti[cUtente].nome[0];
        buf[1] = utenti[cUtente].nome[1];
        buf[2] = utenti[cUtente].nome[2];
        buf[3] = utenti[cUtente].nome[3];
        buf[4] = utenti[cUtente].nome[4];
        buf[5] = utenti[cUtente].nome[5];
        buf[6] = utenti[cUtente].nome[6];
        buf[7] = utenti[cUtente].nome[7];
        buf[8] = utenti[cUtente].nome[8];
        buf[9] = utenti[cUtente].nome[9];

        buf[10] = utenti[cUtente].log[cLog].datetime.giorno;
        buf[11] = utenti[cUtente].log[cLog].datetime.mese;
        buf[12] = utenti[cUtente].log[cLog].datetime.anno;
        buf[13] = utenti[cUtente].log[cLog].datetime.ore;
        buf[14] = utenti[cUtente].log[cLog].datetime.minuti;
        buf[15] = utenti[cUtente].log[cLog].richiesta;
        buf[16] = utenti[cUtente].log[cLog].richiesta_eseguita;

        ++cLog;
        break;

    case send_log_serratura:

        if (cLog == logSerratura.tlog)
        {
            gapDevice[device].faseSend = send_log_ufg;
            cLog = 0;
            len = 0;
            break;
        }

        command = 0x35;
        len = 32;

        buf[0] = 's';
        buf[1] = 'e';
        buf[2] = 'r';
        buf[3] = 'r';
        buf[4] = 'a';
        buf[5] = 't';
        buf[6] = 'u';
        buf[7] = 'r';
        buf[8] = 'a';
        buf[9] = 0;

        buf[10] = logSerratura.log[cLog].datetime.giorno;
        buf[11] = logSerratura.log[cLog].datetime.mese;
        buf[12] = logSerratura.log[cLog].datetime.anno;
        buf[13] = logSerratura.log[cLog].datetime.ore;
        buf[14] = logSerratura.log[cLog].datetime.minuti;
        buf[15] = logSerratura.log[cLog].richiesta;
        buf[16] = logSerratura.log[cLog].richiesta_eseguita;

        ++cLog;
        break;

    case send_log_ufg:
        command = 0x34;
        len = 32;

        buf[0] = logUfg.release;
        buf[1] = logUfg.giorno_inizio;
        buf[2] = logUfg.mese_inizio;
        buf[3] = logUfg.anno_inizio;
        buf[4] = (uint8_t)((logUfg.totale_aperture >> 24) & 0xff);
        buf[5] = (uint8_t)((logUfg.totale_aperture >> 16) & 0xff);
        buf[6] = (uint8_t)((logUfg.totale_aperture >> 8) & 0xff);
        buf[7] = (uint8_t)((logUfg.totale_aperture) & 0xff);
        buf[8] = (uint8_t)((logUfg.totale_chiusure >> 24) & 0xff);
        buf[9] = (uint8_t)((logUfg.totale_chiusure >> 16) & 0xff);
        buf[10] = (uint8_t)((logUfg.totale_chiusure >> 8) & 0xff);
        buf[11] = (uint8_t)((logUfg.totale_chiusure) & 0xff);
        buf[12] = (uint8_t)((logUfg.secondi_ble >> 24) & 0xff);
        buf[13] = (uint8_t)((logUfg.secondi_ble >> 16) & 0xff);
        buf[14] = (uint8_t)((logUfg.secondi_ble >> 8) & 0xff);
        buf[15] = (uint8_t)((logUfg.secondi_ble) & 0xff);
        buf[16] = logUfg.giorno_eas;
        buf[17] = logUfg.mese_eas;
        buf[18] = logUfg.anno_eas;
        buf[19] = logUfg.evento_reset;

        gapDevice[device].faseSend = send_end;
        break;

    case send_orologio:
        Cy_RTC_GetDateAndTime(&orologio);

        command = 0x06;
        len = 16;

        buf[0] = (uint8_t)orologio.date;
        buf[1] = (uint8_t)orologio.month;
        buf[2] = (uint8_t)orologio.year;
        buf[3] = (uint8_t)orologio.hour;
        buf[4] = (uint8_t)orologio.min;
        buf[5] = (uint8_t)orologio.sec;

        gapDevice[device].faseSend = send_null;
        break;

    case send_sensors:

        command = 0x015;
        len = 16;

        leggo_magnetometro();

        buf[0] = tasto_ex;
        buf[1] = (uint8_t)(lis3mdl.value_x >> 8);
        buf[2] = (uint8_t)(lis3mdl.value_x & 0xff);
        buf[3] = (uint8_t)(lis3mdl.value_y >> 8);
        buf[4] = (uint8_t)(lis3mdl.value_y & 0xff);
        buf[5] = (uint8_t)(lis3mdl.value_z >> 8);
        buf[6] = (uint8_t)(lis3mdl.value_z & 0xff);

        Cy_RTC_GetDateAndTime(&orologio);

        buf[7] = (uint8_t)orologio.date;
        buf[8] = (uint8_t)orologio.month;
        buf[9] = (uint8_t)orologio.year;
        buf[10] = (uint8_t)orologio.hour;
        buf[11] = (uint8_t)orologio.min;
        buf[12] = (uint8_t)orologio.sec;

        gapDevice[device].faseSend = send_null;
        break;

    case send_sensors_cont:

        command = 0x015;
        len = 16;

        leggo_magnetometro();

        tasto_ex = (uint8_t)Cy_GPIO_Read(sensore_porta_chiusa_PORT, sensore_porta_chiusa_NUM) << 1;
        tasto_ex |= (uint8_t)Cy_GPIO_Read(citofono_PORT, citofono_NUM);

        buf[0] = tasto_ex;
        buf[1] = (uint8_t)(lis3mdl.value_x >> 8);
        buf[2] = (uint8_t)(lis3mdl.value_x & 0xff);
        buf[3] = (uint8_t)(lis3mdl.value_y >> 8);
        buf[4] = (uint8_t)(lis3mdl.value_y & 0xff);
        buf[5] = (uint8_t)(lis3mdl.value_z >> 8);
        buf[6] = (uint8_t)(lis3mdl.value_z & 0xff);

        Cy_RTC_GetDateAndTime(&orologio);

        buf[7]  = (uint8_t)orologio.date;
        buf[8]  = (uint8_t)orologio.month;
        buf[9]  = (uint8_t)orologio.year;
        buf[10] = (uint8_t)orologio.hour;
        buf[11] = (uint8_t)orologio.min;
        buf[12] = (uint8_t)orologio.sec;

        break;

    case send_rfid_ok:
        gapDevice[device].faseSend = send_null;

        command = 0x07;
        len = 16;

        buf[0] = 0x01;
        buf[1] = 0x01;

        break;

    case send_rfid_fail:
        gapDevice[device].faseSend = send_null;

        command = 0x07;
        len = 16;

        buf[0] = 0x01;
        buf[1] = 0x00;
        break;
        
    case send_crypto_key:
        // Inviata in chiaro !

        gapDevice[device].faseSend = send_null;
        crypto_en = 0;
        command = 0x20;
        len = 32;

        memcpy(&chiper[0], crypto.aes, 16);
        memcpy(&chiper[16], crypto.des, 8);

        GenerateRandomKey(8, &chiper[25]);

        //timerDisableCrypto = 0;
        flag.recovery  = 0;
        flag.crypto_en = 1;
        //CyDelay(100);
        break;

    case send_backdoor:
        // Inviata in chiaro !

        gapDevice[device].faseSend = send_null;
        crypto_en = 0;
        command = 0x29;
        len = 48;

        memcpy(&chiper[0],  crypto.aes, 16);
        memcpy(&chiper[16], crypto.des, 8);
        memcpy(&chiper[24], crypto.mastercode, 10);

        break;

    case send_random_number:
        command = 0x1c;
        len = 16;

        random_code[0] = 0;
        random_code[1] = 0;

        if (GenerateRandomKey(2, random_code) != CY_CRYPTO_SUCCESS)
        {
            random_code[0] = 255;
            random_code[1] = 255;
        }

        buf[0] = random_code[0];
        buf[1] = random_code[1];

        gapDevice[device].faseSend = send_null;
        break;

    case send_bdaddress:
        command = 0x37;
        len = 16;
        
        buf[0]  = bdAddress[0];
        buf[1]  = bdAddress[1];
        buf[2]  = bdAddress[2];
        buf[3]  = bdAddress[3];
        buf[4]  = bdAddress[4];
        buf[5]  = bdAddress[5];
        buf[6]  = par.localname[0];
        buf[7]  = par.localname[1];
        buf[8]  = par.localname[2];
        buf[9]  = par.localname[3];
        buf[10] = par.localname[4];
        buf[11] = par.localname[5];
        buf[12] = par.localname[6];
        buf[13] = par.localname[7];
        buf[14] = par.localname[8];
        buf[15] = par.localname[9];
        
        gapDevice[device].faseSend = send_null;
        break;
        
    case send_bdaddress_in_chiaro:
        command = 0x27;
        len = 48;
        
        crypto_en = 0;
        
        chiper[0]  = bdAddress[0];
        chiper[1]  = bdAddress[1];
        chiper[2]  = bdAddress[2];
        chiper[3]  = bdAddress[3];
        chiper[4]  = bdAddress[4];
        chiper[5]  = bdAddress[5];
        chiper[6]  = par.localname[0];
        chiper[7]  = par.localname[1];
        chiper[8]  = par.localname[2];
        chiper[9]  = par.localname[3];
        chiper[10] = par.localname[4];
        chiper[11] = par.localname[5];
        chiper[12] = par.localname[6];
        chiper[13] = par.localname[7];
        chiper[14] = par.localname[8];
        chiper[15] = par.localname[9];
        chiper[16] = 1; // DSMART
        chiper[17] = (uint8_t)( RELEASE_FW >> 8 );
        chiper[18] = (uint8_t)( RELEASE_FW & 0xff);
        chiper[19] = (uint8_t)( RELEASE_HW >> 8 );
        chiper[20] = (uint8_t)( RELEASE_HW & 0xff);
        chiper[21] = (uint8_t)( RELEASE_PROTO >> 8 );
        chiper[22] = (uint8_t)( RELEASE_PROTO & 0xff);
        chiper[23] = bridge.id[0];
        chiper[24] = bridge.id[1];
        chiper[25] = bridge.id[2];
        chiper[26] = bridge.id[3];
        chiper[27] = bridge.id[4];
        chiper[28] = bridge.id[5];
        chiper[29] = bridge.id[6];
        chiper[30] = bridge.id[7];
        chiper[31] = bridge.id[8];
        chiper[32] = bridge.id[9];
        chiper[33] = bridge.id[10];
        chiper[34] = bridge.id[11];
        chiper[35] = bridge.id[12];
        chiper[36] = bridge.id[13];
        chiper[37] = bridge.id[14];
        chiper[38] = bridge.id[15];
        chiper[39] = bridge.id[16];
        chiper[40] = bridge.id[17];
        chiper[41] = bridge.id[18];
        chiper[42] = bridge.id[19];

        
        gapDevice[device].faseSend = send_null;
        
        break;
        
    case send_bridgeid:
        command = 0x39;
        len = 32;
        
        memset( buf,0,32);
        memcpy( buf,bridge.id,20);
        
        gapDevice[device].faseSend = send_null;
        break;
        
    case send_end:
        gapDevice[device].faseSend = send_null;
        cUtente = 0;

        command = 0xff;
        len = 16;

        buf[0] = 0xff;
        break;

    case send_test:
        gapDevice[device].faseSend = send_null;

        crypto_en = 0;
        command   = 0x23;
        len       = 32;

        chiper[0]  = 't';
        chiper[1]  = 'e';
        chiper[2]  = 's';
        chiper[3]  = 't';
        chiper[4]  = ' ';
        chiper[5]  = 'b';
        chiper[6]  = 'l';
        chiper[7]  = 'e';
        chiper[8]  = ' ';
        chiper[9]  = 'o';
        chiper[10] = 'k';
        chiper[11] = ' ';
        chiper[12] = ' ';
        chiper[13] = ' ';
        chiper[14] = ' ';
        chiper[15] = ' ';

        leggo_magnetometro();

        tasto_ex = (uint8_t)Cy_GPIO_Read(sensore_porta_chiusa_PORT, sensore_porta_chiusa_NUM) << 1;
        tasto_ex |= (uint8_t)Cy_GPIO_Read(citofono_PORT, citofono_NUM);

        chiper[16] = tasto_ex;
        chiper[17] = (uint8_t)(lis3mdl.value_x >> 8);
        chiper[18] = (uint8_t)(lis3mdl.value_x & 0xff);
        chiper[19] = (uint8_t)(lis3mdl.value_y >> 8);
        chiper[20] = (uint8_t)(lis3mdl.value_y & 0xff);
        chiper[21] = (uint8_t)(lis3mdl.value_z >> 8);
        chiper[22] = (uint8_t)(lis3mdl.value_z & 0xff);

        Cy_RTC_GetDateAndTime(&orologio);

        chiper[23] = (uint8_t)orologio.date;
        chiper[24] = (uint8_t)orologio.month;
        chiper[25] = (uint8_t)orologio.year;
        chiper[26] = (uint8_t)orologio.hour;
        chiper[27] = (uint8_t)orologio.min;
        chiper[28] = (uint8_t)orologio.sec;
        
        chiper[29] = 0;
        chiper[30] = 0;
        chiper[31] = 0;

        break;

    case send_evento:
        command = 0x38;
        len = 32;
        
        buf[0]  = ultimoLog.utente[0];
        buf[1]  = ultimoLog.utente[1];
        buf[2]  = ultimoLog.utente[2];
        buf[3]  = ultimoLog.utente[3];
        buf[4]  = ultimoLog.utente[4];
        buf[5]  = ultimoLog.utente[5];
        buf[6]  = ultimoLog.utente[6];
        buf[7]  = ultimoLog.utente[7];
        buf[8]  = ultimoLog.utente[8];
        buf[9]  = ultimoLog.utente[9];
        buf[10] = ultimoLog.log.datetime.giorno;
        buf[11] = ultimoLog.log.datetime.mese;
        buf[12] = ultimoLog.log.datetime.anno;
        buf[13] = ultimoLog.log.datetime.ore;
        buf[14] = ultimoLog.log.datetime.minuti;
        buf[15] = ultimoLog.log.richiesta;
        buf[16] = ultimoLog.log.richiesta_eseguita;
        
        flag.ble_update_adv     = 1;
        flag.send_event_tocloud = 0;
        timer_send_event        = 0;
        
        gapDevice[device].faseSend = send_null;    
        break;
        
    case send_wifi_par:
        
        crypto_en = 0;
        command = 0x61;
        len     = 80;
        
        chiper[0] = wifi.len_ssid;
        chiper[1] = wifi.len_pass;
        chiper[2] = wifi.channel;
        
        memcpy( &chiper[3], wifi.ssid, wifi.len_ssid );
        memcpy( &chiper[3+wifi.len_ssid], wifi.pass, wifi.len_pass );
        
        flag.send_wifi_tobridge = 0;
        
        gapDevice[device].faseSend = send_null;    
        break;
        
    case send_error_code:
        gapDevice[device].faseSend = send_null;

        command = 0xf0;
        buf[0] = gapDevice[device].errCommand;  // errorCommand;
        buf[1] = gapDevice[device].errCode;     // errorCode;
        buf[2] = gapDevice[device].id_risposta;
        
        flag.send_error_to_cloud = 0;

        len = 16;
        break;
    }
    
    if (len % 16)
        return;

    if (len != 0)
    {
        if (crypto_en)
            crypto_aes_encrypt(chiper, buf, len);

        pacchetti = len / 16;

        while (len != 0)
        {
            memset(buftx, 0, MAX_BUF);
            buftx[0] = command;
            buftx[1] = pacchetto + 1;
            buftx[2] = pacchetti;
            buftx[3] = id_risposta;
            memcpy(&buftx[4], &chiper[pacchetto * 16], 16);
            ++pacchetto;

            gapDevice[device].notificationHandle.value.val = buftx;
            gapDevice[device].notificationHandle.value.len = 20;
            Cy_BLE_GATTS_SendNotification( &gapDevice[device].connHandle, &gapDevice[device].notificationHandle);
            do
            {
                Cy_BLE_ProcessEvents();
            } while (Cy_BLE_GATT_GetBusyStatus(gapDevice[device].connHandle.attId) != CY_BLE_STACK_STATE_FREE);
            
            CyDelay(60);
            len = len - 16;
        }
    }    
}

#define accesso_master()        (gapDevice[device].master_ok == 1 )
#define nessun_accesso_master() (gapDevice[device].master_ok == 0 )

uint8_t writePar(uint8_t *mes, int len, cy_stc_ble_conn_handle_t connHandle )
{
    eErrorCode errCode          = err_ok;
    uint8_t    gatt_err_code    = CY_BLE_GATT_ERR_NONE;
    
    uint16_t ui16;
    int ut = 0;

    uint8_t command = 0;
    uint8_t ini = 0;
    uint8_t pacchetto = 0;
    uint8_t pacchetti = 0;
    
    // Chi è che mi parla ?
    int device = CY_BLE_MAX_CONNECTION_INSTANCES;    
    for(ini=0;ini < CY_BLE_MAX_CONNECTION_INSTANCES;ini++)
    {
        if( gapDevice[ini].connHandle.attId == connHandle.attId && gapDevice[ini].connHandle.bdHandle == connHandle.bdHandle ) device = ini;
    }
    //     
    
    if ((len % 20) != 0)
        return CY_BLE_GATT_ERR_INVALID_ATTRIBUTE_LEN;

    // Il primo 4 byte sono trasmessi in chiaro
    
    gapDevice[device].id_risposta = mes[3];

    command   = mes[0];
    pacchetto = mes[1];
    pacchetti = mes[2];
    ini       = (pacchetto - 1) * 16;

    memcpy(&bufrx[ini], &mes[4], 16);

    if (pacchetti > MAX_PACKET)
        return CY_BLE_GATT_ERR_INVALID_ATTRIBUTE_LEN;
    
    if (pacchetto != pacchetti)
        return gatt_err_code;

    if (par.nuova == 1)
    {
        if (command < 0x20 || command > 0x2f)
            crypto_aes_decrypt(bufrx, buf, (pacchetti * 16));
    }
    else if (flag.crypto_en == 1)
    {
        if (command == 0x20)
            return CY_BLE_GATT_ERR_ATTRIBUTE_NOT_FOUND;

        else if (command < 0x21 || command > 0x2f)
            crypto_aes_decrypt(bufrx, buf, (pacchetti * 16));
    }
    else
    {
        if (command < 0x20 || command > 0x30)
            return CY_BLE_GATT_ERR_ATTRIBUTE_NOT_FOUND;
    }

    // messaggio ok
    //flag.master = 1;
    
    if( accesso_master() )
    {
        timeout_ble        = 300000;
        timeout_mastercode = 300000;
    }
    else
    {
        if ( timeout_ble < 20000 ) timeout_ble = 20000;
    }

    switch (command)
    {
    case cmd_write_par:
        // Parametri
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }

        par.en_tastiera_con_filo = 0; // buf[3];
        par.timezone = buf[2];
        par.forza_motore = buf[3];
        par.direzione_apertura = buf[4];
        par.tipo_alimentazione = buf[5];
        par.tipo_apertura      = buf[6];
        par.tipo_chiusura_set  = buf[7];
        par.tipo_movimentazione_chiusura = buf[8];
        par.tipo_sensore_chiusura = buf[9];

        par.tempo_chiusura = (uint16)buf[11] << 8;
        par.tempo_chiusura += (uint16)buf[12];
        par.tempo_apertura = (uint16)buf[26] << 8;
        par.tempo_apertura += (uint16)buf[27];

        par.limitazioni_corrente = (uint16)buf[13] << 8;
        par.limitazioni_corrente += (uint16)buf[14];

        par.output.anable = buf[16];
        par.output.stato_attivo = buf[17];
        par.output.quando_attivo = buf[18];
        par.output.ms_attivo = (uint16_t)buf[19] << 8;
        par.output.ms_attivo += (uint16_t)buf[20];

        par.tempo_scrocchio = (uint16_t)buf[21] << 8;
        par.tempo_scrocchio += (uint16_t)buf[22];

        par.en_citofono       = buf[23];
        par.velocita_apertura = buf[24];
        par.en_motore         = buf[25];
        par.velocita_rallentamento_in_chiusura = buf[28];

        if (par.tempo_chiusura > 10000)
            par.tempo_chiusura = 10000;
        if (par.tempo_apertura > 10000)
            par.tempo_apertura = 10000;
        if (par.limitazioni_corrente > 5000)
            par.limitazioni_corrente = 5000;
        if (par.limitazioni_corrente < 100)
            par.limitazioni_corrente = 100;
        if (par.forza_motore > 100)
            par.forza_motore = 100;
        if (par.velocita_apertura < 50)
            par.velocita_apertura = 50;
        if (par.velocita_apertura > 100)
            par.velocita_apertura = 100;

        if (salvaParametri() == false)
            gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;

        break;

    case cmd_edit_user:
        // Modifica l'utente in posizione ut
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }

        if (findUtente((char *)&buf[1], &ut))
        {
            utenti[ut].tipo = buf[0];

            utenti[ut].pass[0] = buf[11];
            utenti[ut].pass[1] = buf[12];
            utenti[ut].pass[2] = buf[13];
            utenti[ut].pass[3] = buf[14];
            utenti[ut].pass[4] = buf[15];
            utenti[ut].pass[5] = buf[16];
            utenti[ut].pass[6] = buf[17];
            utenti[ut].pass[7] = buf[18];

            utenti[ut].tastierino[0] = buf[19];
            utenti[ut].tastierino[1] = buf[20];
            utenti[ut].tastierino[2] = buf[21];
            utenti[ut].tastierino[3] = buf[22];
            utenti[ut].tastierino[4] = buf[23];
            utenti[ut].tastierino[5] = buf[24];

            utenti[ut].permessi = buf[25];

            utenti[ut].permessi_orari[0] = (uint64_t)buf[26] << 40;
            utenti[ut].permessi_orari[0] += (uint64_t)buf[27] << 32;
            utenti[ut].permessi_orari[0] += (uint64_t)buf[28] << 24;
            utenti[ut].permessi_orari[0] += (uint64_t)buf[29] << 16;
            utenti[ut].permessi_orari[0] += (uint64_t)buf[30] << 8;
            utenti[ut].permessi_orari[0] += (uint64_t)buf[31];

            utenti[ut].permessi_orari[1] = (uint64_t)buf[32] << 40;
            utenti[ut].permessi_orari[1] += (uint64_t)buf[33] << 32;
            utenti[ut].permessi_orari[1] += (uint64_t)buf[34] << 24;
            utenti[ut].permessi_orari[1] += (uint64_t)buf[35] << 16;
            utenti[ut].permessi_orari[1] += (uint64_t)buf[36] << 8;
            utenti[ut].permessi_orari[1] += (uint64_t)buf[37];

            utenti[ut].permessi_orari[2] = (uint64_t)buf[38] << 40;
            utenti[ut].permessi_orari[2] += (uint64_t)buf[39] << 32;
            utenti[ut].permessi_orari[2] += (uint64_t)buf[40] << 24;
            utenti[ut].permessi_orari[2] += (uint64_t)buf[41] << 16;
            utenti[ut].permessi_orari[2] += (uint64_t)buf[42] << 8;
            utenti[ut].permessi_orari[2] += (uint64_t)buf[43];

            utenti[ut].permessi_orari[3] = (uint64_t)buf[44] << 40;
            utenti[ut].permessi_orari[3] += (uint64_t)buf[45] << 32;
            utenti[ut].permessi_orari[3] += (uint64_t)buf[46] << 24;
            utenti[ut].permessi_orari[3] += (uint64_t)buf[47] << 16;
            utenti[ut].permessi_orari[3] += (uint64_t)buf[48] << 8;
            utenti[ut].permessi_orari[3] += (uint64_t)buf[49];

            utenti[ut].permessi_orari[4] = (uint64_t)buf[50] << 40;
            utenti[ut].permessi_orari[4] += (uint64_t)buf[51] << 32;
            utenti[ut].permessi_orari[4] += (uint64_t)buf[52] << 24;
            utenti[ut].permessi_orari[4] += (uint64_t)buf[53] << 16;
            utenti[ut].permessi_orari[4] += (uint64_t)buf[54] << 8;
            utenti[ut].permessi_orari[4] += (uint64_t)buf[55];

            utenti[ut].permessi_orari[5] = (uint64_t)buf[56] << 40;
            utenti[ut].permessi_orari[5] += (uint64_t)buf[57] << 32;
            utenti[ut].permessi_orari[5] += (uint64_t)buf[58] << 24;
            utenti[ut].permessi_orari[5] += (uint64_t)buf[59] << 16;
            utenti[ut].permessi_orari[5] += (uint64_t)buf[60] << 8;
            utenti[ut].permessi_orari[5] += (uint64_t)buf[61];

            utenti[ut].permessi_orari[6] = (uint64_t)buf[62] << 40;
            utenti[ut].permessi_orari[6] += (uint64_t)buf[63] << 32;
            utenti[ut].permessi_orari[6] += (uint64_t)buf[64] << 24;
            utenti[ut].permessi_orari[6] += (uint64_t)buf[65] << 16;
            utenti[ut].permessi_orari[6] += (uint64_t)buf[66] << 8;
            utenti[ut].permessi_orari[6] += (uint64_t)buf[67];

            utenti[ut].affitto.giorno = buf[70];
            utenti[ut].affitto.mese   = buf[71];
            utenti[ut].affitto.anno   = buf[72];
            utenti[ut].affitto.ore    = buf[73];
            utenti[ut].affitto.orePermanenza  = (uint16_t)buf[74] << 8;
            utenti[ut].affitto.orePermanenza += (uint16_t)buf[75];

            fram_wakeup();
            if (writeUtente(ut, &utenti[ut]) == false)
                gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            fram_sleep();
            
            if( gapDevice[device].is_bridge )
            {
                id_risposta_cloud      = gapDevice[device].id_risposta;
                utente_cloud_tipo      = 0;     // modficato
                utente_cloud           = ut;
                gapDevice[device].faseSend     = send_user_to_cloud;
                gapDevice[device].faseSendNext = send_null;
            }
            else if( bridge.accoppiato )
            {
                id_risposta_cloud      = gapDevice[device].id_risposta;
                utente_cloud_tipo      = 0;     // modficato
                utente_cloud           = ut;
                flag.send_user_tocloud = 1;
                flag.ble_update_adv    = 1;
            }
        }
        else
        {
            gatt_err_code = CY_BLE_GATT_ERR_ATTRIBUTE_NOT_FOUND;
        }

        break;

    case cmd_add_user:
        // Aggiunge un utente
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }

        memset(&utente_tmp, 0, sizeof(sUtente));
        utente_tmp.tipo = buf[0];

        utente_tmp.nome[0] = buf[1];
        utente_tmp.nome[1] = buf[2];
        utente_tmp.nome[2] = buf[3];
        utente_tmp.nome[3] = buf[4];
        utente_tmp.nome[4] = buf[5];
        utente_tmp.nome[5] = buf[6];
        utente_tmp.nome[6] = buf[7];
        utente_tmp.nome[7] = buf[8];
        utente_tmp.nome[8] = buf[9];
        utente_tmp.nome[9] = buf[10];

        if (utente_tmp.tipo == utente_residente || utente_tmp.tipo == utente_aziendale )
        {

            utente_tmp.pass[0] = buf[11];
            utente_tmp.pass[1] = buf[12];
            utente_tmp.pass[2] = buf[13];
            utente_tmp.pass[3] = buf[14];
            utente_tmp.pass[4] = buf[15];
            utente_tmp.pass[5] = buf[16];
            utente_tmp.pass[6] = buf[17];
            utente_tmp.pass[7] = buf[18];

            utente_tmp.tastierino[0] = buf[19];
            utente_tmp.tastierino[1] = buf[20];
            utente_tmp.tastierino[2] = buf[21];
            utente_tmp.tastierino[3] = buf[22];
            utente_tmp.tastierino[4] = buf[23];
            utente_tmp.tastierino[5] = buf[24];

            utente_tmp.permessi = buf[25];

            utente_tmp.permessi_orari[0] = (uint64_t)buf[26] << 40;
            utente_tmp.permessi_orari[0] += (uint64_t)buf[27] << 32;
            utente_tmp.permessi_orari[0] += (uint64_t)buf[28] << 24;
            utente_tmp.permessi_orari[0] += (uint64_t)buf[29] << 16;
            utente_tmp.permessi_orari[0] += (uint64_t)buf[30] << 8;
            utente_tmp.permessi_orari[0] += (uint64_t)buf[31];

            utente_tmp.permessi_orari[1] = (uint64_t)buf[32] << 40;
            utente_tmp.permessi_orari[1] += (uint64_t)buf[33] << 32;
            utente_tmp.permessi_orari[1] += (uint64_t)buf[34] << 24;
            utente_tmp.permessi_orari[1] += (uint64_t)buf[35] << 16;
            utente_tmp.permessi_orari[1] += (uint64_t)buf[36] << 8;
            utente_tmp.permessi_orari[1] += (uint64_t)buf[37];

            utente_tmp.permessi_orari[2] = (uint64_t)buf[38] << 40;
            utente_tmp.permessi_orari[2] += (uint64_t)buf[39] << 32;
            utente_tmp.permessi_orari[2] += (uint64_t)buf[40] << 24;
            utente_tmp.permessi_orari[2] += (uint64_t)buf[41] << 16;
            utente_tmp.permessi_orari[2] += (uint64_t)buf[42] << 8;
            utente_tmp.permessi_orari[2] += (uint64_t)buf[43];

            utente_tmp.permessi_orari[3] = (uint64_t)buf[44] << 40;
            utente_tmp.permessi_orari[3] += (uint64_t)buf[45] << 32;
            utente_tmp.permessi_orari[3] += (uint64_t)buf[46] << 24;
            utente_tmp.permessi_orari[3] += (uint64_t)buf[47] << 16;
            utente_tmp.permessi_orari[3] += (uint64_t)buf[48] << 8;
            utente_tmp.permessi_orari[3] += (uint64_t)buf[49];

            utente_tmp.permessi_orari[4] = (uint64_t)buf[50] << 40;
            utente_tmp.permessi_orari[4] += (uint64_t)buf[51] << 32;
            utente_tmp.permessi_orari[4] += (uint64_t)buf[52] << 24;
            utente_tmp.permessi_orari[4] += (uint64_t)buf[53] << 16;
            utente_tmp.permessi_orari[4] += (uint64_t)buf[54] << 8;
            utente_tmp.permessi_orari[4] += (uint64_t)buf[55];

            utente_tmp.permessi_orari[5] = (uint64_t)buf[56] << 40;
            utente_tmp.permessi_orari[5] += (uint64_t)buf[57] << 32;
            utente_tmp.permessi_orari[5] += (uint64_t)buf[58] << 24;
            utente_tmp.permessi_orari[5] += (uint64_t)buf[59] << 16;
            utente_tmp.permessi_orari[5] += (uint64_t)buf[60] << 8;
            utente_tmp.permessi_orari[5] += (uint64_t)buf[61];

            utente_tmp.permessi_orari[6] = (uint64_t)buf[62] << 40;
            utente_tmp.permessi_orari[6] += (uint64_t)buf[63] << 32;
            utente_tmp.permessi_orari[6] += (uint64_t)buf[64] << 24;
            utente_tmp.permessi_orari[6] += (uint64_t)buf[65] << 16;
            utente_tmp.permessi_orari[6] += (uint64_t)buf[66] << 8;
            utente_tmp.permessi_orari[6] += (uint64_t)buf[67];
        }
        else if (utente_tmp.tipo == utente_affittuario)
        {
            utente_tmp.affitto.giorno = buf[11];
            utente_tmp.affitto.mese = buf[12];
            utente_tmp.affitto.anno = buf[13];
            utente_tmp.affitto.ore = buf[14];
            utente_tmp.affitto.orePermanenza = (uint16_t)buf[15] << 8;
            utente_tmp.affitto.orePermanenza += (uint16_t)buf[16];

            utente_tmp.tastierino[0] = buf[17];
            utente_tmp.tastierino[1] = buf[18];
            utente_tmp.tastierino[2] = buf[19];
            utente_tmp.tastierino[3] = buf[20];
            utente_tmp.tastierino[4] = buf[21];
            utente_tmp.tastierino[5] = buf[22];

            utente_tmp.permessi = permesso_tastierino;
        }
        else
        {
            gatt_err_code = CY_BLE_GATT_ERR_ATTRIBUTE_NOT_FOUND;
            break;
        }

        errCode = addUtente(utente_tmp);
        if (errCode == err_ok)
        {
            ut = utenti_registrati - 1;
            
            fram_wakeup();
            if (writeUtente(ut, &utenti[ut]) == false)
                gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            
            if (fram_writeArray(ADDRESS_NUM_UTENTI, (uint8_t *)&utenti_registrati, 2) == false)
                gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            fram_sleep();
            
            if( gapDevice[device].is_bridge )
            {
                id_risposta_cloud      = gapDevice[device].id_risposta;
                utente_cloud_tipo      = 1; // nuovo
                utente_cloud           = ut;
                gapDevice[device].faseSend     = send_user_to_cloud;
                gapDevice[device].faseSendNext = send_null;
            }
            else if( bridge.accoppiato )
            {
                id_risposta_cloud      = gapDevice[device].id_risposta;
                utente_cloud_tipo      = 1; // nuovo
                utente_cloud           = ut;
                flag.send_user_tocloud = 1;
                flag.ble_update_adv    = 1;
            }
        }
        else
        {
            gapDevice[device].faseSend   = send_error_code;
            gapDevice[device].errCode    = errCode;
            gapDevice[device].errCommand = 0x03;
            
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
        }

        break;

    case cmd_del_user:
        // Elimina un utente
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }
        if (findUtente((char *)&buf[0], &ut))
        {
            // Savlo il nome utente per avvisare il cloud dell'eliminazione
            if( gapDevice[device].is_bridge )
            {
                memcpy( &utente_eliminato, &utenti[ut], sizeof( sUtente ));
                id_risposta_cloud        = gapDevice[device].id_risposta;
                utente_cloud_tipo        = 2; // eliminato
                gapDevice[device].faseSend     = send_user_to_cloud;
                gapDevice[device].faseSendNext = send_null;
            }
            else if( bridge.accoppiato )
            {
                memcpy( &utente_eliminato, &utenti[ut], sizeof( sUtente ));
                
                id_risposta_cloud        = gapDevice[device].id_risposta;
                flag.send_user_tocloud   = 1;
                flag.ble_update_adv      = 1;
                utente_cloud_tipo        = 2; // eliminato
            }
            
            removeUtente(ut);
            flag.save_utenti = 1;            
        }
        else
            gatt_err_code = CY_BLE_GATT_ERR_ATTRIBUTE_NOT_FOUND;
        break;
        
    case cmd_del_user_affitto:
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }
        
        if( findAffittuario((char*)&buf[0], (char*)&buf[10], &ut) )
        {
            // Savlo il nome utente per avvisare il cloud dell'eliminazione
            if( gapDevice[device].is_bridge )
            {
                memcpy( &utente_eliminato, &utenti[ut], sizeof( sUtente ));
                id_risposta_cloud              = gapDevice[device].id_risposta;
                utente_cloud_tipo              = 2; // eliminato
                gapDevice[device].faseSend     = send_user_to_cloud;
                gapDevice[device].faseSendNext = send_null;
            }
            if( bridge.accoppiato )
            {
                memcpy( &utente_eliminato, &utenti[ut], sizeof( sUtente ));
                id_risposta_cloud        = gapDevice[device].id_risposta;
                flag.send_user_tocloud   = 1;
                flag.ble_update_adv      = 1;
                utente_cloud_tipo        = 2; // eliminato
            }
            removeUtente(ut);
            flag.save_utenti = 1;            
        }
        
        break;

    case cmd_set_clock:
        // Set orologio
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }

        if (buf[0] == 0 || buf[0] > 31)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[1] == 0 || buf[1] > 12)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[2] == 0 || buf[2] > 99)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[3] == 0 || buf[3] > 24)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[4] == 0 || buf[4] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[5] == 0 || buf[5] > 7)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;

        if (gatt_err_code != CY_BLE_GATT_ERR_INVALID_HANDLE)
        {
            orologio.date = buf[0];
            orologio.month = buf[1];
            orologio.year = buf[2];
            orologio.hour = buf[3];
            orologio.min = buf[4];
            orologio.dayOfWeek = buf[5];

            orologio.sec = 0;
            Cy_RTC_SetDateAndTime(&orologio);
            orologio_ok = 1;
        }
        break;

    case 0x07:
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }

        // Associa un RFID ad un utente
        utente_tmp.nome[0] = buf[0];
        utente_tmp.nome[1] = buf[1];
        utente_tmp.nome[2] = buf[2];
        utente_tmp.nome[3] = buf[3];
        utente_tmp.nome[4] = buf[4];
        utente_tmp.nome[5] = buf[5];
        utente_tmp.nome[6] = buf[6];
        utente_tmp.nome[7] = buf[7];
        utente_tmp.nome[8] = buf[8];
        utente_tmp.nome[9] = buf[9];

        utente_tmp.pass[0] = buf[10];
        utente_tmp.pass[1] = buf[11];
        utente_tmp.pass[2] = buf[12];
        utente_tmp.pass[3] = buf[13];
        utente_tmp.pass[4] = buf[14];
        utente_tmp.pass[5] = buf[15];
        utente_tmp.pass[6] = buf[16];
        utente_tmp.pass[7] = buf[17];

        // l'utente esiste ?
        if (findUtente((char *)&utente_tmp.nome[0], &ut))
        {
            // password corretta ?
            if (verificaCredenziali(ut, utente_tmp.pass) == true)
            {
                utente_rfid       = ut;
                flag.associa_rfid = 1;
                
                device_a_cui_rispondere = device;
                
                break;
            }
        }
        gapDevice[device].faseSend   = send_error_code;
        gapDevice[device].errCode    = err_credenziali;
        gapDevice[device].errCommand = 0x08;

        gatt_err_code = CY_BLE_GATT_ERR_ATTRIBUTE_NOT_FOUND;
        break;
    case 0x08:
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }

        // Associa/ Elimina un Fingerprint ad un utente
        utente_tmp.nome[0] = buf[0];
        utente_tmp.nome[1] = buf[1];
        utente_tmp.nome[2] = buf[2];
        utente_tmp.nome[3] = buf[3];
        utente_tmp.nome[4] = buf[4];
        utente_tmp.nome[5] = buf[5];
        utente_tmp.nome[6] = buf[6];
        utente_tmp.nome[7] = buf[7];
        utente_tmp.nome[8] = buf[8];
        utente_tmp.nome[9] = buf[9];

        utente_tmp.pass[0] = buf[10];
        utente_tmp.pass[1] = buf[11];
        utente_tmp.pass[2] = buf[12];
        utente_tmp.pass[3] = buf[13];
        utente_tmp.pass[4] = buf[14];
        utente_tmp.pass[5] = buf[15];
        utente_tmp.pass[6] = buf[16];
        utente_tmp.pass[7] = buf[17];

        if( buf[18] == 1 )
        {
            // ASSOCIA UN FINGERPRINT        
            // l'utente esiste ?
            if (findUtente((char *)&utente_tmp.nome[0], &ut))
            {
                // password corretta ?
                if (verificaCredenziali(ut, utente_tmp.pass) == true)
                {
                    utente_fingerprint = ut;            
                    // l'Utente aveva gia un impronta memorizzata e devo sovrascriverla ?
                    if( utenti[ut].impronta != 0 ) codice_impronta = utenti[ut].impronta;
                    else
                    {
                        // Trovo un codice impronta libero !
                        codice_impronta = findfreeFingerprint();                    
                    }
                    if( codice_impronta != 0 )                     
                    {
                        device_a_cui_rispondere  = device;
                        flag.associa_fingerprint = 1;
                    }
                    break;
                }
            }
        }
        if( buf[18] == 2 )
        {
            // ELIMINA UN FINGERPRINT        
            // l'utente esiste ?
            if (findUtente((char *)&utente_tmp.nome[0], &ut))
            {
                // password corretta ?
                if (verificaCredenziali(ut, utente_tmp.pass) == true)
                {
                    // elimino l'associazione alla sua impronta
                    utenti[ut].impronta = 0;                    
                    fram_wakeup();
                    if (writeUtente(ut, &utenti[ut]) == false)
                        gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
                    fram_sleep();
                }
            }
        }
        if( buf[18] == 3 )
        {
            // Elimina tutti i fingerprint memorizzati sulla fingerboard 
            codice_impronta = 200;
            flag.associa_fingerprint = 1;
            break;
        }
        
        gapDevice[device].faseSend   = send_error_code;
        gapDevice[device].errCode    = err_credenziali;
        gapDevice[device].errCommand = 0x08;

        gatt_err_code = CY_BLE_GATT_ERR_ATTRIBUTE_NOT_FOUND;
        break;
    case 0x10:
        // Richiede se master code ok
        // Se sbagliato rispondo con CY_BLE_GATT_ERR_INVALID_HANDLE
        if (memcmp(buf, crypto.mastercode, 10) == 0)
        {
            timeout_ble        = 300000;
            timeout_mastercode = 300000;
            gapDevice[device].master_ok = 1;
        }
        else
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        
        break;

    case 0x11:
        // Apri porta
        memset(&utente_tmp, 0, sizeof(sUtente));

        utente_tmp.nome[0] = buf[0];
        utente_tmp.nome[1] = buf[1];
        utente_tmp.nome[2] = buf[2];
        utente_tmp.nome[3] = buf[3];
        utente_tmp.nome[4] = buf[4];
        utente_tmp.nome[5] = buf[5];
        utente_tmp.nome[6] = buf[6];
        utente_tmp.nome[7] = buf[7];
        utente_tmp.nome[8] = buf[8];
        utente_tmp.nome[9] = buf[9];

        utente_tmp.pass[0] = buf[10];
        utente_tmp.pass[1] = buf[11];
        utente_tmp.pass[2] = buf[12];
        utente_tmp.pass[3] = buf[13];
        utente_tmp.pass[4] = buf[14];
        utente_tmp.pass[5] = buf[15];
        utente_tmp.pass[6] = buf[16];
        utente_tmp.pass[7] = buf[17];

        verifi_code[0] = buf[18];
        verifi_code[1] = buf[19];
        
        // è stato il ripetitore ad aprire ?
        if( buf[26] == 'r' && buf[27] == 'i' && buf[28] == 'p' )
        {
            if( buf[0] == 'm' && buf[1] == 'a' && buf[2] == 's' && buf[3] == 't' && buf[4] == 'e' && buf[5] == 'r' )
            {
                if( verifi_code[0] == random_code[0] && verifi_code[1] == random_code[1] )
                    flag.apri_porta = 1;      
                else
                    gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE ;
            }
            else
                gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
            break;
        }
        // l'utente esiste ?
        if (findUtente((char *)&utente_tmp.nome[0], &ut))
        {
            // password corretta ?
            if (verificaCredenziali(ut, utente_tmp.pass) == true)
            {
                // posso aprire adesso ?
                if( buf[26] == 'w' && buf[27] == 'e' && buf[28] == 'b' )
                {
                    // Apertura da WEB    
                    verifi_code[0] = random_code[0];
                    verifi_code[1] = random_code[1];
                    
                    errCode = verificaPermessoApertura(ut, permesso_cloud);
                    if (errCode == err_ok)
                    {
                        salvoLogUtente(ut, apertura_da_cloud, evento_eseguito);
                        ++logUfg.totale_aperture;

                        flag.send_event_tocloud = 1;
                        flag.ble_update_adv     = 1;
                        flag.save_logufg        = 1;                    
                        flag.apri_porta         = 1;
                        break;
                    }
                    else
                    {
                        salvoLogUtente(ut, apertura_da_cloud, evento_non_eseguito);
                        flag.send_event_tocloud = 1;
                        flag.ble_update_adv     = 1;
                    }
                }
                else
                {   
                    // Apertura da smartphone   
                    if (orologio_ok == 0)
                    {
                        // Se non ho ancora settato l'orario sfrutto quello che mi arriva alla prima apertura
                        if (buf[20] == 0 || buf[20] > 31)
                            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
                        if (buf[21] == 0 || buf[21] > 12)
                            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
                        if (buf[22] == 0 || buf[22] > 99)
                            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
                        if (buf[23] == 0 || buf[23] > 24)
                            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
                        if (buf[24] == 0 || buf[24] > 59)
                            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
                        if (buf[25] == 0 || buf[25] > 7)
                            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;

                        if (gatt_err_code == CY_BLE_GATT_ERR_NONE)
                        {
                            orario_apertura.date = buf[20];
                            orario_apertura.month = buf[21];
                            orario_apertura.year = buf[22];
                            orario_apertura.hour = buf[23];
                            orario_apertura.min = buf[24];
                            orario_apertura.dayOfWeek = buf[25];

                            Cy_RTC_SetDateAndTime(&orario_apertura);
                            orologio_ok = 1;
                        }
                    }
                    errCode = verificaPermessoApertura(ut, permesso_smartphone);
                    if (errCode == err_ok)
                    {
                        salvoLogUtente(ut, apertura_da_smartphone, evento_eseguito);
                        ++logUfg.totale_aperture;

                        flag.send_event_tocloud = 1;
                        flag.ble_update_adv     = 1;
                        flag.save_logufg = 1;                    
                        flag.apri_porta  = 1;
                        break;
                    }
                    else
                    {
                        salvoLogUtente(ut, apertura_da_smartphone, evento_non_eseguito);
                        flag.send_event_tocloud = 1;
                        flag.ble_update_adv     = 1;
                    }
                }                
                writeUtente(ut, &utenti[ut]);
            }
            else
            {
                errCode = err_credenziali;
            }
        }
        else
        {
            // Potrebbe essere un apertura da master code
            if (utente_tmp.nome[0] == 'm' &&
                utente_tmp.nome[1] == 'a' &&
                utente_tmp.nome[2] == 's' &&
                utente_tmp.nome[3] == 't' &&
                utente_tmp.nome[4] == 'e' &&
                utente_tmp.nome[5] == 'r')
            {
                if (utente_tmp.pass[0] == crypto.mastercode[0] &&
                    utente_tmp.pass[1] == crypto.mastercode[1] &&
                    utente_tmp.pass[2] == crypto.mastercode[2] &&
                    utente_tmp.pass[3] == crypto.mastercode[3] &&
                    utente_tmp.pass[4] == crypto.mastercode[4] &&
                    utente_tmp.pass[5] == crypto.mastercode[5] &&
                    utente_tmp.pass[6] == crypto.mastercode[6] &&
                    utente_tmp.pass[7] == crypto.mastercode[7] &&
                    utente_tmp.pass[8] == crypto.mastercode[8] &&
                    utente_tmp.pass[9] == crypto.mastercode[9])
                {
                    flag.apri_porta = 1;
                    break;
                }
            }
            // Se non è nemmeno un utente master allora setto l'errore
            errCode = err_credenziali;           
        }        

        if( errCode == err_credenziali ) gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
        
//        cy_stc_rtc_config_t orologio;
//        Cy_RTC_GetDateAndTime(&orologio);
//        if( gapDevice[device].is_bridge == true )
//        {
//            ultimoLog.utente[0] = utente_tmp.nome[0];
//            ultimoLog.utente[1] = utente_tmp.nome[1];
//            ultimoLog.utente[2] = utente_tmp.nome[2];
//            ultimoLog.utente[3] = utente_tmp.nome[3];
//            ultimoLog.utente[4] = utente_tmp.nome[4];
//            ultimoLog.utente[5] = utente_tmp.nome[5];
//            ultimoLog.utente[6] = utente_tmp.nome[6];
//            ultimoLog.utente[7] = utente_tmp.nome[7];
//            ultimoLog.utente[8] = utente_tmp.nome[8];
//            ultimoLog.utente[9] = utente_tmp.nome[9];
//            ultimoLog.log.datetime.giorno    = orologio.date;
//            ultimoLog.log.datetime.mese      = orologio.month;
//            ultimoLog.log.datetime.anno      = orologio.year;
//            ultimoLog.log.datetime.ore       = orologio.hour;
//            ultimoLog.log.datetime.minuti    = orologio.min;
//            if( buf[26] == 'w' && buf[27] == 'e' && buf[28] == 'b' )                    
//                ultimoLog.log.richiesta          = apertura_da_cloud;
//            else
//                ultimoLog.log.richiesta          = apertura_da_smartphone;                    
//            ultimoLog.log.richiesta_eseguita = evento_non_eseguito;
//            flag.send_event_tocloud = 1;
//            flag.ble_update_adv     = 1;
//        }
//        else
        {
            gapDevice[device].faseSend   = send_error_code;
            gapDevice[device].errCode    = errCode;
            gapDevice[device].errCommand = 0x11;
        }
        break;

    case 0x12:
        // Chiudi porta
        memset(&utente_tmp, 0, sizeof(sUtente));

        utente_tmp.nome[0] = buf[0];
        utente_tmp.nome[1] = buf[1];
        utente_tmp.nome[2] = buf[2];
        utente_tmp.nome[3] = buf[3];
        utente_tmp.nome[4] = buf[4];
        utente_tmp.nome[5] = buf[5];
        utente_tmp.nome[6] = buf[6];
        utente_tmp.nome[7] = buf[7];
        utente_tmp.nome[8] = buf[8];
        utente_tmp.nome[9] = buf[9];

        utente_tmp.pass[0] = buf[10];
        utente_tmp.pass[1] = buf[11];
        utente_tmp.pass[2] = buf[12];
        utente_tmp.pass[3] = buf[13];
        utente_tmp.pass[4] = buf[14];
        utente_tmp.pass[5] = buf[15];
        utente_tmp.pass[6] = buf[16];
        utente_tmp.pass[7] = buf[17];

        // l'utente esiste ?
        if (findUtente((char *)&utente_tmp.nome[0], &ut))
        {
            // password corretta ?
            if (verificaCredenziali(ut, utente_tmp.pass) == true)
            {
                if( buf[26] == 'w' && buf[27] == 'e' && buf[28] == 'b' )
                {
                    // Chiusura da cloud    
                    if (verificaPermessoChiusura(ut, permesso_cloud ) == err_ok)
                    {
                        salvoLogUtente(ut, chiusura_da_cloud , evento_eseguito);
                        ++logUfg.totale_chiusure;

                        flag.send_event_tocloud = 1;
                        flag.save_logufg  = 1;
                        flag.save_utenti  = 1;
                        flag.chiudi_porta = 1;
                        break;
                    }
                    else
                    {
                        salvoLogUtente(ut, chiusura_da_cloud , evento_non_eseguito);
                        flag.send_event_tocloud = 1;
                        flag.save_utenti = 1;
                        errCode = err_permesso_telefono;
                    }
                }
                else
                {
                    // Chiusura da smartphone    
                    if (verificaPermessoChiusura(ut, permesso_smartphone) == err_ok)
                    {
                        salvoLogUtente(ut, chiusura_da_smartphone, evento_eseguito);
                        ++logUfg.totale_chiusure;

                        flag.send_event_tocloud = 1;
                        flag.save_logufg  = 1;
                        flag.save_utenti  = 1;
                        flag.chiudi_porta = 1;
                        break;
                    }
                    else
                    {
                        salvoLogUtente(ut, chiusura_da_smartphone, evento_non_eseguito);
                        flag.send_event_tocloud = 1;
                        flag.save_utenti = 1;
                        errCode = err_permesso_telefono;
                    }
                }
                
            }
            else
                errCode = err_credenziali;
        }
        else
            errCode = err_credenziali;

        gapDevice[device].faseSend   = send_error_code;
        gapDevice[device].errCode    = errCode;
        gapDevice[device].errCommand = 0x12;
        
        gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;

        break;

    case 0x13:
        // delete log
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }

        if (buf[0] == 1)
        {
            // Elimino i log di un utente
            utente_tmp.nome[0] = buf[1];
            utente_tmp.nome[1] = buf[2];
            utente_tmp.nome[2] = buf[3];
            utente_tmp.nome[3] = buf[4];
            utente_tmp.nome[4] = buf[5];
            utente_tmp.nome[5] = buf[6];
            utente_tmp.nome[6] = buf[7];
            utente_tmp.nome[7] = buf[8];
            utente_tmp.nome[8] = buf[9];
            utente_tmp.nome[9] = buf[10];
            // l'utente esiste ?
            if (findUtente((char *)&utente_tmp.nome[0], &ut))
            {
                eliminoLogUtente(ut);
                flag.save_utenti = 1;
            }
            break;
        }
        else if (buf[0] == 0xff)
        {
            // Elimino tutti i log
            eliminoLogUtente(0xff);
            flag.save_utenti = 1;
        }

        break;

    case 0x14:
        // read dati
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }
        
        if (buf[0] == 1)
        {
            // Invio tutto
            gapDevice[device].faseSend     = send_par1;
            gapDevice[device].faseSendNext = send_chiusure;
        }
        else if (buf[0] == 2)
        {
            // Invio solo i parametri
            gapDevice[device].faseSend     = send_par1;
            gapDevice[device].faseSendNext = send_null;
        }
        else if (buf[0] == 3)
        {
            // Invio solo gli utenti
            gapDevice[device].faseSend     = send_users;
            gapDevice[device].faseSendNext = send_end;
        }
        else if( buf[0] == 4 )        
        {
            // Invio solo le chiusure automatiche programmate
            gapDevice[device].faseSend     = send_chiusure;
            gapDevice[device].faseSendNext = send_null;
        }        
        else        
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
            
        break;

    case 0x15:
        // Read Sensori
        if (buf[0] == 0x01)
        {    
            gapDevice[device].faseSend = send_sensors;
            NVIC_EnableIRQ(SysInt_port10_cfg.intrSrc);
        }   
        else if (buf[0] == 0x02)
        {
            gapDevice[device].faseSend = send_sensors_cont;
            NVIC_DisableIRQ(SysInt_port10_cfg.intrSrc);
        }
        else
        {
            gapDevice[device].faseSend = send_null;
            NVIC_EnableIRQ(SysInt_port10_cfg.intrSrc);
        }
        break;

    case 0x16:
        // Richiesta dell' orologio
        if (buf[0] != 1)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[1] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[2] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[3] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else
            gapDevice[device].faseSend = send_orologio;
        break;

    case 0x17:
        // Salva stato accelerometri per porta chiusa
        if (buf[0] > 2)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[1] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[2] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[3] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else
        {
            if (buf[0] == 1)
                flag.calibra_porta_chiusa = 1;
            else if (buf[0] == 2)
                flag.calibra_serratura_chiusa = 1;
        }

        break;

    case 0x18:
        // Imposta il nome della serratura ( appare nel local name del BLE )

        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }

        par.localname[0] = buf[0];
        par.localname[1] = buf[1];
        par.localname[2] = buf[2];
        par.localname[3] = buf[3];
        par.localname[4] = buf[4];
        par.localname[5] = buf[5];
        par.localname[6] = buf[6];
        par.localname[7] = buf[7];
        par.localname[8] = buf[8];
        par.localname[9] = 0;

        Cy_BLE_SetLocalName((const char *)par.localname);

        flag.ble_update_adv = 1;
        flag.save_par = 1;

        //timeout_ble = 500;
        break;

    case 0x19:
        // eseguo una calibrazione della corsa della serratura
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }

        if (buf[0] != 1)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[1] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[2] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[3] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else
        {
            device_a_cui_rispondere = device;
            flag.calibra_corsa = 1;
        }

        break;

    case 0x1a:
        // Modifico la password master
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }

        crypto.mastercode[0] = buf[0];
        crypto.mastercode[1] = buf[1];
        crypto.mastercode[2] = buf[2];
        crypto.mastercode[3] = buf[3];
        crypto.mastercode[4] = buf[4];
        crypto.mastercode[5] = buf[5];
        crypto.mastercode[6] = buf[6];
        crypto.mastercode[7] = buf[7];
        crypto.mastercode[8] = buf[8];
        crypto.mastercode[9] = buf[9];

        crypto.crc = CRC8((unsigned char *)&crypto, sizeof(sCryptoKey) - 1);

        fram_wakeup();
        if (fram_writeArray(ADDRESS_KEY, (uint8_t *)&crypto, sizeof(sCryptoKey)) == false)
        {
            // se fallisce la scrittura riprendo la copia che avevo in FR
            fram_readArray(ADDRESS_KEY_COPY, (uint8_t *)&crypto, sizeof(sCryptoKey));
            gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
        }
        else
        {
            // Se tutto ok Salvo anche la copia ( pregando che vada a buon fine )
            fram_writeArray(ADDRESS_KEY_COPY, (uint8_t *)&crypto, sizeof(sCryptoKey));
        }
        fram_sleep();

        //flag.save_crypto = 1;
        break;

    case 0x1b:
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }
        if (buf[1] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[2] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[3] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        
        else if (buf[0] == 0x01)
        {
            // Abilita l'accoppiamento con un altro telefono ( passaggio della chiave in chiaro )
            //timerDisableCrypto = 300000;
            flag.crypto_en = 0;
            flag.ble_accoppia_telefono = 1;
            flag.ble_update_adv = 1;
            device_a_cui_rispondere = device;
        }
        else if (buf[0] == 0x02  )
        {
            // Abilita l'accoppiamento con una tastiera ( passaggio della chiave in chiaro )
            // timerDisableCrypto = 300000;
            flag.crypto_en             = 0;
            flag.ble_accoppia_tastiera = 1;
            flag.ble_update_adv        = 1;
            device_a_cui_rispondere    = device;
        }
        else if ( buf[0] == 0x03 )
        {
            // Abilita l'accoppiamento con un lettore fingerprint NUOVO ( passaggio della chiave in chiaro )
            par.id_fingerprint[0] = 0;
            par.id_fingerprint[1] = 0;
            par.id_fingerprint[2] = 0;
            par.id_fingerprint[3] = 0;
            par.id_fingerprint[4] = 0;
            par.id_fingerprint[5] = 0;
            par.id_fingerprint[6] = 0;
            par.id_fingerprint[7] = 0;
            
            for( ut= 0; ut < utenti_registrati; ++ut )
            {
                utenti[ut].impronta = 0;    
            }
            
            flag.save_utenti   = 1;
            flag.save_par      = 1;
            flag.crypto_en     = 0;
            flag.ble_accoppia_tastiera = 1;
            flag.ble_update_adv        = 1;
            device_a_cui_rispondere    = device;
        }
        else if ( buf[0] == 0x04 )
        {
            // Abilita l'accoppiamento con un lettore fingerprint USATO PRECEDENTEMENTE ( passaggio della chiave in chiaro )            
            flag.crypto_en             = 0;
            flag.ble_accoppia_tastiera = 1;
            flag.ble_update_adv        = 1;
            device_a_cui_rispondere    = device;
        }        
        else if ( buf[0] == 0x05 )
        {
            // Abilita l'accoppiamento con un BRIDGE
            flag.ble_accoppia_bridge = 1;
            flag.ble_update_adv      = 1;
            device_a_cui_rispondere  = device;
        }
        else if( buf[0] == 0x06 )
        {
            // Abilita l'accoppiamento con una scheda del ripetitore ( passaggio della chiave in chiaro )
            flag.crypto_en               = 0;
            flag.ble_accoppia_ripetitore = 1;
            flag.ble_update_adv          = 1;
            device_a_cui_rispondere      = device;
        }
        else if( buf[0] == 0x07 )
        {
            // Abilita l'accoppiamento con il sensore BLE esterno       
            flag.crypto_en             = 0;
            flag.ble_accoppia_tastiera = 1;
            flag.ble_update_adv        = 1;
            device_a_cui_rispondere    = device;
        }
        else
        {
            gatt_err_code = CY_BLE_GATT_ERR_ATTRIBUTE_NOT_FOUND;
        }
        break;

    case 0x1c:
        // Invio del random number ( numero di verifica da aggiungere al codice di apertura )
        if (buf[0] != 1)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[1] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[2] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[3] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else
        {
            gapDevice[device].faseSend = send_random_number;
        }

        break;

    case 0x1d:
        //
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }
        if (buf[0] != 1 && buf[0] != 2)
        {
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
            break;
        }

        if (buf[0] == 1)
            abilitaUtente(0xff);
        else
            disabilitaUtente(0xff);

        flag.save_utenti = 1;

        break;

    case 0x1e:
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }
        if (buf[0] == 1)
        {
            flag.reset_scheda = 1;
        }
        else
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;

        break;

    case 0x1f:
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }
        if (buf[0] == 1)
        {
            par.nuova           = 1;
            flag.save_par       = 1;
            flag.ble_update_adv = 1;
        }
        else if (buf[0] == 2)
        {
            par.nuova           = 0;
            flag.save_par       = 1;
            flag.ble_update_adv = 1;
        }
        else
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;

        break;

    case 0x20:
        // Richiede la chiave crittografica
        //if( flag.crypto_en == 0 || par.nuova == 1 )
        if (flag.crypto_en == 0 || (flag.recovery == 1 && par.nuova == 1))
            gapDevice[device].faseSend = send_crypto_key;
        else
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;

        break;

    case 0x21:
        // Aggiungo un affittuario creato sulla piattaforma WEB
        crypto_des_decript(bufrx, mess, 8);

        memset(&utente_tmp, 0, sizeof(sUtente));

        utente_tmp.tipo     = utente_affittuario;
        utente_tmp.permessi = permesso_tastierino;
        
        utente_tmp.affitto.anno   = mess[0];
        utente_tmp.affitto.mese   = mess[1];
        utente_tmp.affitto.giorno = mess[2];
        utente_tmp.affitto.ore    = mess[3];
        utente_tmp.affitto.orePermanenza  = (uint16_t)mess[4] << 8;
        utente_tmp.affitto.orePermanenza |= (uint16_t)mess[5];

        ui16  = (uint16_t)mess[6] << 8;
        ui16 += (uint16_t)mess[7];

        // faccio un controllo
        if (mess[0] < 19 || mess[0] > 99)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (mess[1] < 1 || mess[1] > 12)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (mess[2] < 1 || mess[2] > 31)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (mess[3] < 1 || mess[3] > 24)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (utente_tmp.affitto.orePermanenza == 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (ui16 < 1000)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (ui16 > 9999)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;

        if (gatt_err_code == CY_BLE_GATT_ERR_INVALID_HANDLE)
            break;

        sprintf(utente_tmp.tastierino, "%u", ui16);
        sprintf(utente_tmp.nome, "Affitto");

        if (addUtente(utente_tmp) != err_ok) 
        {
            gapDevice[device].faseSend   = send_error_code;
            gapDevice[device].errCommand = 0x21;
            gapDevice[device].errCode    = err_codice_esistente;
            
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
        }
        else
        {
            ut = utenti_registrati - 1;

            fram_wakeup();
            if (writeUtente(ut, &utenti[ut]) == false)
                gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            
            if (fram_writeArray(ADDRESS_NUM_UTENTI, (uint8_t *)&utenti_registrati, 2) == false)
                gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            fram_sleep();
            
            if( gapDevice[device].is_bridge )
            {
                id_risposta_cloud              = gapDevice[device].id_risposta;
                utente_cloud_tipo              = 1; // nuovo
                utente_cloud                   = ut;
                gapDevice[device].faseSend     = send_user_to_cloud;                
                gapDevice[device].faseSendNext = send_null;                
            }            
            else if( bridge.accoppiato )
            {
                id_risposta_cloud      = gapDevice[device].id_risposta;
                utente_cloud_tipo      = 1; // nuovo
                utente_cloud           = ut;
                flag.send_user_tocloud = 1;
                flag.ble_update_adv    = 1;
            }
        }
        break;

    case 0x22:
        // Ricevo il codice master in chiaro e disabilito la cryptografia per l'invio della chiave crittografica
        if (flag.recovery == 0)
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
        else
        {
            if (memcmp(bufrx, crypto.mastercode, 10) == 0)
            {
                flag.crypto_en = 0;
            }
            else
                gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        }
        break;

    case 0x23:
        // Invio il nome della serratura in chiaro ( Momentaneo solo per test )
        
        gapDevice[device].faseSend = send_test;
        
        break;

    case 0x24:
        // Il bridge è pronto a ricevere il suo evento
        if( bufrx[0] == 1 ) 
        {
            // Verifico se questo è il mio bridge
            if( bridge.bdaddress[0] != gapDevice[device].bdAddress[0] ) break;           
            if( bridge.bdaddress[1] != gapDevice[device].bdAddress[1] ) break;           
            if( bridge.bdaddress[2] != gapDevice[device].bdAddress[2] ) break;            
            if( bridge.bdaddress[3] != gapDevice[device].bdAddress[3] ) break;    
            if( bridge.bdaddress[4] != gapDevice[device].bdAddress[4] ) break;    
            if( bridge.bdaddress[5] != gapDevice[device].bdAddress[5] ) break;           
            
            if( flag.send_event_tocloud )    
            {
                gapDevice[device].faseSend     = send_evento;
                gapDevice[device].faseSendNext = send_null;
            }   
            if( flag.send_user_tocloud )
            {
                gapDevice[device].id_risposta  = id_risposta_cloud;
                gapDevice[device].faseSend     = send_user_to_cloud;
                gapDevice[device].faseSendNext = send_null;
            }   
            if( flag.send_users_tocloud )
            {
                gapDevice[device].id_risposta  = id_risposta_cloud;
                gapDevice[device].faseSend     = send_users;
                gapDevice[device].faseSendNext = send_null;
            }   
            if( flag.send_par_tocloud )
            {
                gapDevice[device].faseSend     = send_par1;
                gapDevice[device].faseSendNext = send_null;
            }
            if( flag.send_wifi_tobridge )
            {
                gapDevice[device].faseSend     = send_wifi_par;
                gapDevice[device].faseSendNext = send_null;
            }
            if( flag.send_all_tocloud )
            {
                gapDevice[device].faseSend     = send_par1;
                gapDevice[device].faseSendNext = send_users;
            }
            if( flag.send_error_to_cloud )
            {
                gapDevice[device].faseSend     = send_error_code;
                gapDevice[device].faseSendNext = send_null;
                gapDevice[device].errCode      = errcode_risposta_cloud;
                gapDevice[device].id_risposta  = id_risposta_cloud;
            }
                
            break;
        }
        if( bufrx[0] == 2 )
        {
            // ricevo l'ID del bridge da passare al telefono !    
            memset( bridge.id , 0 , 20 );
            memcpy( bridge.id , &bufrx[2] , bufrx[1] );

            bridge.bdaddress[0] = gapDevice[device].bdAddress[0];            
            bridge.bdaddress[1] = gapDevice[device].bdAddress[1];            
            bridge.bdaddress[2] = gapDevice[device].bdAddress[2];            
            bridge.bdaddress[3] = gapDevice[device].bdAddress[3];            
            bridge.bdaddress[4] = gapDevice[device].bdAddress[4];            
            bridge.bdaddress[5] = gapDevice[device].bdAddress[5];    
            
            bridge.release_fw = bufrx[60];
            bridge.release_hw = bufrx[61];
            
            bridge.accoppiato = 1;
            

            fram_wakeup();
            if (fram_writeArray( ADDRESS_BRIDGEID, (uint8_t *)&bridge, sizeof(bridge_s)) == false) 
            {

            }
            fram_sleep();
            
            flag.ble_accoppia_bridge = 0;
        }
        
    break;
    case 0x27:
        // Invio il mio BT Address e il mio nome
        gapDevice[device].faseSend = send_bdaddress_in_chiaro;
        break;
        
    case 0x28:
        // Ricezione sensore porta chiusa ble
        if( bufrx[0] != 0x25 ) break;
        if( bufrx[1] != 0x01 ) break;
        
        if( bufrx[2] == 0 ) statoPortaBle.bit.porta_chiusa = 1;
        else statoPortaBle.bit.porta_chiusa = 0;
        
        statoPortaBle.bit.nuovo_dato = 1;
        
    break;
        
    case 0x29:
        // MIA BACKDOOR
        if( par.nuova == 0 ) break;
        
        if (bufrx[0] == 'A' &&
            bufrx[1] == 't' &&
            bufrx[2] == 'a' &&
            bufrx[3] == 'l' &&
            bufrx[4] == 'a' &&
            bufrx[5] == 'n' &&
            bufrx[6] == 't' &&
            bufrx[7] == 'a')
        {
            // Invio codice master e chiave crittografica !!
            // QUI COMANDO IO !
            // Ma tranquilli, funziona solo se non è mai stata configurata :)

            gapDevice[device].faseSend = send_backdoor;
        }
        else 
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;

        break;

    case 0x30:
        // Set livello batteria 3.7 volt
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }
        if (buf[0] != 1)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[1] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[2] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else if (buf[3] != 0)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_PDU;
        else
            flag.calibra_batteria = 1;

        break;

    case 0x31:
        // gapDevice[device].faseSend = send_test_criptato;
        break;

    case 0x32:
        // Set del Serial Number ( OBSOLETO )
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }

//        par.serial_number[0] = buf[0];
//        par.serial_number[1] = buf[1];
//        par.serial_number[2] = buf[2];
//        par.serial_number[3] = buf[3];
//        par.serial_number[4] = buf[4];
//        par.serial_number[5] = buf[5];
//
//        Cy_BLE_DISS_SetCharacteristicValue(2, 6, par.serial_number);

//        flag.ble_update_adv = 1;
//        flag.save_par = 1;

        break;

    case 0x33:
        // Accendo il led o l'uscita verde per 5 secondi
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }
        if (buf[0] == 1)
            timerLampeggio = 5000;
        if (buf[0] == 2)
            timerOutProgrammabile = 5000;

        break;

    case 0x34:
    {
        // Azzeramento dei log ufg
        cy_stc_rtc_config_t orologio;
        Cy_RTC_GetDateAndTime(&orologio);

        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }
        if (buf[0] == 1)
        {
            logUfg.totale_aperture = 0;
            logUfg.totale_chiusure = 0;
            logUfg.secondi_ble = 0;

            logUfg.giorno_inizio = orologio.date;
            logUfg.mese_inizio = orologio.month;
            logUfg.anno_inizio = orologio.year;
        }
    }
    break;

    case 0x35:
        // Non usare !
        break;

    case 0x36:
        // Set delle chiusure programmate
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }

        // Verifico che le ore siano giuste
        if (buf[1] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[3] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[6] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[8] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[11] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[13] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[16] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[18] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[21] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[23] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[26] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[28] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[31] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[33] > 23)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;

        // Verifico che i minuti siano giusti
        if (buf[2] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[4] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[7] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[9] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[12] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[14] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[17] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[19] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[22] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[24] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[27] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[29] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[32] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
        if (buf[34] > 59)
            gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;

        if (gatt_err_code == CY_BLE_GATT_ERR_NONE)
        {
            chiusureProgrammate[0].enable       = buf[0];
            chiusureProgrammate[0].ore          = buf[1];
            chiusureProgrammate[0].minuti       = buf[2];
            chiusureProgrammate[0].ore_stop     = buf[3];
            chiusureProgrammate[0].minuti_stop  = buf[4];
            
            chiusureProgrammate[1].enable       = buf[5];
            chiusureProgrammate[1].ore          = buf[6];
            chiusureProgrammate[1].minuti       = buf[7];
            chiusureProgrammate[1].ore_stop     = buf[8];
            chiusureProgrammate[1].minuti_stop  = buf[9];
            
            chiusureProgrammate[2].enable       = buf[10];
            chiusureProgrammate[2].ore          = buf[11];
            chiusureProgrammate[2].minuti       = buf[12];
            chiusureProgrammate[2].ore_stop     = buf[13];
            chiusureProgrammate[2].minuti_stop  = buf[14];
            
            chiusureProgrammate[3].enable       = buf[15];
            chiusureProgrammate[3].ore          = buf[16];
            chiusureProgrammate[3].minuti       = buf[17];
            chiusureProgrammate[3].ore_stop     = buf[18];
            chiusureProgrammate[3].minuti_stop  = buf[19];
            
            chiusureProgrammate[4].enable       = buf[20];
            chiusureProgrammate[4].ore          = buf[21];
            chiusureProgrammate[4].minuti       = buf[22];
            chiusureProgrammate[4].ore_stop     = buf[23];
            chiusureProgrammate[4].minuti_stop  = buf[24];
            
            chiusureProgrammate[5].enable       = buf[25];
            chiusureProgrammate[5].ore          = buf[26];
            chiusureProgrammate[5].minuti       = buf[27];
            chiusureProgrammate[5].ore_stop     = buf[28];
            chiusureProgrammate[5].minuti_stop  = buf[29];
            
            chiusureProgrammate[6].enable       = buf[30];
            chiusureProgrammate[6].ore          = buf[31];
            chiusureProgrammate[6].minuti       = buf[32];
            chiusureProgrammate[6].ore_stop     = buf[33];
            chiusureProgrammate[6].minuti_stop  = buf[34];

            flag.save_orari_chiusura = 1;
            //impostoPrimoRisveglioPerChiusura();
        }

        break;

    case 0x37:
        // Invio il mio BT Address e il mio nome
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }
        gapDevice[device].faseSend = send_bdaddress;
    break;
        
    case 0x38:
        // NON USARE !
        break;
        
    case 0x39:
        //if ( nessun_accesso_master() )
        //{
        //    gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
        //    break;
        //}
        gapDevice[device].faseSend = send_bridgeid;        
        break;
        
    case 0x40:
    {
        uint64_t u64;
        // Codice numerico arrivato dal tastierino BLE

        // buf[31] = lunghezza codice
        // buf[30] = revision hardware

        if (buf[0] == 'R')
        {
            // Chiusura porta
            flag.chiudi_porta = 1;
        }
        else if( buf[31] == 3 )
        {
            // Finchè non memorizzo un utente, posso aprire la porta con 123 ( modalità cantiere )
            if( utenti_registrati == 0 )
            {
                if( buf[0] == '1' && buf[1] == '2' && buf[2] == '3' )
                {
                    flag.apri_porta = 1;    
                }
            }
        }
        else if (buf[31] > 3 && buf[31] < 7)
        {
            // Codice apertura porta

            memset(tastieraCode, 0, 30);
            memcpy(tastieraCode, buf, 6);

            // di chi è il codice ?
            if (verificaCodiceApertura((char *)tastieraCode, &ut) == true)
            {
                // può aprire ?
                if (verificaPermessoApertura(ut, permesso_tastierino) == err_ok)
                {
                    tentativi_falliti = 0;
                    ++utenti[ut].aperture;
                    salvoLogUtente(ut, apertura_da_tastierino, evento_eseguito);

                    ++logUfg.totale_aperture;
                    flag.send_event_tocloud = 1;
                    flag.save_logufg = 1;
                    flag.apri_porta = 1;
                }
                else
                {
                    salvoLogUtente(ut, apertura_da_tastierino, evento_non_eseguito);
                    gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
                    flag.send_event_tocloud = 1;
                    ++tentativi_falliti;
                }
                fram_wakeup();
                if (writeUtente(ut, &utenti[ut]) == false)
                    gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
                fram_sleep();
            }
            else
            {
                gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
                ++tentativi_falliti;
            }
        }
        else if (buf[31] < 30)
        {
            // Aggiungo un affittuario creato sulla piattaforma WEB
            u64 = strtoull( (const char*)buf,0,10);
            
            bufrx[0] = (u64 >> 56) & 0xff;
            bufrx[1] = (u64 >> 48) & 0xff;
            bufrx[2] = (u64 >> 40) & 0xff;
            bufrx[3] = (u64 >> 32) & 0xff;
            bufrx[4] = (u64 >> 24) & 0xff;
            bufrx[5] = (u64 >> 16) & 0xff;
            bufrx[6] = (u64 >>  8) & 0xff;
            bufrx[7] = (u64      ) & 0xff;
            
            crypto_des_decript(bufrx, mess, 8);

            memset(&utente_tmp, 0, sizeof(sUtente));

            utente_tmp.tipo     = utente_affittuario;
            utente_tmp.permessi = permesso_tastierino;

            utente_tmp.affitto.anno   = mess[0];
            utente_tmp.affitto.mese   = mess[1];
            utente_tmp.affitto.giorno = mess[2];
            utente_tmp.affitto.ore    = mess[3];
            utente_tmp.affitto.orePermanenza  = (uint16_t)mess[4] << 8;
            utente_tmp.affitto.orePermanenza |= (uint16_t)mess[5];

            ui16 = (uint16_t)mess[6] << 8;
            ui16 += (uint16_t)mess[7];

            // faccio un controllo
            if (mess[0] < 19 || mess[0] > 99)
                gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
            if (mess[1] < 1 || mess[1] > 12)
                gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
            if (mess[2] < 1 || mess[2] > 31)
                gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
            if (mess[3] < 1 || mess[3] > 24)
                gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
            if (utente_tmp.affitto.orePermanenza == 0)
                gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
            if (ui16 < 1000)
                gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
            if (ui16 > 9999)
                gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;

            if (gatt_err_code == CY_BLE_GATT_ERR_INVALID_HANDLE)
                break;

            sprintf(utente_tmp.tastierino, "%u", ui16);
            sprintf(utente_tmp.nome, "Affitto");

            if (addUtente(utente_tmp) != err_ok)
                gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            else
            {
                flag.save_utenti = 1;
            }            
            //err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            //timeout_ble = 500;
        }
        break;
    }
        
    case 0x41:
        
        //        keyCode.code[26] = batteryLevel;
        //        keyCode.code[27] = revision;
        //        keyCode.code[28] = keyCode.card;
        //        keyCode.code[29] = keyCode.type;
        //        keyCode.code[30] = keyCode.sak;
        //        keyCode.code[31] = keyCode.len;

        // Codice arrivato dalla tastiera RFID
        if( flag.associa_rfid )
        {       
            if( buf[31] < 4 || buf[31] > 9 ) gatt_err_code = CY_BLE_GATT_ERR_INVALID_HANDLE;
            else
            {
                memcpy( utenti[utente_rfid].rfid, buf, buf[31] );            
                utenti[utente_rfid].permessi |= permesso_rfid;

                fram_wakeup();
                if (writeUtente(utente_rfid, &utenti[utente_rfid]) == false)
                    gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
                fram_sleep();
                
            }
            flag.associa_rfid = 0;
            break;
        }        
        
        if( utenti_registrati == 0 )
        {
            // Se non ho registrato nessun utente, tutti i tag aprono ! ( Modalita cantiere )
            flag.apri_porta = 1;
            break;
        }
        
        memset(tastieraCode,   0,  30);
        memcpy(tastieraCode, buf, buf[31] );
        
        // di chi è il codice ?
        if (verificaCodiceRfidApertura((char *)tastieraCode, &ut) == true)
        {
            // può aprire ?
            if (verificaPermessoApertura(ut, permesso_tastierino) == err_ok)
            {
                ++utenti[ut].aperture;
                salvoLogUtente(ut, apertura_da_nfc, evento_eseguito);

                ++logUfg.totale_aperture;
                flag.send_event_tocloud = 1;
                flag.save_logufg = 1;
                flag.apri_porta = 1;
            }
            else
            {
                salvoLogUtente(ut, apertura_da_nfc, evento_non_eseguito);
                gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
                flag.send_event_tocloud = 1;
                ++tentativi_falliti;
            }
            fram_wakeup();
            if (writeUtente(ut, &utenti[ut]) == false)
                gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            fram_sleep();
        }
        else
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            //timeout_ble = 500;
        }
        //flag.apri_porta = 1;        
        
        break;

    case 0x42:
    {
        uint16_t sum = 0;
        uint16_t ver = 0;
        // Codice arrivato dal FingerPrint !!
        
        // Id fingerprint valido ?
        sum  = (uint16_t)buf[0] + (uint16_t)buf[1] + (uint16_t)buf[2] + (uint16_t)buf[3] + (uint16_t)buf[4] + (uint16_t)buf[5] + (uint16_t)buf[6] + (uint16_t)buf[7];
        ver  = (uint16_t)buf[8] << 8;
        ver += (uint16_t)buf[9];
        
        if( sum != ver )
        {
            gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            break;
        }
        
        // Avevo gia accoppiato una scheda fingerprint ?
        if( par.id_fingerprint[0] == 0 )
        {
            // se no prendo per buona questa che mi è appena arrivata    
            par.id_fingerprint[0] = buf[0];
            par.id_fingerprint[1] = buf[1];
            par.id_fingerprint[2] = buf[2];
            par.id_fingerprint[3] = buf[3];
            par.id_fingerprint[4] = buf[4];
            par.id_fingerprint[5] = buf[5];
            par.id_fingerprint[6] = buf[6];
            par.id_fingerprint[7] = buf[7];
            flag.save_par = 1;
        }
        else
        {
            // Verifico che arrivi da quella che ho sempre usato 
            if( par.id_fingerprint[0] != buf[0] ) gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            if( par.id_fingerprint[1] != buf[1] ) gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            if( par.id_fingerprint[2] != buf[2] ) gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            if( par.id_fingerprint[3] != buf[3] ) gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            if( par.id_fingerprint[4] != buf[4] ) gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            if( par.id_fingerprint[5] != buf[5] ) gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            if( par.id_fingerprint[6] != buf[6] ) gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            if( par.id_fingerprint[7] != buf[7] ) gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
        }
        
        if( gatt_err_code == CY_BLE_GATTS_ERR_OPERATION_FAILED ) break;
        
        // Vedo se la scheda fingerprint è quella con cui ho sempre parlato io        
        //
        
        if( flag.associa_fingerprint )
        {
            flag.associa_fingerprint = false;
            
            if( buf[10] == 200 )
            {
                // Cancellazzione delle impronte andate a buon fine
                break;
            }
            
            if( buf[10] == 0 ) // errore nell'associazione rfid
            {
                gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
            }
            else
            {
                utenti[utente_fingerprint].impronta  = buf[10];
                utenti[utente_fingerprint].permessi |= permesso_fingerprint;
                
                fram_wakeup();
                if (writeUtente(utente_fingerprint, &utenti[utente_fingerprint]) == false) 
                    gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
                fram_sleep();
            }
        }
        else if( buf[10] > 9 && buf[10] < 101 )
        {
            // Cerco l'utente associato a quell'impronta 
            if( findFingerprint( buf[10], &ut ) == true )
            {
                // puo' aprire adesso ?
                if (verificaPermessoApertura(ut, permesso_fingerprint ) == err_ok )
                {
                    ++utenti[ut].aperture;
                    salvoLogUtente(ut, apertura_da_fingerprint, evento_eseguito);

                    ++logUfg.totale_aperture;
                    flag.send_event_tocloud = 1;
                    flag.save_logufg = 1;
                    flag.apri_porta  = 1;
                }
                else
                {
                    salvoLogUtente(ut, apertura_da_fingerprint, evento_non_eseguito);
                    gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
                    flag.send_event_tocloud = 1;
                    ++tentativi_falliti;
                }
                
                fram_wakeup();
                if (writeUtente(ut, &utenti[ut]) == false)
                    gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
                fram_sleep();
            }
            else gatt_err_code = CY_BLE_GATTS_ERR_OPERATION_FAILED;
        }
        else
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
        }
    break;
    }
        
    case 0x43:
    {
        // Apertura da telefono con il codice al posto delle credenziali
        uint8_t len = buf[0];
        uint8_t code[6];

        errCode = err_credenziali;  
        
        // controllo se alcuni affittuari sono scaduti
        if( eliminaAffittiScaduti() == true )
        {
            fram_wakeup();
            writeUtenti();
            fram_sleep();
        }
        
        if( len > 3 && len < 7 )
        {        
            memset( code,0,6);            
            code[0] = buf[1];
            code[1] = buf[2];
            code[2] = buf[3];
            code[3] = buf[4];
            if( len > 4 ) code[4] = buf[5];
            if( len > 5 ) code[5] = buf[6];
            
            if (verificaCodiceApertura((char *)code, &ut) == true)
            {
                // è un affittuario ?
                if( utenti[ut].tipo == utente_affittuario )
                {
                    // può aprire adesso ?
                    if( verificaPermessoApertura( ut, permesso_tastierino ) == err_ok )
                    {
                        // ok puo' aprire
                        flag.apri_porta = 1;
                        errCode = err_ok;
                    }
                }
            }    
        }
        
        if( errCode == err_credenziali ) gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
        
        gapDevice[device].faseSend   = send_error_code;
        gapDevice[device].errCode    = errCode;
        gapDevice[device].errCommand = 0x11;
        
        break;  
    }
    case 0x50:
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }

        if (buf[0] == 0x10)
            flag.upgrade_fw = 1;

        break;

    case 0x60:
        // Dal cloud arriva la richiesta di backup utenti
        // Questa richiesta potrebbe arrivare anche dal telefono per fare un backup sul cloud
        
        // Se questa richiesta arriva dal Bridge posso rispondere subito !
        if( gapDevice[device].is_bridge )
        {
            if( buf[0] == 1 )
            {
                gapDevice[device].id_risposta  = id_risposta_cloud;
                gapDevice[device].faseSend     = send_users;
                gapDevice[device].faseSendNext = send_null;

            }
            else if( buf[0] == 2 )
            {
                gapDevice[device].id_risposta  = id_risposta_cloud;
                gapDevice[device].faseSend     = send_par1;
                gapDevice[device].faseSendNext = send_null;
            }
            else if( buf[0] == 3 )
            {
                gapDevice[device].id_risposta  = id_risposta_cloud;
                gapDevice[device].faseSend     = send_par1;
                gapDevice[device].faseSendNext = send_users;
                flag.send_cloud = 1;    // serve solo per non ripetere fasesendNext
            }
            else
            {
                errCode = err_operazione_non_permessa;
                
                gapDevice[device].faseSend   = send_error_code;
                gapDevice[device].errCode    = errCode;
                gapDevice[device].errCommand = 0x60;
            }                
        }
        else
        {
            // Se è arrivata dal telefono devo avvisare il bridge attraverso l'advertising
            if( buf[0] == 1 )
            {
                flag.send_users_tocloud = 1;    
                flag.ble_update_adv = 1;
                
                gapDevice[device].faseSend   = send_error_code;
                gapDevice[device].errCode    = errCode;
                gapDevice[device].errCommand = 0x60;

            }
            else if( buf[0] == 2 )
            {
                flag.send_par_tocloud = 1;   
                flag.ble_update_adv = 1;
                
                gapDevice[device].faseSend   = send_error_code;
                gapDevice[device].errCode    = errCode;
                gapDevice[device].errCommand = 0x60;
            }
            else if( buf[0] == 3 )
            {
                flag.send_all_tocloud = 1;   
                flag.ble_update_adv = 1;
                
                gapDevice[device].faseSend   = send_error_code;
                gapDevice[device].errCode    = errCode;
                gapDevice[device].errCommand = 0x60;
            }
            else
            {
                errCode = err_operazione_non_permessa;
                
                gapDevice[device].faseSend   = send_error_code;
                gapDevice[device].errCode    = errCode;
                gapDevice[device].errCommand = 0x60;
            }                
        }
        
        
        break;
        
    case 0x61:
    {
        // arrivate le credenziali della wireless da passare al bridge
        if ( nessun_accesso_master() )
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }
        if( bridge.accoppiato == 0 ) 
        {
            gatt_err_code = CY_BLE_GATT_ERR_WRITE_NOT_PERMITTED;
            break;
        }
        wifi.len_ssid = buf[0];
        wifi.len_pass = buf[1];
        wifi.channel  = buf[2];
        
        memcpy( wifi.ssid, &buf[3], wifi.len_ssid );
        memcpy( wifi.pass, &buf[3 + wifi.len_ssid], wifi.len_pass );
        
        flag.send_wifi_tobridge = 1;
        flag.ble_update_adv = 1;
        
        gapDevice[device].faseSend   = send_error_code;
        gapDevice[device].errCode    = errCode;
        gapDevice[device].errCommand = 0x61;
        
    }
        break;
        
    default:
        gatt_err_code = CY_BLE_GATT_ERR_ATTRIBUTE_NOT_FOUND;
        break;
    }
    
    if( gapDevice[device].is_bridge == 1 )
    {
        if( gatt_err_code != CY_BLE_GATT_ERR_NONE )
        {
            // Devo avvisare il cloud che qualcosa è andato storto !!
            
            gapDevice[device].faseSend     = send_error_code;
            gapDevice[device].faseSendNext = send_null;
            gapDevice[device].errCommand   = command;
            gapDevice[device].errCode      = errCode;
            gapDevice[device].id_risposta  = id_risposta_cloud;

            
//            
//            flag.send_error_to_cloud = 1;
//            flag.ble_update_adv      = 1;
//
//            id_risposta_cloud        = gapDevice[device].id_risposta;
//            errcode_risposta_cloud   = gatt_err_code;
//            
//            gapDevice[device].errCode      = errcode_risposta_cloud;
//            gapDevice[device].id_risposta  = id_risposta_cloud;
//           

//            gapDevice[device].faseSend     = send_error_code;
//            gapDevice[device].faseSendNext = send_null;
//            
//            gapDevice[device].errCommand = command;
//            gapDevice[device].errCode    = gatt_err_code;
            
        }        
        return CY_BLE_GATT_ERR_NONE;    // al bridge posso dire che tutto è andato bene
    }
    
    return gatt_err_code;
}

void setMagnetometroPortaChiusa()
{
    int sum_x = 0;
    int sum_y = 0;
    int sum_z = 0;
    int cnt;

    leggo_magnetometro();

    Lis3mdl_StartContinuous();

    for (cnt = 0; cnt < 100; cnt++)
    {
        while (Cy_GPIO_Read(ready_magn_PORT, ready_magn_NUM) == 0)
        {
            CyDelay(1);
        }
        // Leggo magnetometro
        Lis3mdl_ReadValues();

        sum_x += lis3mdl.value_x;
        sum_y += lis3mdl.value_y;
        sum_z += lis3mdl.value_z;
    }

    par.magnete_porta_chiusa.x = sum_x / 100;
    par.magnete_porta_chiusa.y = sum_y / 100;
    par.magnete_porta_chiusa.z = sum_z / 100;

    flag.calibra_porta_chiusa = 0;
    flag.save_par = 1;

    Lis3mdl_Idle();
}
//
//void setMagnetometroSerraturaChiusa()
//{
//   int sum_x = 0;
//   int sum_y = 0;
//   int sum_z = 0;
//   int cnt;
//
//   leggo_magnetometro();
//
//   Lis3mdl_StartContinuous();
//
//   for( cnt = 0; cnt < 100; cnt++)
//   {
//      while(  Cy_GPIO_Read( ready_magn_PORT, ready_magn_NUM ) == 0 )
//      {
//         CyDelay(1);
//      }
//      // Leggo magnetometro
//      Lis3mdl_ReadValues();
//
//      sum_x += lis3mdl.value_x;
//      sum_y += lis3mdl.value_y;
//      sum_z += lis3mdl.value_z;
//   }
//
//   par.magnete_serratura_chiusa.x = sum_x / 100;
//   par.magnete_serratura_chiusa.y = sum_y / 100;
//   par.magnete_serratura_chiusa.z = sum_z / 100;
//
//   flag.calibra_serratura_chiusa = 0;
//   flag.save_par                 = 1;
//
//   Lis3mdl_Idle();
//}

bool leggo_magnetometro()
{
    int timeout = 10000;

    Lis3mdl_StartSingle();

    //   while( (lis3mdl.status_reg & 0b10000000 ) == 0 )
    //   {
    //      CyDelay(100);
    //      Lis3mdl_ReadStatusReg();
    //   }

    while (Cy_GPIO_Read(ready_magn_PORT, ready_magn_NUM) == 0)
    {
        if (timeout)
            --timeout;
        else
            return false;
        CyDelay(1);
    }

    Lis3mdl_ReadValues();
    //   Lis3mdl_Idle();
    return true;
}

void outProgrammabile(eOutQuandoAttivo quando)
{
    if (par.output.anable == 1 && par.output.quando_attivo == quando)
    {
        if (par.output.ms_attivo == 0)
        {
            Cy_GPIO_Write(out_programmabile_PORT, out_programmabile_NUM, par.output.stato_attivo);
        }
        else
        {
            Cy_GPIO_Write(out_programmabile_PORT, out_programmabile_NUM, par.output.stato_attivo);
            CyDelay(par.output.ms_attivo);
            Cy_GPIO_Write(out_programmabile_PORT, out_programmabile_NUM, !par.output.stato_attivo);
        }
    }
    else
    {
        Cy_GPIO_Write(out_programmabile_PORT, out_programmabile_NUM, !par.output.stato_attivo);
    }
}

void salvaLogPorta(uint8_t evento, uint8_t esecuzione)
{
    cy_stc_rtc_config_t orologio;
    Cy_RTC_GetDateAndTime(&orologio);

    logSerratura.log[logSerratura.plog].datetime.giorno = orologio.date;
    logSerratura.log[logSerratura.plog].datetime.mese = orologio.month;
    logSerratura.log[logSerratura.plog].datetime.anno = orologio.year;
    logSerratura.log[logSerratura.plog].datetime.ore = orologio.hour;
    logSerratura.log[logSerratura.plog].datetime.minuti = orologio.min;

    logSerratura.log[logSerratura.plog].richiesta = evento;
    logSerratura.log[logSerratura.plog].richiesta_eseguita = esecuzione;

    ultimoLog.utente[0] = 's';
    ultimoLog.utente[1] = 'e';
    ultimoLog.utente[2] = 'r';
    ultimoLog.utente[3] = 'r';
    ultimoLog.utente[4] = 'a';
    ultimoLog.utente[5] = 't';
    ultimoLog.utente[6] = 'u';
    ultimoLog.utente[7] = 'r';
    ultimoLog.utente[8] = 'a';
    ultimoLog.utente[9] = 0;
    
    ultimoLog.log.datetime.giorno    = orologio.date;
    ultimoLog.log.datetime.mese      = orologio.month;
    ultimoLog.log.datetime.anno      = orologio.year;
    ultimoLog.log.datetime.ore       = orologio.hour;
    ultimoLog.log.datetime.minuti    = orologio.min;
    ultimoLog.log.richiesta          = evento;
    ultimoLog.log.richiesta_eseguita = esecuzione;
    
    flag.send_event_tocloud = 1;
    
    ++logSerratura.plog;

    if (logSerratura.tlog < 30)
        ++logSerratura.tlog;
    if (logSerratura.plog == 30)
        logSerratura.plog = 0;

    if (esecuzione == evento_eseguito)
    {
        if (evento == apertura_da_serratura)
            ++logUfg.totale_aperture;
        else if (evento == apertura_da_citofono)
            ++logUfg.totale_aperture;
        else if (evento == chiusura_da_serratura)
            ++logUfg.totale_chiusure;
        else if (evento == chiusura_automatica_da_serratura)
            ++logUfg.totale_chiusure;

        flag.save_logufg = 1;
    }
    flag.save_log = 1;
            
    /*
    if (esecuzione == evento_eseguito)
    {
        if( par.tipo_alimentazione == alimentazione_batteria ) Seriale_Transmit( "#100\r" , 5 );
        else                                                   Seriale_Transmit( "#111\r" , 5 );
    }
    else
    {
        if( par.tipo_alimentazione == alimentazione_batteria ) Seriale_Transmit( "#200\r" , 5 );
        else                                                   Seriale_Transmit( "#201\r" , 5 );
    }
    */
}

extern const cy_stc_sysint_t SysInt_tasto_cfg;
void isrTasto()
{
    flag.tasto_aprichiudi = 1;
    Cy_GPIO_ClearInterrupt(tasto_PORT, tasto_NUM);
    //NVIC_ClearPendingIRQ( SysInt_tasto_cfg.intrSrc );
    
#if LOG_UFG == 1    
    Seriale_PutString( "T PREMUTO\r\n");
#endif

}
void isrCitofono()
{
    if( Cy_GPIO_Read( citofono_PORT , citofono_NUM ) == 0 )
    {
        // Mi ha svegliato il citofono    
        if( par.en_citofono == 0 ) return;
        if( flag.apri_porta == 1 ) return;
        
        if ( gapDevice[0].faseSend == send_sensors_cont ) return;
        if ( gapDevice[1].faseSend == send_sensors_cont ) return;
        if ( gapDevice[2].faseSend == send_sensors_cont ) return;
        if ( gapDevice[3].faseSend == send_sensors_cont ) return;
        
        flag.apri_da_citofono = 1;
    }    
    Cy_GPIO_ClearInterrupt(citofono_PORT, citofono_NUM);
}

extern const cy_stc_sysint_t SysInt_port9_cfg;
void isrCapsense()
{
    if( Cy_GPIO_Read( pin_wakeup_ext_PORT, pin_wakeup_ext_0_NUM ) == 0 )
    {
        // Mi ha svegliato la tastiera esterna
        fl_invia_tastiera_esterna = 1;
        flag.wakeup = 1;
    }
    Cy_GPIO_ClearInterrupt(pin_wakeup_ext_PORT, pin_wakeup_ext_0_NUM);
    
    //NVIC_ClearPendingIRQ(SysInt_port9_cfg.intrSrc);
}
void isr_RTC()
{
    //NVIC_ClearPendingIRQ( RTC_RTC_IRQ_cfg.intrSrc );
    //NVIC_EnableIRQ(RTC_1_RTC_IRQ_cfg.intrSrc);

    Cy_RTC_Interrupt(&RTC_dstConfig, RTC_rtcDstStatus);
}

void impostaRisveglioTraUnMinuto()
{
    uint32_t timeout = 5;
    cy_en_rtc_status_t retStatus;
    
    uint32_t next_min, next_hour;

    cy_stc_rtc_alarm_t myRtcAlarmConfig = {
        .sec = 0,
        .secEn = CY_RTC_ALARM_DISABLE,
        .min = 0,
        .minEn = CY_RTC_ALARM_DISABLE,
        .hour = 0,
        .hourEn = CY_RTC_ALARM_DISABLE,
        .dayOfWeek = 1,
        .dayOfWeekEn = CY_RTC_ALARM_DISABLE,
        .date = 1,
        .dateEn = CY_RTC_ALARM_DISABLE,
        .month = 1,
        .monthEn = CY_RTC_ALARM_DISABLE,
        .almEn = CY_RTC_ALARM_DISABLE};

    // Che giorno è oggi ?
    cy_stc_rtc_config_t orologio;
    Cy_RTC_GetDateAndTime(&orologio);
    
    next_min  = orologio.min;
    next_hour = orologio.hour;
    
    if( next_min == 59 )
    {
        next_min  = 0;
        next_hour = next_hour < 23 ? next_hour + 1 : 0; 
    }
    else ++next_min;
        
    myRtcAlarmConfig.sec         = 0;
    myRtcAlarmConfig.secEn       = CY_RTC_ALARM_DISABLE;
    myRtcAlarmConfig.min         = next_min;
    myRtcAlarmConfig.minEn       = CY_RTC_ALARM_ENABLE;
    myRtcAlarmConfig.hour        = next_hour;
    myRtcAlarmConfig.hourEn      = CY_RTC_ALARM_ENABLE;
    myRtcAlarmConfig.dayOfWeek   = 1;
    myRtcAlarmConfig.dayOfWeekEn = CY_RTC_ALARM_DISABLE;
    myRtcAlarmConfig.date        = 1;
    myRtcAlarmConfig.dateEn      = CY_RTC_ALARM_DISABLE;
    myRtcAlarmConfig.month       = 1;
    myRtcAlarmConfig.monthEn     = CY_RTC_ALARM_DISABLE;
    myRtcAlarmConfig.almEn       = CY_RTC_ALARM_ENABLE;

    do
    {
        retStatus = Cy_RTC_SetAlarmDateAndTime(&myRtcAlarmConfig, CY_RTC_ALARM_1);
        timeout--;
        Cy_SysLib_Delay(5);
    } while ((retStatus != CY_RTC_SUCCESS) && (timeout != 0u));

    if (retStatus == CY_RTC_SUCCESS)
    {
        /* Set interrupt for next custom alarm */
        Cy_RTC_SetInterruptMask(CY_RTC_INTR_ALARM1);
    }
}

void impostoProssimoRisveglioPerChiusura()
{
    uint32_t timeout = 5;
    cy_en_rtc_status_t retStatus;

    cy_stc_rtc_alarm_t myRtcAlarmConfig = {
        .sec = 0,
        .secEn = CY_RTC_ALARM_DISABLE,
        .min = 0,
        .minEn = CY_RTC_ALARM_DISABLE,
        .hour = 0,
        .hourEn = CY_RTC_ALARM_DISABLE,
        .dayOfWeek = 1,
        .dayOfWeekEn = CY_RTC_ALARM_DISABLE,
        .date = 1,
        .dateEn = CY_RTC_ALARM_DISABLE,
        .month = 1,
        .monthEn = CY_RTC_ALARM_DISABLE,
        .almEn = CY_RTC_ALARM_DISABLE};

    // Che giorno è oggi ?
    cy_stc_rtc_config_t orologio;
    Cy_RTC_GetDateAndTime(&orologio);

    // mi sono svegliato perchè è mezzanotte ?
    if (orologio.hour == 0 && orologio.min == 1)
    {
        // vedo se oggi devo chiudere la porta automaticamente
        if (chiusureProgrammate[orologio.dayOfWeek - 1].enable)
        {
            myRtcAlarmConfig.sec = 0;
            myRtcAlarmConfig.secEn = CY_RTC_ALARM_DISABLE;
            myRtcAlarmConfig.min = chiusureProgrammate[orologio.dayOfWeek - 1].minuti;
            myRtcAlarmConfig.minEn = CY_RTC_ALARM_ENABLE;
            myRtcAlarmConfig.hour = chiusureProgrammate[orologio.dayOfWeek - 1].ore;
            myRtcAlarmConfig.hourEn = CY_RTC_ALARM_ENABLE;
            myRtcAlarmConfig.dayOfWeek = 1;
            myRtcAlarmConfig.dayOfWeekEn = CY_RTC_ALARM_DISABLE;
            myRtcAlarmConfig.date = 1;
            myRtcAlarmConfig.dateEn = CY_RTC_ALARM_DISABLE;
            myRtcAlarmConfig.month = 1;
            myRtcAlarmConfig.monthEn = CY_RTC_ALARM_DISABLE;
            myRtcAlarmConfig.almEn = CY_RTC_ALARM_ENABLE;
        }
        else
        {
            // altrimenti mi riprogrammo per svegliarmi a mezzanotte
            myRtcAlarmConfig.sec = 0;
            myRtcAlarmConfig.secEn = CY_RTC_ALARM_ENABLE;
            myRtcAlarmConfig.min = 1;
            myRtcAlarmConfig.minEn = CY_RTC_ALARM_ENABLE;
            myRtcAlarmConfig.hour = 0;
            myRtcAlarmConfig.hourEn = CY_RTC_ALARM_ENABLE;
            myRtcAlarmConfig.dayOfWeek = 1;
            myRtcAlarmConfig.dayOfWeekEn = CY_RTC_ALARM_DISABLE;
            myRtcAlarmConfig.date = 1;
            myRtcAlarmConfig.dateEn = CY_RTC_ALARM_DISABLE;
            myRtcAlarmConfig.month = 1;
            myRtcAlarmConfig.monthEn = CY_RTC_ALARM_DISABLE;
            myRtcAlarmConfig.almEn = CY_RTC_ALARM_ENABLE;
        }
    }
    else
    {
        // mi sono svegliato per chiudere la porta
        flag.chiudi_porta = 1;

        // mi riprogrammo per svegliarmi a mezzanotte
        myRtcAlarmConfig.sec = 0;
        myRtcAlarmConfig.secEn = CY_RTC_ALARM_ENABLE;
        myRtcAlarmConfig.min = 1;
        myRtcAlarmConfig.minEn = CY_RTC_ALARM_ENABLE;
        myRtcAlarmConfig.hour = 0;
        myRtcAlarmConfig.hourEn = CY_RTC_ALARM_ENABLE;
        myRtcAlarmConfig.dayOfWeek = 1;
        myRtcAlarmConfig.dayOfWeekEn = CY_RTC_ALARM_DISABLE;
        myRtcAlarmConfig.date = 1;
        myRtcAlarmConfig.dateEn = CY_RTC_ALARM_DISABLE;
        myRtcAlarmConfig.month = 1;
        myRtcAlarmConfig.monthEn = CY_RTC_ALARM_DISABLE;
        myRtcAlarmConfig.almEn = CY_RTC_ALARM_ENABLE;
    }

    do
    {
        retStatus = Cy_RTC_SetAlarmDateAndTime(&myRtcAlarmConfig, CY_RTC_ALARM_1);
        timeout--;
        Cy_SysLib_Delay(5);
    } while ((retStatus != CY_RTC_SUCCESS) && (timeout != 0u));

    if (retStatus == CY_RTC_SUCCESS)
    {
        /* Set interrupt for next custom alarm */
        Cy_RTC_SetInterruptMask(CY_RTC_INTR_ALARM1);
    }
}

void impostoPrimoRisveglioPerChiusura()
{
    uint32_t timeout = 5;
    cy_en_rtc_status_t retStatus;

    cy_stc_rtc_alarm_t myRtcAlarmConfig = {
        .sec = 0,
        .secEn = CY_RTC_ALARM_DISABLE,
        .min = 0,
        .minEn = CY_RTC_ALARM_DISABLE,
        .hour = 0,
        .hourEn = CY_RTC_ALARM_DISABLE,
        .dayOfWeek = 1,
        .dayOfWeekEn = CY_RTC_ALARM_DISABLE,
        .date = 1,
        .dateEn = CY_RTC_ALARM_DISABLE,
        .month = 1,
        .monthEn = CY_RTC_ALARM_DISABLE,
        .almEn = CY_RTC_ALARM_DISABLE};

    // Che giorno è oggi ?
    cy_stc_rtc_config_t orologio;
    Cy_RTC_GetDateAndTime(&orologio);

    // Oggi devo chiudere automaticamente ?
    if (chiusureProgrammate[orologio.dayOfWeek - 1].enable == 1)
    {
        if (chiusureProgrammate[orologio.dayOfWeek - 1].ore > orologio.hour)
        {
            // Mi programmo per chiudere automaticamente
            myRtcAlarmConfig.sec = 0;
            myRtcAlarmConfig.secEn = CY_RTC_ALARM_DISABLE;
            myRtcAlarmConfig.min = chiusureProgrammate[orologio.dayOfWeek - 1].minuti;
            myRtcAlarmConfig.minEn = CY_RTC_ALARM_ENABLE;
            myRtcAlarmConfig.hour = chiusureProgrammate[orologio.dayOfWeek - 1].ore;
            myRtcAlarmConfig.hourEn = CY_RTC_ALARM_ENABLE;
            myRtcAlarmConfig.dayOfWeek = 1;
            myRtcAlarmConfig.dayOfWeekEn = CY_RTC_ALARM_DISABLE;
            myRtcAlarmConfig.date = 1;
            myRtcAlarmConfig.dateEn = CY_RTC_ALARM_DISABLE;
            myRtcAlarmConfig.month = 1;
            myRtcAlarmConfig.monthEn = CY_RTC_ALARM_DISABLE;
            myRtcAlarmConfig.almEn = CY_RTC_ALARM_ENABLE;
        }
        if (chiusureProgrammate[orologio.dayOfWeek - 1].ore == orologio.hour)
        {
            if (chiusureProgrammate[orologio.dayOfWeek - 1].minuti > orologio.min)
            {
                // Mi programmo per chiudere automaticamente
                myRtcAlarmConfig.sec = 0;
                myRtcAlarmConfig.secEn = CY_RTC_ALARM_DISABLE;
                myRtcAlarmConfig.min = chiusureProgrammate[orologio.dayOfWeek - 1].minuti;
                myRtcAlarmConfig.minEn = CY_RTC_ALARM_ENABLE;
                myRtcAlarmConfig.hour = chiusureProgrammate[orologio.dayOfWeek - 1].ore;
                myRtcAlarmConfig.hourEn = CY_RTC_ALARM_ENABLE;
                myRtcAlarmConfig.dayOfWeek = 1;
                myRtcAlarmConfig.dayOfWeekEn = CY_RTC_ALARM_DISABLE;
                myRtcAlarmConfig.date = 1;
                myRtcAlarmConfig.dateEn = CY_RTC_ALARM_DISABLE;
                myRtcAlarmConfig.month = 1;
                myRtcAlarmConfig.monthEn = CY_RTC_ALARM_DISABLE;
                myRtcAlarmConfig.almEn = CY_RTC_ALARM_ENABLE;
            }
        }
    }

    // Altrimenti mi programmo per svegliarmi domani mattina e ricontrollare
    if (myRtcAlarmConfig.almEn == CY_RTC_ALARM_DISABLE)
    {
        // mi riprogrammo per svegliarmi amezzanotte
        myRtcAlarmConfig.sec = 0;
        myRtcAlarmConfig.secEn = CY_RTC_ALARM_ENABLE;
        myRtcAlarmConfig.min = 1;
        myRtcAlarmConfig.minEn = CY_RTC_ALARM_ENABLE;
        myRtcAlarmConfig.hour = 0;
        myRtcAlarmConfig.hourEn = CY_RTC_ALARM_ENABLE;
        myRtcAlarmConfig.dayOfWeek = 1;
        myRtcAlarmConfig.dayOfWeekEn = CY_RTC_ALARM_DISABLE;
        myRtcAlarmConfig.date = 1;
        myRtcAlarmConfig.dateEn = CY_RTC_ALARM_DISABLE;
        myRtcAlarmConfig.month = 1;
        myRtcAlarmConfig.monthEn = CY_RTC_ALARM_DISABLE;
        myRtcAlarmConfig.almEn = CY_RTC_ALARM_ENABLE;
    }
    do
    {
        retStatus = Cy_RTC_SetAlarmDateAndTime(&myRtcAlarmConfig, CY_RTC_ALARM_1);
        timeout--;
        Cy_SysLib_Delay(5);
    } while ((retStatus != CY_RTC_SUCCESS) && (timeout != 0u));

    if (retStatus == CY_RTC_SUCCESS)
    {
        /* Set interrupt for next custom alarm */
        Cy_RTC_SetInterruptMask(CY_RTC_INTR_ALARM1);
    }
}

void Cy_RTC_Alarm1Interrupt(void)
{
    #ifdef TEST_APERTURE
    impostaRisveglioTraUnMinuto();
    flag.tasto_aprichiudi = 1;
    #else
    impostoProssimoRisveglioPerChiusura();
    #endif
}

// --------------------------------------------------------------------------------------

bool salvaParametri()
{
    bool ok;
    fram_wakeup();
    par.crc = CRC8((unsigned char *)&par, sizeof(sParametri) - 1);
    ok = fram_writeArray(ADDRESS_PAR, (uint8_t *)&par, sizeof(sParametri));
    fram_sleep();
    return ok;
}

bool salvaCrypto()
{
    bool ok;
    crypto.opt = 0xaa;
    crypto.crc = CRC8( (unsigned char*)&crypto, sizeof(sCryptoKey) -1 );
    fram_wakeup();
    fram_writeArray(ADDRESS_KEY, (uint8_t *)&crypto, sizeof(sCryptoKey));
    fram_writeArray(ADDRESS_KEY_COPY, (uint8_t *)&crypto, sizeof(sCryptoKey));
    fram_sleep();
    return ok;
}

bool salvaCards()
{
    bool ok;
    fram_wakeup(); 
    ok = fram_writeArray(ADDRESS_CARDS, (uint8_t *)&cards, sizeof(sCards) ); 
    fram_sleep(); 
    return ok;
}
//
//void invia_a_tastiera_esterna( int b1, int b2, int b3 )
//{
//    /*
//    * Byte 1 
//        * 0 : Spengo i led
//        * 1 : Segnalo con il led verde l'avvenuta **apertura** della porta
//        * 2 : Segnalo con il led rosso e un  beep di un secondo la **non apertura** della porta
//        * 3 : Lampeggio del led verde
//        * 5 : Segnalo con il led verde e un  beep di un secondo l'avvenuta **apertura** della porta
//        * 6 : Segnalo con il led verde e un  doppio beep ( usato per la registrazione del fingerprint )
//        * 7 : Lampeggio del led rosso
//    * Byte 2
//        * 1 : la tastiera resta sveglia
//        * 0 : La tastiera si addormenta
//    * Byte 3
//        * 1 : buzzer abilitato
//        * 0 : buzzer disabilitato
//    * Byte 4
//        * durata lampeggio
//    * Byte 5
//        * numero lampeggi
//    */
//    
//    char buf[6];
//    
//    buf[0] = '#';
//    buf[1] = b1 + 48;
//    buf[2] = b2 + 48;
//    buf[3] = b3 + 48;
//    buf[4] = 0x0d;
//    buf[5] = 0;
//    
//    Seriale_Transmit( buf , 5 );
//    
//    while( Seriale_IsTxComplete() )
//    {
//        ;
//    }
//    fl_invia_tastiera_esterna = 0;
//}

void set_tastiera_esterna( int b1, int b2, int b3 , int b4, int b5)
{
   /*
    * Byte 1 
        * 0 : Spengo i led
        * 1 : Segnalo con il led verde l'avvenuta **apertura** della porta                                  ( obsoleto )
        * 2 : Segnalo con il led rosso e un  beep di un secondo la **non apertura** della porta             ( obsoleto )
        * 3 : Lampeggio del led verde                                                                       
        * 5 : Segnalo con il led verde e un  beep di un secondo l'avvenuta **apertura** della porta         ( obsoleto )
        * 6 : Segnalo con il led verde e un  doppio beep ( usato per la registrazione del fingerprint )     ( obsoleto )
        * 7 : Lampeggio del led rosso
        * V : set del led verde con durata e lampeggi
        * R : set del led rosso con durata e lampeggi
    * Byte 2
        * 1 : la tastiera resta sveglia
        * 0 : La tastiera si addormenta
    * Byte 3
        * 1 : buzzer abilitato
        * 0 : buzzer disabilitato
    * Byte 4
        * durata lampeggio
    * Byte 5
        * numero lampeggi
    */
    
    char buf[7];
    
    buf[0] = '#';
    buf[1] = b1;
    buf[2] = b2 + 48;
    buf[3] = b3 + 48;
    buf[4] = b4 + 48;
    buf[5] = b5 + 48;
    buf[6] = 0x0d;
    
    CyDelay(500);
   
    Seriale_Transmit( buf , 7 );
    
    while( Seriale_IsTxComplete() )
    {
        ;
    }
    fl_invia_tastiera_esterna = 0;
}

/* [] END OF FILE */
