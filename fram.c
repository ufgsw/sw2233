/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "fram.h"
#include "app.h"

void    fram_wren();
uint8_t fram_getStatusReg();  
bool    fram_writeEnd();

uint8_t  txBuffer[10];
uint8_t  rxBuffer[10];
uint16_t timeout;

cy_en_scb_spi_status_t spi_status;
uFramStatusReg         framSR;

uint32_t masterStatus;        

uint8_t buffertest[50];

bool fram_init()
{
   fram_wakeup();
   
   return fram_readId();
}

bool fram_readId()
{
   timeout = 500;
   cs_on();
   txBuffer[0] = cmd_rdid;
   txBuffer[1] = 0;
   txBuffer[2] = 0;
   txBuffer[3] = 0;
   txBuffer[4] = 0;
   rxBuffer[0] = 0;
   rxBuffer[1] = 0;
   rxBuffer[2] = 0;
   rxBuffer[3] = 0;
   rxBuffer[4] = 0;
   
   spi_status = Cy_SCB_SPI_Transfer( spifr_HW, txBuffer , rxBuffer , 5, &spifr_context);
   
   /* If no error wait till master sends data in Tx FIFO */
    if(spi_status == CY_SCB_SPI_SUCCESS)
    {
        /* Wait until master complete read transfer or time out has occured */
        do
        {
            masterStatus  = Cy_SCB_SPI_GetTransferStatus(spifr_HW, &spifr_context);
            if( (masterStatus & CY_SCB_SPI_TRANSFER_ACTIVE) == 0 ) timeout = 0;
            else
            {
               CyDelay(1);
               --timeout;
            }
            
        } while(timeout);        
   }
   
   cs_off();   

   if( rxBuffer[1] == 0x04 ) // Fujitsu ?
   {
      if( rxBuffer[3] == 0x26 )  return true;   // Product ID ( 512Kbit )
      else                       return false;
   }
   else 
      return false;
}

bool fram_writeByte( uint16_t address, uint8_t  dato )
{ 
   fram_wren();
   
   timeout = 500;
   cs_on();
   txBuffer[0] = cmd_write;
   txBuffer[1] = address >> 8;
   txBuffer[2] = address & 0xff;
   txBuffer[3] = dato;

   spi_status = Cy_SCB_SPI_Transfer( spifr_HW, txBuffer , 0 , 4 , &spifr_context);
   
   /* If no error wait till master sends data in Tx FIFO */
    if(spi_status == CY_SCB_SPI_SUCCESS)
    {
        /* Timeout 1 sec */
        /* Wait until master complete read transfer or time out has occured */
        do
        {
            masterStatus  = Cy_SCB_SPI_GetTransferStatus(spifr_HW, &spifr_context);
            if( (masterStatus & CY_SCB_SPI_TRANSFER_ACTIVE) == 0 ) timeout = 0;
            else
            {
               CyDelay(1);
               timeout--;            
            }
        } while(timeout);        
   }
   
   cs_off();
   
   return true;
}
bool fram_readByte(  uint16_t address, uint8_t* dato)
{
   timeout = 500;
   cs_on();
   txBuffer[0] = cmd_read;
   txBuffer[1] = address >> 8;
   txBuffer[2] = address & 0xff;
   txBuffer[3] = 0;

   spi_status = Cy_SCB_SPI_Transfer( spifr_HW, txBuffer , rxBuffer , 4 , &spifr_context);
   
   /* If no error wait till master sends data in Tx FIFO */
    if(spi_status == CY_SCB_SPI_SUCCESS)
    {
        /* Timeout 1 sec */
        /* Wait until master complete read transfer or time out has occured */
        do
        {
            masterStatus  = Cy_SCB_SPI_GetTransferStatus(spifr_HW, &spifr_context);
            if( (masterStatus & CY_SCB_SPI_TRANSFER_ACTIVE) == 0 ) timeout = 0;
            else
            {
               CyDelay(1);
               timeout--;            
            }
        } while(timeout);        
   }
   
   cs_off();
   
   *dato = rxBuffer[3];
   
   return true;
}
bool fram_writeArray(uint16_t address, uint8_t* arr , int count )
{
   uint8_t  ret;
   uint8_t  ver;
   uint8_t* src = arr;
   
   if( count == 0 ) return false;
   
   fram_wren();
   
   timeout = 1000;
   cs_on();
   txBuffer[0] = cmd_write;
   txBuffer[1] = address >> 8;
   txBuffer[2] = address & 0xff;

   spi_status = Cy_SCB_SPI_Transfer( spifr_HW, txBuffer , 0 , 3 , &spifr_context);
   
   /* If no error wait till master sends data in Tx FIFO */
    if(spi_status == CY_SCB_SPI_SUCCESS)
    {
        /* Timeout 1 sec */
        /* Wait until master complete read transfer or time out has occured */
        do
        {
            masterStatus  = Cy_SCB_SPI_GetTransferStatus(spifr_HW, &spifr_context);
            if( (masterStatus & CY_SCB_SPI_TRANSFER_ACTIVE) == 0 ) timeout = 0;
            else
            {
               CyDelay(1);
               timeout--;            
            }
        } while(timeout);        
   }
   timeout = 10000;
   spi_status = Cy_SCB_SPI_Transfer( spifr_HW, arr , 0 , count , &spifr_context);
   /* If no error wait till master sends data in Tx FIFO */
    if(spi_status == CY_SCB_SPI_SUCCESS)
    {
        /* Timeout 1 sec */
        /* Wait until master complete read transfer or time out has occured */
        do
        {
            masterStatus  = Cy_SCB_SPI_GetTransferStatus(spifr_HW, &spifr_context);
            if( (masterStatus & CY_SCB_SPI_TRANSFER_ACTIVE) == 0 ) timeout = 0;
            else
            {
               CyDelay(1);
               timeout--;            
            }
        } while(timeout);        
   }
   
   cs_off();
   
   while( !fram_writeEnd() );
   
   ret = true;
   // VERIFICO QUELLO CHE HO SCRITTO !
   do
   {
      fram_readByte(address, &ver);
      
      wdog_ResetCounters( CY_MCWDT_CTR0 | CY_MCWDT_CTR1 , 200 );      
      
      if( ver != *src ) 
      {          
         error.bit.fram = 1;
         ret = false;
         break;
      }
      ++address;
      ++src;
      --count;
   }while(count != 0);
   
   return ret;
  
}
bool fram_readArray( uint16_t address, uint8_t* arr, int count  )
{
   if( count == 0 ) return false;

   timeout = 500;
   cs_on();
   txBuffer[0] = cmd_read;
   txBuffer[1] = address >> 8;
   txBuffer[2] = address & 0xff;

   spi_status = Cy_SCB_SPI_Transfer( spifr_HW, txBuffer , 0 , 3 , &spifr_context);
   
   /* If no error wait till master sends data in Tx FIFO */
    if(spi_status == CY_SCB_SPI_SUCCESS)
    {
        /* Timeout 1 sec */
        /* Wait until master complete read transfer or time out has occured */
        do
        {
            masterStatus  = Cy_SCB_SPI_GetTransferStatus(spifr_HW, &spifr_context);
            if( (masterStatus & CY_SCB_SPI_TRANSFER_ACTIVE) == 0 ) timeout = 0;
            else
            {
               CyDelay(1);
               timeout--;            
            }
        } while(timeout);        
   }
   timeout = 500;
   spi_status = Cy_SCB_SPI_Transfer( spifr_HW, 0 , arr , count , &spifr_context);
   /* If no error wait till master sends data in Tx FIFO */
    if(spi_status == CY_SCB_SPI_SUCCESS)
    {
        /* Timeout 1 sec */
        /* Wait until master complete read transfer or time out has occured */
        do
        {
            masterStatus  = Cy_SCB_SPI_GetTransferStatus(spifr_HW, &spifr_context);
            if( (masterStatus & CY_SCB_SPI_TRANSFER_ACTIVE) == 0 ) timeout = 0;
            else
            {
               CyDelay(1);
               timeout--;            
            }
        } while(timeout);        
   }
   
   cs_off();
   
   return true;
}

void fram_wren()
{
   timeout = 500;
   cs_on();
   txBuffer[0] = cmd_wren;

   spi_status = Cy_SCB_SPI_Transfer( spifr_HW, txBuffer , 0 , 1 , &spifr_context);
   
   /* If no error wait till master sends data in Tx FIFO */
    if(spi_status == CY_SCB_SPI_SUCCESS)
    {
        /* Timeout 1 sec */
        /* Wait until master complete read transfer or time out has occured */
        do
        {
            masterStatus  = Cy_SCB_SPI_GetTransferStatus(spifr_HW, &spifr_context);
            if( (masterStatus & CY_SCB_SPI_TRANSFER_ACTIVE) == 0 ) timeout = 0;
            else
            {
               CyDelay(1);
               timeout--;            
            }
        } while(timeout);        
   }
   
   cs_off();
   
   return;
}

uint8_t fram_getStatusReg()
{
   timeout = 500;
   cs_on();  
   txBuffer[0] = cmd_rdsr;
   txBuffer[1] = 0;

   spi_status = Cy_SCB_SPI_Transfer( spifr_HW, txBuffer , rxBuffer , 2 , &spifr_context);
   
   /* If no error wait till master sends data in Tx FIFO */
    if(spi_status == CY_SCB_SPI_SUCCESS)
    {
        /* Timeout 1 sec */
        /* Wait until master complete read transfer or time out has occured */
        do
        {
            masterStatus  = Cy_SCB_SPI_GetTransferStatus(spifr_HW, &spifr_context);
            if( (masterStatus & CY_SCB_SPI_TRANSFER_ACTIVE) == 0 ) timeout = 0;
            else
            {
               CyDelay(1);
               timeout--;            
            }
        } while(timeout);        
   }
   cs_off();
   
   return rxBuffer[1];
}

bool fram_writeEnd()
{
   framSR.val = fram_getStatusReg();
   if( framSR.bits.WIP ) return false;
   else                  return true;   
}

bool fram_sleep()
{
   timeout = 500;
   cs_on();
   txBuffer[0] = cmd_sleep;

   spi_status = Cy_SCB_SPI_Transfer( spifr_HW, txBuffer , 0 , 1 , &spifr_context);
   
   /* If no error wait till master sends data in Tx FIFO */
    if(spi_status == CY_SCB_SPI_SUCCESS)
    {
        /* Timeout 1 sec */
        /* Wait until master complete read transfer or time out has occured */
        do
        {
            masterStatus  = Cy_SCB_SPI_GetTransferStatus(spifr_HW, &spifr_context);
            if( (masterStatus & CY_SCB_SPI_TRANSFER_ACTIVE) == 0 ) timeout = 0;
            else
            {
               CyDelay(1);
               timeout--;            
            }
        } while(timeout);        
   }
   
   cs_off();
   
   return true;
}
bool fram_wakeup()
{
   cs_on();
   CyDelay(2);
   cs_off();
   
   return true;
}


/* [] END OF FILE */
