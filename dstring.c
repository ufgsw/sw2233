/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "dstring.h"
#include <string.h>

char* subString( char* source, char* dest , char separatore, int max );

parse_s campi;

eErrorCode parseString(char* st)
{
    char* par;
    char* pch = st;

    eErrorCode err = err_ok;
    //int fine = 0;

    memset(&campi, 0, sizeof(parse_s));
    campi.len = 0;
    par = &campi.var[0][0];

    while(1)
    {
        pch = subString(pch, par, SEPARATORE, MAX_LEN_CAMPO);

        if( pch == NULL ) break;//fine = 1;
        //if( *pch == '\r' || *pch == '\n' ) break;

        else if(campi.len < MAX_CAMPI)
        {
                ++campi.len;
                par = &campi.var[campi.len][0];
        }
        else
        {
                //fine = 1;
                err  = err_nodata;
        }
        if( *pch == '\r' || *pch == '\n' || *pch == 0 ) break;
        else
            ++pch;
    }
    return err;
}

char* subString(char* source, char* dest, char separatore, int max)
{
    char ch = *source;

    if(max == 0) return 0;
    if(ch == 0) return 0;

    while(ch != separatore)
    {
        *dest = ch;
        ++dest;
        ++source;
        ch = *source;
        if(--max == 0) return 0;
        if(ch == 0) ch = separatore;
    }

    *dest = 0;
    return source;
}



/* [] END OF FILE */
