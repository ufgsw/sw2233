/*******************************************************************************
* File Name: wdog.c
* Version 1.10
*
* Description:
*  This file provides the source code to the API for the wdog
*  component.
*
********************************************************************************
* Copyright 2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "wdog_PDL.h"


/** wdog_initVar indicates whether the wdog  component
*  has been initialized. The variable is initialized to 0 and set to 1 the first
*  time wdog_Start() is called.
*  This allows the component to restart without reinitialization after the first 
*  call to the wdog_Start() routine.
*
*  If re-initialization of the component is required, then the 
*  wdog_Init() function can be called before the 
*  wdog_Start() or wdog_Enable() function.
*/
uint8 wdog_initVar = 0u;

/** The instance-specific configuration structure. This should be used in the 
*  associated wdog_Init() function.
*/ 
const cy_stc_mcwdt_config_t wdog_config =
{
    .c0Match     = wdog_C0_MATCH,
    .c1Match     = wdog_C1_MATCH,
    .c0Mode      = wdog_C0_MODE,
    .c1Mode      = wdog_C1_MODE,
    .c2ToggleBit = wdog_C2_PERIOD,
    .c2Mode      = wdog_C2_MODE,
    .c0ClearOnMatch = (bool)wdog_C0_CLEAR_ON_MATCH,
    .c1ClearOnMatch = (bool)wdog_C1_CLEAR_ON_MATCH,
    .c0c1Cascade = (bool)wdog_CASCADE_C0C1,
    .c1c2Cascade = (bool)wdog_CASCADE_C1C2
};


/*******************************************************************************
* Function Name: wdog_Start
****************************************************************************//**
*
*  Sets the initVar variable, calls the Init() function, unmasks the 
*  corresponding counter interrupts and then calls the Enable() function 
*  to enable the counters.
*
* \globalvars
*  \ref wdog_initVar
*
*  \note
*  When this API is called, the counter starts counting after two lf_clk cycles 
*  pass. It is the user's responsibility to check whether the selected counters
*  were enabled immediately after the function call. This can be done by the 
*  wdog_GetEnabledStatus() API.
*
*******************************************************************************/
void wdog_Start(void)
{
    if (0u == wdog_initVar)
    {
        (void)wdog_Init(&wdog_config);
        wdog_initVar = 1u; /* Component was initialized */
    }

	/* Set interrupt masks for the counters */
	wdog_SetInterruptMask(wdog_CTRS_INT_MASK);

	/* Enable the counters that are enabled in the customizer */
    wdog_Enable(wdog_ENABLED_CTRS_MASK, 0u);
}


/*******************************************************************************
* Function Name: wdog_Stop
****************************************************************************//**
*
*  Calls the Disable() function to disable all counters.
*
*******************************************************************************/
void wdog_Stop(void)
{
    wdog_Disable(CY_MCWDT_CTR_Msk, wdog_TWO_LF_CLK_CYCLES_DELAY);
    wdog_DeInit();
}


/* [] END OF FILE */
