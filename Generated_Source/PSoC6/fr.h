/***************************************************************************//**
* \file fr.h
* \version 2.0
*
*  This file provides constants and parameter values for the SPI component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(fr_CY_SCB_SPI_PDL_H)
#define fr_CY_SCB_SPI_PDL_H

#include "cyfitter.h"
#include "scb/cy_scb_spi.h"

#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*        Function Prototypes
***************************************/
/**
* \addtogroup group_general
* @{
*/
/* Component specific functions. */
void fr_Start(void);

/* Basic functions. */
__STATIC_INLINE cy_en_scb_spi_status_t fr_Init(cy_stc_scb_spi_config_t const *config);
__STATIC_INLINE void fr_DeInit(void);
__STATIC_INLINE void fr_Enable(void);
__STATIC_INLINE void fr_Disable(void);

/* Register callback. */
__STATIC_INLINE void fr_RegisterCallback(cy_cb_scb_spi_handle_events_t callback);

/* Bus state. */
__STATIC_INLINE bool fr_IsBusBusy(void);

/* Slave select control. */
__STATIC_INLINE void fr_SetActiveSlaveSelect(cy_en_scb_spi_slave_select_t slaveSelect);
__STATIC_INLINE void fr_SetActiveSlaveSelectPolarity(cy_en_scb_spi_slave_select_t slaveSelect, 
                                                                   cy_en_scb_spi_polarity_t polarity);

/* Low level: read. */
__STATIC_INLINE uint32_t fr_Read(void);
__STATIC_INLINE uint32_t fr_ReadArray(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t fr_GetRxFifoStatus(void);
__STATIC_INLINE void     fr_ClearRxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t fr_GetNumInRxFifo(void);
__STATIC_INLINE void     fr_ClearRxFifo(void);

/* Low level: write. */
__STATIC_INLINE uint32_t fr_Write(uint32_t data);
__STATIC_INLINE uint32_t fr_WriteArray(void *buffer, uint32_t size);
__STATIC_INLINE void     fr_WriteArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t fr_GetTxFifoStatus(void);
__STATIC_INLINE void     fr_ClearTxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t fr_GetNumInTxFifo(void);
__STATIC_INLINE bool     fr_IsTxComplete(void);
__STATIC_INLINE void     fr_ClearTxFifo(void);

/* Master/slave specific status. */
__STATIC_INLINE uint32_t fr_GetSlaveMasterStatus(void);
__STATIC_INLINE void     fr_ClearSlaveMasterStatus(uint32_t clearMask);

/* High level: transfer functions. */
__STATIC_INLINE cy_en_scb_spi_status_t fr_Transfer(void *txBuffer, void *rxBuffer, uint32_t size);
__STATIC_INLINE void     fr_AbortTransfer(void);
__STATIC_INLINE uint32_t fr_GetTransferStatus(void);
__STATIC_INLINE uint32_t fr_GetNumTransfered(void);

/* Interrupt handler */
__STATIC_INLINE void fr_Interrupt(void);
/** @} group_general */


/***************************************
*    Variables with External Linkage
***************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t fr_initVar;
extern cy_stc_scb_spi_config_t const fr_config;
extern cy_stc_scb_spi_context_t fr_context;
/** @} group_globals */


/***************************************
*         Preprocessor Macros
***************************************/
/**
* \addtogroup group_macros
* @{
*/
/** The pointer to the base address of the hardware */
#define fr_HW     ((CySCB_Type *) fr_SCB__HW)

/** The slave select line 0 constant which takes into account pin placement */
#define fr_SPI_SLAVE_SELECT0    ( (cy_en_scb_spi_slave_select_t) fr_SCB__SS0_POSITION)

/** The slave select line 1 constant which takes into account pin placement */
#define fr_SPI_SLAVE_SELECT1    ( (cy_en_scb_spi_slave_select_t) fr_SCB__SS1_POSITION)

/** The slave select line 2 constant which takes into account pin placement */
#define fr_SPI_SLAVE_SELECT2    ( (cy_en_scb_spi_slave_select_t) fr_SCB__SS2_POSITION)

/** The slave select line 3 constant which takes into account pin placement */
#define fr_SPI_SLAVE_SELECT3    ((cy_en_scb_spi_slave_select_t) fr_SCB__SS3_POSITION)
/** @} group_macros */


/***************************************
*    In-line Function Implementation
***************************************/

/*******************************************************************************
* Function Name: fr_Init
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_spi_status_t fr_Init(cy_stc_scb_spi_config_t const *config)
{
    return Cy_SCB_SPI_Init(fr_HW, config, &fr_context);
}


/*******************************************************************************
* Function Name: fr_DeInit
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_DeInit(void)
{
    Cy_SCB_SPI_DeInit(fr_HW);
}


/*******************************************************************************
* Function Name: fr_Enable
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_Enable(void)
{
    Cy_SCB_SPI_Enable(fr_HW);
}


/*******************************************************************************
* Function Name: fr_Disable
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_Disable(void)
{
    Cy_SCB_SPI_Disable(fr_HW, &fr_context);
}


/*******************************************************************************
* Function Name: fr_RegisterCallback
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_RegisterCallback() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_RegisterCallback(cy_cb_scb_spi_handle_events_t callback)
{
    Cy_SCB_SPI_RegisterCallback(fr_HW, callback, &fr_context);
}


/*******************************************************************************
* Function Name: fr_IsBusBusy
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_IsBusBusy() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool fr_IsBusBusy(void)
{
    return Cy_SCB_SPI_IsBusBusy(fr_HW);
}


/*******************************************************************************
* Function Name: fr_SetActiveSlaveSelect
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_SetActiveSlaveSelect() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_SetActiveSlaveSelect(cy_en_scb_spi_slave_select_t slaveSelect)
{
    Cy_SCB_SPI_SetActiveSlaveSelect(fr_HW, slaveSelect);
}


/*******************************************************************************
* Function Name: fr_SetActiveSlaveSelectPolarity
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_SetActiveSlaveSelectPolarity() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_SetActiveSlaveSelectPolarity(cy_en_scb_spi_slave_select_t slaveSelect, 
                                                                   cy_en_scb_spi_polarity_t polarity)
{
    Cy_SCB_SPI_SetActiveSlaveSelectPolarity(fr_HW, slaveSelect, polarity);
}


/*******************************************************************************
* Function Name: fr_Read
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Read() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t fr_Read(void)
{
    return Cy_SCB_SPI_Read(fr_HW);
}


/*******************************************************************************
* Function Name: fr_ReadArray
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ReadArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t fr_ReadArray(void *buffer, uint32_t size)
{
    return Cy_SCB_SPI_ReadArray(fr_HW, buffer, size);
}


/*******************************************************************************
* Function Name: fr_GetRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t fr_GetRxFifoStatus(void)
{
    return Cy_SCB_SPI_GetRxFifoStatus(fr_HW);
}


/*******************************************************************************
* Function Name: fr_ClearRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_ClearRxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_SPI_ClearRxFifoStatus(fr_HW, clearMask);
}


/*******************************************************************************
* Function Name: fr_GetNumInRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_GetNumInRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t fr_GetNumInRxFifo(void)
{
    return Cy_SCB_GetNumInRxFifo(fr_HW);
}


/*******************************************************************************
* Function Name: fr_ClearRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_ClearRxFifo(void)
{
    Cy_SCB_SPI_ClearRxFifo(fr_HW);
}


/*******************************************************************************
* Function Name: fr_Write
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Write() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t fr_Write(uint32_t data)
{
    return Cy_SCB_SPI_Write(fr_HW, data);
}


/*******************************************************************************
* Function Name: fr_WriteArray
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_WriteArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t fr_WriteArray(void *buffer, uint32_t size)
{
    return Cy_SCB_SPI_WriteArray(fr_HW, buffer, size);
}


/*******************************************************************************
* Function Name: fr_WriteArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_WriteArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_WriteArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_SPI_WriteArrayBlocking(fr_HW, buffer, size);
}


/*******************************************************************************
* Function Name: fr_GetTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t fr_GetTxFifoStatus(void)
{
    return Cy_SCB_SPI_GetTxFifoStatus(fr_HW);
}


/*******************************************************************************
* Function Name: fr_ClearTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_ClearTxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_SPI_ClearTxFifoStatus(fr_HW, clearMask);
}


/*******************************************************************************
* Function Name: fr_GetNumInTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetNumInTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t fr_GetNumInTxFifo(void)
{
    return Cy_SCB_SPI_GetNumInTxFifo(fr_HW);
}


/*******************************************************************************
* Function Name: fr_IsTxComplete
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_IsTxComplete() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool fr_IsTxComplete(void)
{
    return Cy_SCB_SPI_IsTxComplete(fr_HW);
}


/*******************************************************************************
* Function Name: fr_ClearTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_ClearTxFifo(void)
{
    Cy_SCB_SPI_ClearTxFifo(fr_HW);
}


/*******************************************************************************
* Function Name: fr_GetSlaveMasterStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetSlaveMasterStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t fr_GetSlaveMasterStatus(void)
{
    return Cy_SCB_SPI_GetSlaveMasterStatus(fr_HW);
}


/*******************************************************************************
* Function Name: fr_ClearSlaveMasterStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearSlaveMasterStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_ClearSlaveMasterStatus(uint32_t clearMask)
{
    Cy_SCB_SPI_ClearSlaveMasterStatus(fr_HW, clearMask);
}


/*******************************************************************************
* Function Name: fr_Transfer
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Transfer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_spi_status_t fr_Transfer(void *txBuffer, void *rxBuffer, uint32_t size)
{
    return Cy_SCB_SPI_Transfer(fr_HW, txBuffer, rxBuffer, size, &fr_context);
}

/*******************************************************************************
* Function Name: fr_AbortTransfer
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_AbortTransfer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_AbortTransfer(void)
{
    Cy_SCB_SPI_AbortTransfer(fr_HW, &fr_context);
}


/*******************************************************************************
* Function Name: fr_GetTransferStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetTransferStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t fr_GetTransferStatus(void)
{
    return Cy_SCB_SPI_GetTransferStatus(fr_HW, &fr_context);
}


/*******************************************************************************
* Function Name: fr_GetNumTransfered
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetNumTransfered() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t fr_GetNumTransfered(void)
{
    return Cy_SCB_SPI_GetNumTransfered(fr_HW, &fr_context);
}


/*******************************************************************************
* Function Name: fr_Interrupt
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Interrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void fr_Interrupt(void)
{
    Cy_SCB_SPI_Interrupt(fr_HW, &fr_context);
}


#if defined(__cplusplus)
}
#endif

#endif /* fr_CY_SCB_SPI_PDL_H */


/* [] END OF FILE */
