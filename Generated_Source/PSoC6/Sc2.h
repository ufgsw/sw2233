/***************************************************************************//**
* \file Sc2.h
* \version 2.0
*
*  This file provides constants and parameter values for the SPI component.
*
********************************************************************************
* \copyright
* Copyright 2016-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(Sc2_CY_SCB_SPI_PDL_H)
#define Sc2_CY_SCB_SPI_PDL_H

#include "cyfitter.h"
#include "scb/cy_scb_spi.h"

#if defined(__cplusplus)
extern "C" {
#endif

/***************************************
*        Function Prototypes
***************************************/
/**
* \addtogroup group_general
* @{
*/
/* Component specific functions. */
void Sc2_Start(void);

/* Basic functions. */
__STATIC_INLINE cy_en_scb_spi_status_t Sc2_Init(cy_stc_scb_spi_config_t const *config);
__STATIC_INLINE void Sc2_DeInit(void);
__STATIC_INLINE void Sc2_Enable(void);
__STATIC_INLINE void Sc2_Disable(void);

/* Register callback. */
__STATIC_INLINE void Sc2_RegisterCallback(cy_cb_scb_spi_handle_events_t callback);

/* Bus state. */
__STATIC_INLINE bool Sc2_IsBusBusy(void);

/* Slave select control. */
__STATIC_INLINE void Sc2_SetActiveSlaveSelect(cy_en_scb_spi_slave_select_t slaveSelect);
__STATIC_INLINE void Sc2_SetActiveSlaveSelectPolarity(cy_en_scb_spi_slave_select_t slaveSelect, 
                                                                   cy_en_scb_spi_polarity_t polarity);

/* Low level: read. */
__STATIC_INLINE uint32_t Sc2_Read(void);
__STATIC_INLINE uint32_t Sc2_ReadArray(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t Sc2_GetRxFifoStatus(void);
__STATIC_INLINE void     Sc2_ClearRxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t Sc2_GetNumInRxFifo(void);
__STATIC_INLINE void     Sc2_ClearRxFifo(void);

/* Low level: write. */
__STATIC_INLINE uint32_t Sc2_Write(uint32_t data);
__STATIC_INLINE uint32_t Sc2_WriteArray(void *buffer, uint32_t size);
__STATIC_INLINE void     Sc2_WriteArrayBlocking(void *buffer, uint32_t size);
__STATIC_INLINE uint32_t Sc2_GetTxFifoStatus(void);
__STATIC_INLINE void     Sc2_ClearTxFifoStatus(uint32_t clearMask);
__STATIC_INLINE uint32_t Sc2_GetNumInTxFifo(void);
__STATIC_INLINE bool     Sc2_IsTxComplete(void);
__STATIC_INLINE void     Sc2_ClearTxFifo(void);

/* Master/slave specific status. */
__STATIC_INLINE uint32_t Sc2_GetSlaveMasterStatus(void);
__STATIC_INLINE void     Sc2_ClearSlaveMasterStatus(uint32_t clearMask);

/* High level: transfer functions. */
__STATIC_INLINE cy_en_scb_spi_status_t Sc2_Transfer(void *txBuffer, void *rxBuffer, uint32_t size);
__STATIC_INLINE void     Sc2_AbortTransfer(void);
__STATIC_INLINE uint32_t Sc2_GetTransferStatus(void);
__STATIC_INLINE uint32_t Sc2_GetNumTransfered(void);

/* Interrupt handler */
__STATIC_INLINE void Sc2_Interrupt(void);
/** @} group_general */


/***************************************
*    Variables with External Linkage
***************************************/
/**
* \addtogroup group_globals
* @{
*/
extern uint8_t Sc2_initVar;
extern cy_stc_scb_spi_config_t const Sc2_config;
extern cy_stc_scb_spi_context_t Sc2_context;
/** @} group_globals */


/***************************************
*         Preprocessor Macros
***************************************/
/**
* \addtogroup group_macros
* @{
*/
/** The pointer to the base address of the hardware */
#define Sc2_HW     ((CySCB_Type *) Sc2_SCB__HW)

/** The slave select line 0 constant which takes into account pin placement */
#define Sc2_SPI_SLAVE_SELECT0    ( (cy_en_scb_spi_slave_select_t) Sc2_SCB__SS0_POSITION)

/** The slave select line 1 constant which takes into account pin placement */
#define Sc2_SPI_SLAVE_SELECT1    ( (cy_en_scb_spi_slave_select_t) Sc2_SCB__SS1_POSITION)

/** The slave select line 2 constant which takes into account pin placement */
#define Sc2_SPI_SLAVE_SELECT2    ( (cy_en_scb_spi_slave_select_t) Sc2_SCB__SS2_POSITION)

/** The slave select line 3 constant which takes into account pin placement */
#define Sc2_SPI_SLAVE_SELECT3    ((cy_en_scb_spi_slave_select_t) Sc2_SCB__SS3_POSITION)
/** @} group_macros */


/***************************************
*    In-line Function Implementation
***************************************/

/*******************************************************************************
* Function Name: Sc2_Init
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Init() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_spi_status_t Sc2_Init(cy_stc_scb_spi_config_t const *config)
{
    return Cy_SCB_SPI_Init(Sc2_HW, config, &Sc2_context);
}


/*******************************************************************************
* Function Name: Sc2_DeInit
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_DeInit() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_DeInit(void)
{
    Cy_SCB_SPI_DeInit(Sc2_HW);
}


/*******************************************************************************
* Function Name: Sc2_Enable
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Enable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_Enable(void)
{
    Cy_SCB_SPI_Enable(Sc2_HW);
}


/*******************************************************************************
* Function Name: Sc2_Disable
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Disable() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_Disable(void)
{
    Cy_SCB_SPI_Disable(Sc2_HW, &Sc2_context);
}


/*******************************************************************************
* Function Name: Sc2_RegisterCallback
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_RegisterCallback() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_RegisterCallback(cy_cb_scb_spi_handle_events_t callback)
{
    Cy_SCB_SPI_RegisterCallback(Sc2_HW, callback, &Sc2_context);
}


/*******************************************************************************
* Function Name: Sc2_IsBusBusy
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_IsBusBusy() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool Sc2_IsBusBusy(void)
{
    return Cy_SCB_SPI_IsBusBusy(Sc2_HW);
}


/*******************************************************************************
* Function Name: Sc2_SetActiveSlaveSelect
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_SetActiveSlaveSelect() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_SetActiveSlaveSelect(cy_en_scb_spi_slave_select_t slaveSelect)
{
    Cy_SCB_SPI_SetActiveSlaveSelect(Sc2_HW, slaveSelect);
}


/*******************************************************************************
* Function Name: Sc2_SetActiveSlaveSelectPolarity
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_SetActiveSlaveSelectPolarity() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_SetActiveSlaveSelectPolarity(cy_en_scb_spi_slave_select_t slaveSelect, 
                                                                   cy_en_scb_spi_polarity_t polarity)
{
    Cy_SCB_SPI_SetActiveSlaveSelectPolarity(Sc2_HW, slaveSelect, polarity);
}


/*******************************************************************************
* Function Name: Sc2_Read
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Read() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc2_Read(void)
{
    return Cy_SCB_SPI_Read(Sc2_HW);
}


/*******************************************************************************
* Function Name: Sc2_ReadArray
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ReadArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc2_ReadArray(void *buffer, uint32_t size)
{
    return Cy_SCB_SPI_ReadArray(Sc2_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Sc2_GetRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc2_GetRxFifoStatus(void)
{
    return Cy_SCB_SPI_GetRxFifoStatus(Sc2_HW);
}


/*******************************************************************************
* Function Name: Sc2_ClearRxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearRxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_ClearRxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_SPI_ClearRxFifoStatus(Sc2_HW, clearMask);
}


/*******************************************************************************
* Function Name: Sc2_GetNumInRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_GetNumInRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc2_GetNumInRxFifo(void)
{
    return Cy_SCB_GetNumInRxFifo(Sc2_HW);
}


/*******************************************************************************
* Function Name: Sc2_ClearRxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearRxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_ClearRxFifo(void)
{
    Cy_SCB_SPI_ClearRxFifo(Sc2_HW);
}


/*******************************************************************************
* Function Name: Sc2_Write
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Write() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc2_Write(uint32_t data)
{
    return Cy_SCB_SPI_Write(Sc2_HW, data);
}


/*******************************************************************************
* Function Name: Sc2_WriteArray
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_WriteArray() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc2_WriteArray(void *buffer, uint32_t size)
{
    return Cy_SCB_SPI_WriteArray(Sc2_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Sc2_WriteArrayBlocking
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_WriteArrayBlocking() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_WriteArrayBlocking(void *buffer, uint32_t size)
{
    Cy_SCB_SPI_WriteArrayBlocking(Sc2_HW, buffer, size);
}


/*******************************************************************************
* Function Name: Sc2_GetTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc2_GetTxFifoStatus(void)
{
    return Cy_SCB_SPI_GetTxFifoStatus(Sc2_HW);
}


/*******************************************************************************
* Function Name: Sc2_ClearTxFifoStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearTxFifoStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_ClearTxFifoStatus(uint32_t clearMask)
{
    Cy_SCB_SPI_ClearTxFifoStatus(Sc2_HW, clearMask);
}


/*******************************************************************************
* Function Name: Sc2_GetNumInTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetNumInTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc2_GetNumInTxFifo(void)
{
    return Cy_SCB_SPI_GetNumInTxFifo(Sc2_HW);
}


/*******************************************************************************
* Function Name: Sc2_IsTxComplete
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_IsTxComplete() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE bool Sc2_IsTxComplete(void)
{
    return Cy_SCB_SPI_IsTxComplete(Sc2_HW);
}


/*******************************************************************************
* Function Name: Sc2_ClearTxFifo
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearTxFifo() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_ClearTxFifo(void)
{
    Cy_SCB_SPI_ClearTxFifo(Sc2_HW);
}


/*******************************************************************************
* Function Name: Sc2_GetSlaveMasterStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetSlaveMasterStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc2_GetSlaveMasterStatus(void)
{
    return Cy_SCB_SPI_GetSlaveMasterStatus(Sc2_HW);
}


/*******************************************************************************
* Function Name: Sc2_ClearSlaveMasterStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_ClearSlaveMasterStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_ClearSlaveMasterStatus(uint32_t clearMask)
{
    Cy_SCB_SPI_ClearSlaveMasterStatus(Sc2_HW, clearMask);
}


/*******************************************************************************
* Function Name: Sc2_Transfer
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Transfer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE cy_en_scb_spi_status_t Sc2_Transfer(void *txBuffer, void *rxBuffer, uint32_t size)
{
    return Cy_SCB_SPI_Transfer(Sc2_HW, txBuffer, rxBuffer, size, &Sc2_context);
}

/*******************************************************************************
* Function Name: Sc2_AbortTransfer
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_AbortTransfer() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_AbortTransfer(void)
{
    Cy_SCB_SPI_AbortTransfer(Sc2_HW, &Sc2_context);
}


/*******************************************************************************
* Function Name: Sc2_GetTransferStatus
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetTransferStatus() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc2_GetTransferStatus(void)
{
    return Cy_SCB_SPI_GetTransferStatus(Sc2_HW, &Sc2_context);
}


/*******************************************************************************
* Function Name: Sc2_GetNumTransfered
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_GetNumTransfered() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE uint32_t Sc2_GetNumTransfered(void)
{
    return Cy_SCB_SPI_GetNumTransfered(Sc2_HW, &Sc2_context);
}


/*******************************************************************************
* Function Name: Sc2_Interrupt
****************************************************************************//**
*
* Invokes the Cy_SCB_SPI_Interrupt() PDL driver function.
*
*******************************************************************************/
__STATIC_INLINE void Sc2_Interrupt(void)
{
    Cy_SCB_SPI_Interrupt(Sc2_HW, &Sc2_context);
}


#if defined(__cplusplus)
}
#endif

#endif /* Sc2_CY_SCB_SPI_PDL_H */


/* [] END OF FILE */
