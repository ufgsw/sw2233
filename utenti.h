/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#include "project.h"
#include "error.h"

#define MAX_CARDS  200
#define MAX_UTENTI 150
#define MAX_LOG 30

typedef enum
{
    permesso_fingerprint  = 0x01,
    permesso_smartphone   = 0x02,
    permesso_rfid         = 0x04,
    permesso_tastierino   = 0x08,
    permesso_telecomando  = 0x10,    
    permesso_disabilitato = 0x20,
    permesso_cloud        = 0x40,
    
    permesso_aprisempre   = 0xff,
    permesso_nessuno      = 0x00
} ePermessi;

typedef enum
{
    apertura_da_smartphone  = 1,
    apertura_da_tastierino  = 2,
    apertura_da_nfc         = 3,
    apertura_da_fingerprint = 4,

    apertura_da_serratura = 5,
    chiusura_da_serratura = 6,
    
    apertura_da_citofono  = 7,

    chiusura_automatica_da_serratura = 8,
    
    chiusura_da_smartphone = 10,
    chiusura_da_tastierino = 11,    
    
    apertura_da_cloud = 12,
    chiusura_da_cloud = 13
} eEvento;

typedef enum
{
    evento_non_eseguito = 0,
    evento_eseguito = 1
} eEventoStato;

typedef struct
{
    unsigned char giorno;
    unsigned char mese;
    unsigned char anno;
    unsigned char ore;
    unsigned char minuti;
} sDateTime;

typedef struct
{
    // log aperture
    eEvento richiesta; // apertura da trasponder, da smartphone etc..
    eEventoStato richiesta_eseguita;
    sDateTime datetime;
} sLog;

typedef struct
{
    uint8_t utente[11];
    sLog    log;
}sLogAdv;
extern sLogAdv ultimoLog;

typedef enum
{
    utente_residente   = 1,
    utente_affittuario = 2,
    utente_aziendale   = 3
} eTipoUtente;

typedef struct
{
    unsigned char  anno;
    unsigned char  mese;
    unsigned char  giorno;
    unsigned char  ore;
    unsigned short orePermanenza;
} sAffittuario;

typedef struct
{
    // L'utente puo' avere
    eTipoUtente tipo;
    uint8_t permessi;

    char nome[11];
    char pass[10];  
    char tastierino[7]; // Codice tastirino ble
    char rfid[9];       // Codice Mifaire classic
    char impronta;      // numero impronta memorizzata sul lettore
    
    uint16_t aperture;
    uint64_t permessi_orari[7];

    uint8_t tlog;
    uint8_t plog;
    sLog    log[MAX_LOG];

    sAffittuario affitto;
} sUtente;

typedef enum
{
    card_tastierino = 1,
    card_rfid = 2
}eTipoCard;

typedef struct
{
    uint8_t code[10];
    uint8_t tipo;
    uint8_t en;    
}sSlave;

typedef struct
{
    uint8_t memorizzati;
    uint8_t en_mastercard1;
    uint8_t en_mastercard2;
    uint8_t mastercard1[12];
    uint8_t mastercard2[12];
    sSlave  slave[MAX_CARDS];
}sCards;

extern sUtente  utenti[MAX_UTENTI];
extern sCards   cards;
extern uint16_t utenti_registrati;
extern uint8_t  random_code[2];
extern uint8_t  verifi_code[2];

bool readUtente(int index, sUtente *utente);
bool writeUtente(int index, sUtente *utente);
bool removeUtente(int index);

eErrorCode addUtente(sUtente utente);

uint8_t findfreeFingerprint();

bool findFingerprint(char code, int* index);
bool findCard(char*code, int *index);
bool findCode(char *code, int *index);
bool findUtente(char *nome, int *index);
bool findAffittuario(char *nome, char* pin, int *index);
bool readUtenti();
bool writeUtenti();
bool clearUtenti();

bool verificaCredenziali(int ut, char *pass);
bool verificaCodiceApertura(char *code, int *index);
bool verificaCodiceRfidApertura(char *code, int *index);
bool salvoLogUtente(int index, uint8_t evento, uint8_t esecuzione);
bool eliminoLogUtente(int index);

bool eliminaAffittiScaduti();

eErrorCode verificaPermessoApertura(int ut, ePermessi permesso);
eErrorCode verificaPermessoChiusura(int ut, ePermessi permesso);

void disabilitaUtente(int index);
void abilitaUtente(int index);
