/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#include "utenti.h"
#include "fram.h"
#include <stdlib.h>

cy_stc_rtc_config_t datetime;

void dataDaMinuti2000(uint64_t minuti, cy_stc_rtc_config_t *dt);
long minutiDaAnno2000(int giorno, int mese, int anno, int ore, int minuti);
long oreDaAnno2000(int giorno, int mese, int anno, int ore);

sUtente utenti[MAX_UTENTI];
sCards  cards;

uint16 utenti_registrati;

uint8_t random_code[2];
uint8_t verifi_code[2];

sLogAdv ultimoLog;


bool readUtenti()
{
    int len;

    fram_readArray(ADDRESS_NUM_UTENTI, (uint8_t *)&utenti_registrati, 2);

    if (utenti_registrati > MAX_UTENTI)
        return false;
    if (utenti_registrati == 0)
        return false;

    len = utenti_registrati * sizeof(sUtente);
    return fram_readArray(ADDRESS_UTENTI, (uint8_t *)&utenti, len);
}

bool writeUtenti()
{
    int len;

    if (utenti_registrati > MAX_UTENTI)
        return false;
    if (utenti_registrati == 0)
        return false;

    fram_writeArray(ADDRESS_NUM_UTENTI, (uint8_t *)&utenti_registrati, 2);

    len = utenti_registrati * sizeof(sUtente);
    return fram_writeArray(ADDRESS_UTENTI, (uint8_t *)&utenti, len);
}

bool clearUtenti()
{
    utenti_registrati = 0;
    return fram_writeArray(ADDRESS_NUM_UTENTI, (uint8_t *)&utenti_registrati, 2);
    //return fram_writeArray( ADDRESS_UTENTI , (uint8_t*)&utenti, sizeof(utenti) );
}

//bool readLogs()
//{
//   fram_readArray( ADDRESS_LOG , (uint8_t*)&logs, sizeof( logs ) );
//   return true;
//}

bool readUtente(int index, sUtente *utente)
{
    uint16_t adress = ADDRESS_UTENTI;
    if (index > MAX_UTENTI)
        return false;

    fram_readArray(adress + (sizeof(sUtente) * index), (uint8_t *)utente, sizeof(sUtente));

    return true;
}

bool writeUtente(int index, sUtente *utente)
{
    uint16_t adress = ADDRESS_UTENTI;
    if (index > MAX_UTENTI)
        return false;

    return fram_writeArray(adress + (sizeof(sUtente) * index), (uint8_t *)utente, sizeof(sUtente));
}

bool removeUtente(int index)
{
    int p;
    if (index > utenti_registrati)
        return false;

    for (p = index; p < utenti_registrati; p++)
    {
        memcpy(&utenti[p], &utenti[p + 1], sizeof(sUtente));
    }
    memset(&utenti[utenti_registrati], 0, sizeof(sUtente));

    --utenti_registrati;

    return true;
}

eErrorCode addUtente(sUtente utente)
{
    int ut = 0;

    // memoria piena ?
    if (utenti_registrati == MAX_UTENTI)
    {
        return err_max_utenti;
    }
    if( utente.tipo != utente_affittuario )
    {
        // Utente gia esistente ?    
        if (findUtente(utente.nome, 0) == true)
        {
            return err_credenziali;
        }
        // Codice tastierino gia usato ?
        if (utente.permessi & permesso_tastierino)
        {
            if (findCode(utente.tastierino, &ut) == true)
            {
                return err_codice_esistente;
            }
        }
    }

    memcpy(&utenti[utenti_registrati], &utente, sizeof(sUtente));
    ++utenti_registrati;
    return err_ok;
}

bool findUtente(char *nome, int *index)
{
    bool ret = false;

    int a;
    int ok;
    int len = strlen(nome);

    if (len > 10)
        return false;

    for (a = 0; a < utenti_registrati; a++)
    {
        ok = memcmp(utenti[a].nome, nome, len);
        if (ok == 0)
        {
            if (index != 0)
                *index = a;

            ret = true;
            break;
        }
    }
    return ret;
}

bool findAffittuario(char *nome, char* pin, int *index)
{
    bool ret = false;

    int a;
    int ok;
    int len = strlen(nome);

    if (len > 10)
        return false;

    for (a = 0; a < utenti_registrati; a++)
    {
        ok = memcmp(utenti[a].nome, nome, len);
        if (ok == 0)
        {            
            if( utenti[a].tastierino[0] == pin[0] &&
                utenti[a].tastierino[1] == pin[1] &&
                utenti[a].tastierino[2] == pin[2] &&
                utenti[a].tastierino[3] == pin[3] )
            {
                if (index != 0)
                {
                    *index = a;
                    ret = true;
                }
            }
            // break;
        }
    }
    return ret;
}

bool findCard( char* code, int *index)
{
    bool ret = false;
    int a;
    int ok;
    int len = strlen(code);
    int len2;
    
    if (len == 0)
        return false;

    for (a = 0; a < MAX_CARDS; a++)
    {
        len2 = strlen( (const char*)cards.slave[a].code );
        if( len == len2 )
        {
            ok = memcmp( code, cards.slave[a].code , len );
            if (ok == 0)
            {
                *index = a;
                ret = true;
                break;
            }
        }
    }
    return ret;
}


bool findCode(char *code, int *index)
{
    bool ret = false;
    int a;
    int ok;
    int len = strlen(code);

    if (len == 0)
        return false;
    if (len > 6)
        return false;

    for (a = 0; a < utenti_registrati; a++)
    {
        ok = memcmp(code, utenti[a].tastierino, 6);

        if (ok == 0)
        {
            *index = a;
            ret = true;
            break;
        }
    }
    return ret;
}

bool findRfid(char *code, int *index)
{
    bool ret = false;
    int a;
    int ok;
    int len = strlen(code);

    if (len < 4 || len > 10)
        return false;

    for (a = 0; a < utenti_registrati; a++)
    {
        ok = memcmp(code, utenti[a].rfid, 8);
        if (ok == 0)
        {
            *index = a;
            ret = true;
            break;
        }
    }
    return ret;
}

bool findFingerprint(char code, int* index)
{
    bool ret = false;
    int a;

    for (a = 0; a < utenti_registrati; a++)
    {
        if (utenti[a].impronta == code )
        {
            *index = a;
            ret = true;
            break;
        }
    }
    return ret;
}

uint8_t findfreeFingerprint()
{    
    // La scheda del fingerprint puo' contenere fino a novanta impronte
    // sono catalogate in essa da 10 a 100 
    // corrispondente alla posizione in memoria
    // trovo un impronta non memorizzata per poterla associare ad un utente
    
    int a,f;
    bool ok;

    for( f = 10; f < 101; f++)    
    {
        ok = true;
        for (a = 0; a < utenti_registrati; a++)
        {
            if (utenti[a].impronta == f )
            {
                ok = false;
            }
        }
        if( ok == true ) return f;
    }
    return 0;
}

bool verificaCodiceApertura(char *code, int *index)
{
    if (findCode(code, index) == true)
    {
        // codice trovato
        if (utenti[*index].permessi & permesso_tastierino)
        {
            // posso entrare col tastierino !

            // posso entrare in questo momento  // TODO

            return true;
        }
    }
    return false;
}

bool verificaCodiceRfidApertura(char *code, int *index)
{
    if (findRfid(code, index) == true)
    {
        // codice trovato
        if (utenti[*index].permessi & permesso_rfid)
        {
            // posso entrare colla tessera NFC !
            // posso entrare in questo momento  // TODO
            return true;
        }
    }
    return false;
}

bool verificaCredenziali(int ut, char *pass)
{
    int len = strlen(utenti[ut].pass);
    if (len > 8)
        return false;

    if (memcmp(&utenti[ut].pass[0], pass, len) == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

eErrorCode verificaPermessoChiusura(int ut, ePermessi permesso)
{
    // Ho il permesso di chiudere ?
    if (utenti[ut].permessi & permesso_disabilitato)
        return err_permesso_disabilitato;
    if ((utenti[ut].permessi & permesso) == 0)
        return err_permesso_telefono;

    return err_ok;
}

eErrorCode verificaPermessoApertura(int ut, ePermessi permesso)
{
    long m1;
    long m2;

    uint64_t posAdesso;
    uint64_t confronto;

    cy_stc_rtc_config_t orologio;

    // Ho il permesso di aprire ?
    if (utenti[ut].permessi & permesso_disabilitato)
        return err_permesso_disabilitato;
    
    if ((utenti[ut].permessi & permesso) == 0)
        return err_permesso_disabilitato;

    Cy_RTC_GetDateAndTime(&orologio);

    if (utenti[ut].tipo == utente_affittuario)
    {
        m1 = oreDaAnno2000(utenti[ut].affitto.giorno, utenti[ut].affitto.mese, utenti[ut].affitto.anno, utenti[ut].affitto.ore);
        m2 = oreDaAnno2000(orologio.date, orologio.month, orologio.year, orologio.hour);

        if (m2 < m1)
            return err_orario;
        if ((m2 - m1) >= utenti[ut].affitto.orePermanenza)
        {
            // Scaduto il tempo dell' affittuario, elimino l'utente
            removeUtente(ut);
            return err_affitto_scaduto;
        }
        return err_ok;
    }
    else
    {
        if (permesso == permesso_smartphone)
        {
            if (random_code[0] != verifi_code[0])
                return err_verifica_random_code;
            if (random_code[1] != verifi_code[1])
                return err_verifica_random_code;
        }
    }

    // Posso aprire oggi a quest' ora ?
    m1 = orologio.min + (orologio.hour * 60);
    posAdesso = 0x800000000000;

    if (utenti[ut].permessi_orari[orologio.dayOfWeek - 1] == 0xFFFFFFFFFFFF)
    {
        return err_ok;
    }

    m1 = m1 / 30;
    posAdesso = posAdesso >> m1;

    confronto = (utenti[ut].permessi_orari[orologio.dayOfWeek - 1] & posAdesso);
    if (confronto > 0)
    {
        return err_ok;
    }
    else
        return err_orario;
}

bool salvoLogUtente(int index, uint8_t evento, uint8_t esecuzione)
{
    cy_stc_rtc_config_t orologio;
    Cy_RTC_GetDateAndTime(&orologio);

    utenti[index].log[utenti[index].plog].datetime.giorno = orologio.date;
    utenti[index].log[utenti[index].plog].datetime.mese = orologio.month;
    utenti[index].log[utenti[index].plog].datetime.anno = orologio.year;
    utenti[index].log[utenti[index].plog].datetime.ore = orologio.hour;
    utenti[index].log[utenti[index].plog].datetime.minuti = orologio.min;
    utenti[index].log[utenti[index].plog].richiesta = evento;
    utenti[index].log[utenti[index].plog].richiesta_eseguita = esecuzione;

    ++utenti[index].plog;

    if (utenti[index].tlog < 30)
        ++utenti[index].tlog;
    if (utenti[index].plog == 30)
        utenti[index].plog = 0;

        
    ultimoLog.utente[0]  = utenti[index].nome[0];
    ultimoLog.utente[1]  = utenti[index].nome[1];
    ultimoLog.utente[2]  = utenti[index].nome[2];
    ultimoLog.utente[3]  = utenti[index].nome[3];
    ultimoLog.utente[4]  = utenti[index].nome[4];
    ultimoLog.utente[5]  = utenti[index].nome[5];
    ultimoLog.utente[6]  = utenti[index].nome[6];
    ultimoLog.utente[7]  = utenti[index].nome[7];
    ultimoLog.utente[8]  = utenti[index].nome[8];
    ultimoLog.utente[9]  = utenti[index].nome[9];
    ultimoLog.utente[10] = 0;
    
    ultimoLog.log.datetime.giorno    = orologio.date;
    ultimoLog.log.datetime.mese      = orologio.month;
    ultimoLog.log.datetime.anno      = orologio.year;
    ultimoLog.log.datetime.ore       = orologio.hour;
    ultimoLog.log.datetime.minuti    = orologio.min;
    ultimoLog.log.richiesta          = evento;
    ultimoLog.log.richiesta_eseguita = esecuzione;
        
    return true;
}

bool eliminoLogUtente(int index)
{
    if (index == 0xff)
    {
        // Elimino il log di tutti gli utenti
        for (index = 0; index < utenti_registrati; index++)
        {
            memset(utenti[index].log, 0, sizeof(sLog) * 30);
            utenti[index].tlog = 0;
            utenti[index].plog = 0;
        }
        return true;
    }
    // Elimino il log di un singolo utente
    memset(utenti[index].log, 0, sizeof(sLog) * 30);
    utenti[index].tlog = 0;
    utenti[index].plog = 0;
    return true;
}

void disabilitaUtente(int index)
{
    if (index == 0xff)
    {
        for (index = 0; index < utenti_registrati; index++)
        {
            utenti[index].permessi |= 0x20;
        }
    }
    else
    {
        utenti[index].permessi |= 0x20;
    }
}
void abilitaUtente(int index)
{
    if (index == 0xff)
    {
        for (index = 0; index < utenti_registrati; index++)
        {
            utenti[index].permessi &= ~0x20;
        }
    }
    else
    {
        utenti[index].permessi &= ~0x20;
    }
}

bool eliminaAffittiScaduti()
{
    bool ret = false;
    long m1;
    long m2;

    cy_stc_rtc_config_t orologio;
    Cy_RTC_GetDateAndTime(&orologio);
    
    int ut;
    int max = utenti_registrati;
    
    for( ut=0; ut < max; ut++)
    {
        if( utenti[ut].tipo == utente_affittuario )
        {
            m1 = oreDaAnno2000(utenti[ut].affitto.giorno, utenti[ut].affitto.mese, utenti[ut].affitto.anno, utenti[ut].affitto.ore);
            m2 = oreDaAnno2000(orologio.date, orologio.month, orologio.year, orologio.hour);

            if ((m2 - m1) >= utenti[ut].affitto.orePermanenza)
            {
                // Scaduto il tempo dell' affittuario, elimino l'utente
                removeUtente(ut);
                ret = true;
            }
        }
    }
    return ret;
}
   

long minutiDaAnno2000(int giorno, int mese, int anno, int ore, int minuti)
{
    // Minuti passati dal 1 gennaio 2000 a data x
    long totale = (long)(anno - 1) * 525600;

    // Aggiungo i giorni bisestili passati
    if (anno % 4 == 0 && mese < 3)
        totale += ((anno / 4) - 1) * 1440;
    else
        totale += (anno / 4) * 1440;

    switch (mese)
    {
    case 2:
        // aggiungo fino a fine gennaio
        totale += 44640;
        break;
    case 3:
        // aggiungo fino a fine febbraio
        totale += 84960;
        break;
    case 4:
        // aggiungo fino a fine marzo
        totale += 129600;
        break;
    case 5:
        // aggiungo fino a fine aprile
        totale += 172800;
        break;
    case 6:
        // aggiungo fino a fine maggio
        totale += 217440;
        break;
    case 7:
        // aggiungo fino a fine giugno
        totale += 260640;
        break;
    case 8:
        // aggiungo fino a fine luglio
        totale += 305280;
        break;
    case 9:
        // aggiungo fino a fine agosto
        totale += 349920;
        break;
    case 10:
        // aggiungo fino a fine settembre
        totale += 393120;
        break;
    case 11:
        // aggiungo fino a fine ottobre
        totale += 437760;
        break;
    case 12:
        // aggiungo fino a fine novembre
        totale += 480960;
        break;
    }

    totale += (long)giorno * 1440;
    totale += (long)ore * 60;
    totale += (long)minuti;

    return totale;
}
long oreDaAnno2000(int giorno, int mese, int anno, int ore)
{
    // Minuti passati dal 1 gennaio 2000 a data x
    long totale = (long)(anno - 1) * 8760;

    // Aggiungo i giorni bisestili passati
    if (anno % 4 == 0 && mese < 3)
        totale += ((anno / 4) - 1) * 24;
    else
        totale += (anno / 4) * 24;

    switch (mese)
    {
    case 2:
        // aggiungo fino a fine gennaio
        totale += 744;
        break;
    case 3:
        // aggiungo fino a fine febbraio
        totale += 1416;
        break;
    case 4:
        // aggiungo fino a fine marzo
        totale += 2160;
        break;
    case 5:
        // aggiungo fino a fine aprile
        totale += 2880;
        break;
    case 6:
        // aggiungo fino a fine maggio
        totale += 3624;
        break;
    case 7:
        // aggiungo fino a fine giugno
        totale += 4344;
        break;
    case 8:
        // aggiungo fino a fine luglio
        totale += 5088;
        break;
    case 9:
        // aggiungo fino a fine agosto
        totale += 5832;
        break;
    case 10:
        // aggiungo fino a fine settembre
        totale += 6552;
        break;
    case 11:
        // aggiungo fino a fine ottobre
        totale += 7296;
        break;
    case 12:
        // aggiungo fino a fine novembre
        totale += 8016;
        break;
    }

    totale += (long)giorno * 24;
    totale += (long)ore;

    return totale;
}

void dataDaMinuti2000(uint64_t minuti, cy_stc_rtc_config_t *dt)
{
    uint64_t minuti_anno = 525600;
    uint64_t minuti_anno_bisestile = 527040;

    uint16_t anno = 2000;
    uint16_t mese = 1;
    uint16_t giorno = 1;
    uint16_t ore = 1;
    uint16_t min = 1;

    // Parto dall' anno 2000
    while (minuti > minuti_anno)
    {
        if ((anno % 4) == 0)
            minuti = minuti - minuti_anno_bisestile;
        else
            minuti = minuti - minuti_anno;
        anno = anno + 1;
    }
    // Parto da Gennaio e trovo il mese
    if (minuti > 44640)
    {
        minuti = minuti - 44640;
        mese = 2;
        // febbraio
        if ((anno % 4) == 0 && minuti > 41760)
        {
            mese = 3;
            minuti = minuti - 41760;
        }
        else if ((anno % 4) != 0 && minuti > 40320)
        {
            mese = 3;
            minuti = minuti - 41760;
        }
        if (mese == 3)
        {
            // marzo
            if (minuti > 44640)
            {
                minuti = minuti - 44640;
                mese = 4;
            }
        }
    }
}
