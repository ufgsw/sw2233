/*******************************************************************************
* File Name: pwm_motore.c
* Version 1.0
*
* Description:
*  This file provides the source code to the API for the pwm_motore
*  component.
*
********************************************************************************
* Copyright 2016-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "pwm_motore.h"

/** Indicates whether or not the pwm_motore has been initialized. 
*  The variable is initialized to 0 and set to 1 the first time 
*  pwm_motore_Start() is called. This allows the Component to 
*  restart without reinitialization after the first call to 
*  the pwm_motore_Start() routine.
*/
uint8_t pwm_motore_initVar = 0U;

/** The instance-specific configuration structure. This should be used in the 
*  associated pwm_motore_Init() function.
*/ 
cy_stc_tcpwm_pwm_config_t const pwm_motore_config =
{
    .pwmMode = 4UL,
    .clockPrescaler = 0UL,
    .pwmAlignment = 0UL,
    .deadTimeClocks = 0UL,
    .runMode = 0UL,
    .period0 = 1000UL,
    .period1 = 2000UL,
    .enablePeriodSwap = false,
    .compare0 = 900UL,
    .compare1 = 16384UL,
    .enableCompareSwap = false,
    .interruptSources = 0UL,
    .invertPWMOut = 0UL,
    .invertPWMOutN = 0UL,
    .killMode = 1UL,
    .swapInputMode = 3UL,
    .swapInput = CY_TCPWM_INPUT_CREATOR,
    .reloadInputMode = 3UL,
    .reloadInput = CY_TCPWM_INPUT_CREATOR,
    .startInputMode = 3UL,
    .startInput = CY_TCPWM_INPUT_CREATOR,
    .killInputMode = 0UL,
    .killInput = CY_TCPWM_INPUT_CREATOR,
    .countInputMode = 3UL,
    .countInput = CY_TCPWM_INPUT_CREATOR,
};


/*******************************************************************************
* Function Name: pwm_motore_Start
****************************************************************************//**
*
*  Calls the pwm_motore_Init() when called the first time and enables 
*  the pwm_motore. For subsequent calls the configuration is left 
*  unchanged and the component is just enabled.
*
* \globalvars
*  \ref pwm_motore_initVar
*
*******************************************************************************/
void pwm_motore_Start(void)
{
    if (0U == pwm_motore_initVar)
    {
        (void) Cy_TCPWM_PWM_Init(pwm_motore_HW, pwm_motore_CNT_NUM, &pwm_motore_config);

        pwm_motore_initVar = 1U;
    }

    Cy_TCPWM_Enable_Multiple(pwm_motore_HW, pwm_motore_CNT_MASK);
    
    #if (pwm_motore_INPUT_DISABLED == 7UL)
        Cy_TCPWM_TriggerStart(pwm_motore_HW, pwm_motore_CNT_MASK);
    #endif /* (pwm_motore_INPUT_DISABLED == 7UL) */    
}


/* [] END OF FILE */
