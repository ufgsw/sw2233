# Securemme Crypto

***Registrazione di un utente affittuario con sicurezza 'bassa'***

Per la scheda in test la chiave crittografica è fissa con **12471972**

l'orario di ingresso fisso alle ore 12

l'orario di uscita fisso alle ore 10

### Code

```
D0 = 0 ( 1 numero, sempre 0 )
D1 = mese ( 2 numeri )
D2 = giorno ( 2 numeri )
D3 = giorni permanenza ( 2 numeri )
D4 = 0 ( 1 numero, sempre 0 )

Esempio
02512070 ( ingresso il 25/12, permanenza 7 giorni )

se la data è precedente al giorno corrente l'anno sarà il sucessivo a quello attuale
altrimenti l'anno è il corrente

il codice di 4 cifre per entrare viene pescato nelle posizioni 2,3,6,7 del codice cifrato
```

### Crypto

```
chiave crypto ( 8 numeri ) = 12471972 
Ogni numero va rollato per le volte indicato dalla chiave, crescente per crittografare, decrescente per decriptare

es. ( 25 febbraio -> 7 gioni di permanenza )
02502070 -> 14973942 ( codice di ingresso 4994 )
0+1 = 1
2+2 = 4
5+4 = 9
0+7 = 7
2+1 = 3
0+9 = 9
7+7 = 14 ( prendo il 4 )
0+2 = 2


14973942 -> 02502070
1-1 = 0
4-2 = 2
9-4 = 5
7-7 = 0
3-1 = 2
9-9 = 0
4-7 = (10+4 -7) = 7
2-2 = 0



```

### Function code

```c
void rollup( uint8_t* data, uint8_t* key, int lendata, int lenkey )
{
    int lk = 0;
    while( lendata )
    {
        *data += key[lk];
        *data = *data % 10;
        ++data;
        if( ++lk == lenkey ) lk = 0;
        --lendata;
    }
}


void rolldw( uint8_t* data, uint8_t* key, int lendata, int lenkey )
{
    int lk = 0;
    while( lendata )
    {
        *data = (*data + 10) - key[lk];
        *data = *data % 10;
        ++data;
        if( ++lk == lenkey ) lk = 0;
        --lendata;
    }
}
```

 

